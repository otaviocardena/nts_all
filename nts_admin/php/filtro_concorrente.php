<?php
include_once('../conn/conexao.php');

$where = "";

if (isset($_GET['estado'])) {
    $estado = $_GET['estado'];
    $where .= "WHERE estado_concorrente = '$estado'";
}

$sql = "SELECT * FROM concorrentes $where";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
    if ($row['porte_empresa_concorrente'] != "null") {
        $porte = $row['porte_empresa_concorrente'];
    } else {
        $porte = "Sem informação";
    }

    if ($row['ativo_concorrente'] == 0) {
        $status = "Inativo";
    } else {
        $status = "Ativo";
    }
?>
    <tr>
        <td><?= $row['cnpj_concorrente'] ?></td>
        <td><?= $row['razao_social_concorrente'] ?></td>
        <td><?= $row['nome_fantasia_concorrente'] ?></td>
        <td><?= $row['natureza_juridica_concorrente'] ?></td>
        <td><?= $porte ?></td>
        <td><?= $row['cnae_concorrente'] ?></td>
        <td><?= $row['logradouro_concorrente'] ?></td>
        <td><?= $row['numero_logradouro_concorrente'] ?></td>
        <td><?= $row['complemento_logradouro_concorrente'] ?></td>
        <td><?= $row['bairro_concorrente'] ?></td>
        <td><?= $row['municipio_concorrente'] ?></td>
        <td><?= $row['estado_concorrente'] ?></td>
        <td><?= $row['cep_concorrente'] ?></td>
        <td><?= $status ?></td>
        <td>
            <center>
                <button class="btn btn-warning btn-circle" onclick="edit_concorrente(<?= $row['id'] ?>)">
                    <i class="fas fa-edit"></i>
                </button>
            </center>
        </td>
    </tr>
<?php } ?>