<?php
include_once('../../conn/conexao.php');

$id = $_GET['id'];

$sql = "SELECT * FROM orgao_documento WHERE id = $id";

$res = mysqli_query($conn, $sql);
$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array('orgao' => $row['fk_orgao']));
    array_push($data, array('titulo' => $row['titulo']));
    array_push($data, array('numero' => $row['numero']));
    array_push($data, array('observacao' => $row['observacao']));
    array_push($data, array('validade' => date('Y-m-d', strtotime($row['validade']))));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
