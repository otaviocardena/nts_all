<?php
include_once('../../conn/conexao.php');

$id = $_GET['id'];

$sql = "SELECT * FROM orgao WHERE id = $id";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('nome' => $row['nome']));
	array_push($data,array('cnpj' => $row['cnpj']));
	array_push($data,array('telefone' => $row['telefone']));
	array_push($data,array('celular' => $row['celular']));
	array_push($data,array('endereco' => $row['endereco']));
	array_push($data,array('site' => $row['site']));
	array_push($data,array('email_orgao' => $row['email_orgao']));
	array_push($data,array('responsavel' => $row['responsavel']));
	array_push($data,array('email_responsavel' => $row['email_responsavel']));
	array_push($data,array('telefone_responsavel' => $row['telefone_responsavel']));
	array_push($data,array('setor' => $row['setor']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>