<?php
include_once('../../conn/conexao.php');

$id = $_GET['id'];

$sql = "SELECT * FROM concorrentes WHERE id = $id";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('cnpj' => $row['cnpj_concorrente']));
	array_push($data,array('razao_social' => $row['razao_social_concorrente']));
	array_push($data,array('nome_fantasia' => $row['nome_fantasia_concorrente']));
	array_push($data,array('natureza_juridica' => $row['natureza_juridica_concorrente']));
	array_push($data,array('porte_empresa' => $row['porte_empresa_concorrente']));
	array_push($data,array('cnae' => $row['cnae_concorrente']));
	array_push($data,array('logradouro' => $row['logradouro_concorrente']));
	array_push($data,array('numero_logradouro' => $row['numero_logradouro_concorrente']));
	array_push($data,array('complemento_logradouro' => $row['complemento_logradouro_concorrente']));
	array_push($data,array('bairro' => $row['bairro_concorrente']));
	array_push($data,array('municipio' => $row['municipio_concorrente']));
	array_push($data,array('estado' => $row['estado_concorrente']));
	array_push($data,array('cep' => $row['cep_concorrente']));
	array_push($data,array('ativo' => $row['ativo_concorrente']));
	// array_push($data,array('recadastrado' => $row['recadastrado_concorrente']));
	// array_push($data,array('habilitado_licitar' => $row['habilitado_licitar_concorrente']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>