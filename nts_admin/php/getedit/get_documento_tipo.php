<?php
include_once('../../conn/conexao.php');

$id = $_GET['id'];

$sql = "SELECT * FROM documento_tipo WHERE id = $id";

$res = mysqli_query($conn, $sql);
$data = array();
while ($row = mysqli_fetch_array($res)) {
    array_push($data, array('nome' => $row['nome']));
    array_push($data, array('validade' => date('Y-m-d', strtotime($row['validade']))));
    array_push($data, array('id_categoria_doc' => $row['id_categoria_doc']));

    if (isset($row['id_edital'])) {
        array_push($data, array('id_edital' => $row['id_edital']));
    } else {
        array_push($data, array('id_edital' => 'Empty'));
    }
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
