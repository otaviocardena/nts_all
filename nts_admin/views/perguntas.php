<?php
include_once('../conn/conexao.php');
$sql = "SELECT 
            p.id,
            p.pergunta,
            s.setor 
        FROM pergunta AS p
        INNER JOIN setor AS s
            ON p.fk_setor = s.id";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Perguntas</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTablePerguntas" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Pergunta</th>
                        <th>Setor</th>
                        <th width="5%">Editar</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Pergunta</th>
                        <th>Setor</th>
                        <th width="5%">Editar</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                            <td><?= $row['pergunta'] ?></td>
                            <td><?= $row['setor'] ?></td>
                            <td>
                                <center>
                                    <button class="btn btn-warning btn-circle" onclick="edit_pergunta(<?= $row['id'] ?>)">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-pergunta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edição de Pergunta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit-pergunta" method="POST" action="php/edita_pergunta.php">
                <input type="hidden" id="id_pergunta_edit" name="id_pergunta_edit">
                    <div class="form-group">
                        <label for="pergunta" class="col-form-label" style="padding:0px">Digite a pergunta que será enviada ao setor:</label>
                        <textarea name="pergunta_edit" id="pergunta_edit" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="setor_pergunta" class=" col-form-label" style="padding:0px">Selecione o setor que será enviado:</label>
                        <select id="setor_pergunta_edit" name="setor_pergunta_edit" class="form-control">
                            <?php 
                            $sql = "SELECT * FROM setor";
                            $res_setor_edit_pergunta = mysqli_query($conn, $sql);
                            while ($row = mysqli_fetch_array($res_setor_edit_pergunta)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['setor'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" onclick="editar_pergunta()" class="btn btn-primary">Editar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTablePerguntas').DataTable({});
    });

    function edit_pergunta(id) {
        $.get("php/getedit/get_perguntas.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $("#id_pergunta_edit").val(id);
            $("#pergunta_edit").val(json[0].pergunta);
            $("#setor_pergunta_edit").val(json[1].setor);

            $('#modal-edit-pergunta').modal('show');
        });
    }

    function editar_pergunta() {
        $('#form-edit-pergunta').submit();
    }
</script>