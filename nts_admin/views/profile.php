<?php
session_start();

include_once('../conn/conexao.php');

function get_nome_setor($id)
{
  global $conn;
  $sql = "SELECT setor FROM setor WHERE id=$id";
  $res = mysqli_query($conn, $sql);

  while ($row = mysqli_fetch_array($res)) {
    $setor = $row[0];
  }
  return $setor;
}

$sessao = $_SESSION["ZWxldHJpY2Ft_adm"];
$setor = $_SESSION['setor_adm'];

$sql = "SELECT * FROM usuario WHERE id = $sessao";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
  $cargo = $row['cargo'];
  $email = $row['email'];
  $cpf = $row['cpf'];
  $telefone = $row['telefone'];
}

$sql = "SELECT 
          ep.id 
        FROM edital_pendente AS ep 
        INNER JOIN pergunta AS p ON 
          ep.fk_pergunta = p.id 
        WHERE 
          p.fk_setor = $setor AND
          ep.status = 0
        GROUP BY ep.edital_id";
$resPendente = mysqli_query($conn, $sql);
$pendentes = mysqli_num_rows($resPendente);

$sql = "SELECT 
          er.id 
        FROM edital_respondido AS er
        INNER JOIN pergunta AS p ON 
          er.fk_pergunta = p.id 
        WHERE 
          p.fk_setor = $setor AND
          er.user_id = $sessao
        GROUP BY er.edital_id";
$resRespondidos = mysqli_query($conn, $sql);
$respondidos = mysqli_num_rows($resRespondidos);

?>

<!-- Begin Page Content -->
<div class="container-fluid">
  <div class="card shadow" style="align-items: center;">
    <div class="card-body" style="display: flex;align-items:center">
      <div class="d-flex flex-column align-items-center text-center" style="width: fit-content;">
        <img src="img/avatar.png" alt="Admin" class="rounded-circle" width="150">
      </div>
      <div class="mt-3" style="margin-left: 35px;">
        <div class="form-row">
          <div class="col">
            <h4 style="margin:0"><?php echo $_SESSION['name_adm'] ?></h4>
            <p><b><?php echo "Setor " . get_nome_setor($_SESSION['setor_adm']) ?></b></p>
          </div>
        </div>
        <p class="text-secondary mb-1" style="margin:0"><?= $cargo ?></p>
        <p class="text-muted mb-1" style="margin:0"><?= $email ?></p>
        <p class="text-muted mb-1" style="margin:0"><strong>CPF: </strong><?= $cpf ?></p>
        <p class="text-muted mb-1" style="margin:0"><strong>Telefone: </strong><?= $telefone ?></p>
      </div>
    </div>
  </div>
</div>