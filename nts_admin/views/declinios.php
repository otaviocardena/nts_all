<?php
include_once('../conn/conexao.php');
$sql = "SELECT * FROM motivo_declinado";
$res = mysqli_query($conn, $sql);

$sql = "SELECT * FROM categoria_motivo_declinado";
$res_cat = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Declínios</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableDeclinios" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Motivo</th>
                        <th>Categoria</th>
                        <th width="5%">Editar</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Motivo</th>
                        <th>Categoria</th>
                        <th width="5%">Editar</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                            <td><?= $row['motivo'] ?></td>
                            <td><?= $row['categoria'] ?></td>
                            <td>
                                <center>
                                    <button class="btn btn-warning btn-circle" onclick="edit_declinio(<?= $row['id'] ?>)">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-declinios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edição de Motivo Declinio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit-declinios" method="POST" action="php/edita_motivo_declinio.php">
                <input type="hidden" id="id_declinio" name="id_declinio">
                    <div class="form-group">
                        <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Declinio:</label>
                        <textarea name="motivo_declinio_edit" id="motivo_declinio_edit" cols="30" rows="10" class="form-control" placeholder="Informe o Motivo de Declinio..."></textarea>
                    </div>
                    <div class="form-group">
                        <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Categoria:</label><br>
                        <select id="categoria_declinio_edit" name="categoria_declinio_edit" class="form-control">
                        <?php while ($row = mysqli_fetch_array($res_cat)){ ?>
                         <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
                        <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="editar_declinio()">Editar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTableDeclinios').DataTable({});
    });

    function edit_declinio(id) {
        $.get("php/getedit/get_declinios.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $("#id_declinio").val(id);
            $("#motivo_declinio_edit").val(json[0].motivo);
            $("#categoria_declinio_edit").val(json[1].categoria);

            $('#modal-edit-declinios').modal('show');
        });
    }

    function editar_declinio() {
        $('#form-edit-declinios').submit();
    }
</script>