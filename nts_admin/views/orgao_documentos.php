<?php
include_once('../conn/conexao.php');
$sql = "SELECT
          od.id,
          o.nome AS orgao,
          od.titulo,
          od.numero,
          od.validade,
          od.observacao
        FROM orgao_documento AS od
        INNER JOIN orgao AS o ON
          od.fk_orgao = o.id
        ";
$res = mysqli_query($conn, $sql);

$sql = "SELECT * FROM orgao";
$res_orgao_doc = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Documentos de orgão</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTableOrgaoDocumentos" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Orgao</th>
            <th>Titulo</th>
            <th>Número</th>
            <th>Validade</th>
            <th width="20%">Observacao</th>
            <th width="5%">Arquivo</th>
            <th width="5%">Editar</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Orgao</th>
            <th>Titulo</th>
            <th>Número</th>
            <th>Validade</th>
            <th width="20%">Observacao</th>
            <th width="5%">Arquivo</th>
            <th width="5%">Editar</th>
          </tr>
        </tfoot>
        <tbody>
          <?php while ($row = mysqli_fetch_array($res)) {
          ?>
            <tr>
              <td><?= $row['orgao'] ?></td>
              <td><?= $row['titulo'] ?></td>
              <td><?= $row['numero'] ?></td>
              <td><?= date('d/m/Y', strtotime($row['validade'])) ?></td>
              <td><?= $row['observacao'] ?></td>
              <td>
                <center>
                  <a href="php/baixa_arquivos_edit.php?tabela=orgao_documento&id=<?= $row['id'] ?>" class="btn btn-primary btn-circle">
                    <i class="fas fa-cloud-download-alt"></i>
                  </a>
                </center>
              </td>
              <td>
                <center>
                  <button class="btn btn-warning btn-circle" onclick="edit_orgao_documento(<?= $row['id'] ?>)">
                    <i class="fas fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-orgao-documento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edição de Documentos de Orgão</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-edit-orgao-documento" method="POST" action="php/edita_orgao_documento.php" enctype="multipart/form-data">
          <input type="hidden" name="id_orgao_documento_edit" id="id_orgao_documento_edit">
          <div class="form-group">
            <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Orgão:</label>
            <select class="form-control" id="orgao_documento_edit" name="orgao_documento_edit">
              <?php while ($row = mysqli_fetch_array($res_orgao_doc)) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Titulo:</label>
            <input type="text" class="form-control" id="titulo_orgao_documento_edit" name="titulo_orgao_documento_edit">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Número:</label>
            <input type="number" class="form-control" id="numero_orgao_documento_edit" name="numero_orgao_documento_edit">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Validade:</label>
            <input type="date" class="form-control" id="date_orgao_documento_edit" name="date_orgao_documento_edit">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Observação:</label>
            <input type="text" class="form-control" id="observacao_orgao_documento_edit" name="observacao_orgao_documento_edit">
          </div>
          <hr>
          <div class="form-group">
            <label>*Não obrigatório</label><br>
            <input onchange="inserir_nome_doc()" type="file" id="doc_orgao_documento_edit" name="doc_orgao_documento_edit" style="display:none;">
            <div style="cursor:pointer;" onclick="abrir_input_doc()" class="btn btn-primary">
              <label id="filename_doc_orgao_documento_edit">*Anexar novo arquivo</label>
            </div>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" onclick="editar_orgao_documento()">Cadastrar</button>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#dataTableOrgaoDocumentos').DataTable({});
  });

  function edit_orgao_documento(id) {
    $.get("php/getedit/get_orgao_documentos.php?id=" + id, function(data) {
      var json = JSON.parse(data);
      $("#id_orgao_documento_edit").val(id);
      $("#orgao_documento_edit").val(json[0].orgao);
      $("#titulo_orgao_documento_edit").val(json[1].titulo);
      $("#numero_orgao_documento_edit").val(json[2].numero);
      $("#observacao_orgao_documento_edit").val(json[3].observacao);
      $("#date_orgao_documento_edit").val(json[4].validade);

      $('#modal-edit-orgao-documento').modal('show');
    });
  }

  function editar_orgao_documento() {
    $('#form-edit-orgao-documento').submit();
  }

  function abrir_input_doc() {
    $('#doc_orgao_documento_edit').click();
  }

  function inserir_nome_doc() {
    $('#filename_doc_orgao_documento_edit').html($('#doc_orgao_documento_edit')[0].files[0].name);
  }
</script>