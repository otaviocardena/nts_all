<?php
include_once('../conn/conexao.php');
// DASHBOARD COUNTERS
$sql = "SELECT 
          * 
        FROM edital as e
        LEFT JOIN edital_impugnacao as ei ON
          e.id = ei.edital_id
        LEFT JOIN edital_esclarecimento as ee ON
          e.id = ee.edital_id
        LEFT JOIN edital_declinado as ed ON
          e.id = ed.fk_id_edital
        LEFT JOIN edital_pendente as ep ON
          e.id = ep.edital_id
        LEFT JOIN edital_aprovado as ea ON
          e.id = ea.edital_id
        LEFT JOIN edital_agendado as eag ON
          e.id = eag.id_edital
        WHERE 
          ei.id IS NULL AND
          ee.id IS NULL AND
          ep.id IS NULL AND
          ed.id IS NULL AND
          ea.id IS NULL AND
          eag.id IS NULL";
$res = mysqli_query($conn, $sql);
$count_edital_aberto = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_pendente WHERE status = 0";
$res = mysqli_query($conn, $sql);
$count_edital_pendente = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_aprovado";
$res = mysqli_query($conn, $sql);
$count_edital_aprovado_1 = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_aprovado_fase_2";
$res = mysqli_query($conn, $sql);
$count_edital_aprovado_2 = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_aprovado_fase_3";
$res = mysqli_query($conn, $sql);
$count_edital_aprovado_3 = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_declinado";
$res = mysqli_query($conn, $sql);
$count_edital_declinado = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_agendado";
$res = mysqli_query($conn, $sql);
$count_edital_agendado = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_impugnacao";
$res = mysqli_query($conn, $sql);
$count_edital_impugnacao = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_esclarecimento";
$res = mysqli_query($conn, $sql);
$count_edital_esclarecimento = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_homologado WHERE status = 0";
$res = mysqli_query($conn, $sql);
$count_edital_aguardando_homologacao = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_homologado WHERE status = 1";
$res = mysqli_query($conn, $sql);
$count_edital_homologado = mysqli_num_rows($res);

$graph_bar = $count_edital_aberto.",".$count_edital_pendente.",".$count_edital_aprovado_1.",".$count_edital_declinado.",".$count_edital_agendado.",".$count_edital_agendado.",".$count_edital_impugnacao.",".$count_edital_esclarecimento;



$months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
$graph_line = "";
$max = 0;
for($i = 0; $i < count($months) ; $i++){
  $sql = "SELECT 
            count(e.id) as contagem
          from 
          edital as e 
            inner join edital_homologado as eh on 
            e.id = eh.id_edital 
          where 
          month(eh.data_cad) = '" . $months[$i] . "'";
  $res = mysqli_query($conn,$sql);
  $count = mysqli_fetch_array($res)[0];
  if($count > $max){
    $max = $count;
  }
  $graph_line .= $count .",";
}

$graph_line = substr($graph_line,0,-1);

$sql = "select * from setor";
$res2 = mysqli_query($conn,$sql);

$setor = "";
$setor_value ="";
$max_setor = 0;
while($row = mysqli_fetch_array($res2)){
  $setor_id = $row['id'];
  $setor_name = $row['setor'];
  $sql = "select 
        count(ep.id)
        from 
        edital as e 
          inner join edital_pendente as ep on 
          e.id = ep.edital_id 
          inner join pergunta as p on 
          ep.fk_pergunta = p.id
        where 
        p.fk_setor = $setor_id
        group by ep.edital_id";
  $res = mysqli_query($conn,$sql);
  $count_perguntas_setor = mysqli_num_rows($res);
  if($count_perguntas_setor > $max_setor){
    $max_setor = $count_perguntas_setor;
  }
  $setor .= "'".$setor_name."',";
  $setor_value .= $count_perguntas_setor.",";
}

$graph_setor = substr($setor,0,-1);
$graph_setor_value = substr($setor_value,0,-1);
?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>

<!-- Content Row -->
<div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaedital.php')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Editais Abertos</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_aberto ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaPendente.html')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Editais Pendentes</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_pendente ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-secondary shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaEditalAgendado.html')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Editais Agendados</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_agendado ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaedital.php')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Editais Impugnados</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_impugnacao ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaedital.php')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Editais Esclarecidos</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_esclarecimento ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Pending Requests Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-danger shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('teladeclinados.php')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Editais Declinados</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_declinado ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Pending Requests Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaAprovado.html')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Editais Aprovados - FASE 1</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_aprovado_1 ?></div>
              </div>

            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaAprovado.html')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Editais Aprovados - FASE 2</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_aprovado_2 ?></div>
              </div>

            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaAprovado.html')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Editais Aprovados - FASE 3</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_aprovado_3 ?></div>
              </div>

            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaAprovado.html')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Editais Aguardando Homologação</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_aguardando_homologacao ?></div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2" style="cursor:pointer;" onclick="page_system('telaAprovado.html')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Editais Homologados</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_homologado ?></div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- AQUI FICA OS GRAFICOS -->

<div class="row">
    <!-- Area Chart -->
    <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4" id="graph1">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                        aria-labelledby="dropdownMenuLink">
                        <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myChart" ></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- Area Chart -->
    <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4" id="graph2" > 
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                        aria-labelledby="dropdownMenuLink">
                        <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myChartBar" style="heigth: 100%"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- Area Chart -->
    <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4" id="graph3" > 
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                        aria-labelledby="dropdownMenuLink">
                        <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myChartBarSetor" style="heigth: 100%"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
const labels = ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'];
const datapoints = [<?php echo $graph_line; ?>];
const data = {
  labels: labels,
  datasets: [
    {
      label: 'Edital Homologado',
      data: datapoints,
      borderColor: 'red',
      fill: false,
      cubicInterpolationMode: 'monotone',
      tension: 0.4
    }
  ]
};
const config = {
  type: 'line',
  data: data,
  options: {
    responsive: true,
    plugins: {
      title: {
        display: true,
        text: 'Editais Homologados por mês'
      },
    },
    interaction: {
      intersect: false,
    },
    scales: {
      x: {
        display: true,
        title: {
          display: true
        }
      },
      y: {
        display: true,
        title: {
          display: true,
          text: 'Value'
        },
        suggestedMin: 0,
        suggestedMax: <?php echo $max > 10 ? $max + 10 : $max + 2; ?> 
      }
    }
  },
};
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, config);


const data_bar = {
  labels: ['Abertos','Pendentes','Aprovado','Declinado','Agendado','Impugnado','Esclarecimento'],
  datasets: [
    {
      label: 'Editais',
      data: [<?php echo $graph_bar;?>],
      backgroundColor: 'blue',
    }
  ]
};
const config_bar = {
  type: 'bar', 
  data: data_bar,
  options: {
    plugins: {
      title: {
        display: true,
        text: 'Contagens de Editais'
      },
    },
    responsive: true,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true
      }
    }
  }
};


var ctx_bar = document.getElementById('myChartBar').getContext('2d');
var myChartBar = new Chart(ctx_bar, config_bar);

const data_setor = {
  labels: [<?php echo $graph_setor;?>],
  datasets: [
    {
      label: 'Setores',
      data: [<?php echo $graph_setor_value;?>],
      backgroundColor: 'blue',
    }
  ]
};
const config_setor = {
  type: 'bar', 
  data: data_setor,
  options: {
    plugins: {
      title: {
        display: true,
        text: 'Setor mais solicitado'
      },
    },
    responsive: true,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true,
        suggestedMin: 0,
        suggestedMax: <?php echo $max_setor > 10 ? $max_setor + 10 : $max_setor + 2; ?> 
      }
    }
  }
};


var ctx_setor = document.getElementById('myChartBarSetor').getContext('2d');
var myChartSetor = new Chart(ctx_setor, config_setor);

let height = ctx_bar.canvas.clientHeight + 77;

$('#graph1').css("height",height+"px");
$('#graph2').css("height",height+"px");  
$('#graph3').css("height",height+"px");  
  
window.addEventListener('resize', function(event) {
      let height = ctx_bar.canvas.clientHeight + 77;

      $('#graph1').css("height",height+"px");
      $('#graph2').css("height",height+"px");  
      $('#graph3').css("height",height+"px");  
  
}, true);
</script>
