<?php
include_once('../conn/conexao.php');
$sql = "SELECT * FROM orgao";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Orgao</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableOrgao" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Razão Social</th>
                        <th>CNPJ</th>
                        <th>Telefone</th>
                        <th>Celular</th>
                        <th>Endereço</th>
                        <th>Email</th>
                        <th>Representante</th>
                        <th>Setor</th>
                        <th width="5%">Editar</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Razão Social</th>
                        <th>CNPJ</th>
                        <th>Telefone</th>
                        <th>Celular</th>
                        <th>Endereço</th>
                        <th>Email</th>
                        <th>Representante</th>
                        <th>Setor</th>
                        <th width="5%">Editar</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                            <td><?= $row['nome'] ?></td>
                            <td><?= $row['cnpj'] ?></td>
                            <td><?= $row['telefone'] ?></td>
                            <td><?= $row['celular'] ?></td>
                            <td><?= $row['endereco'] ?></td>
                            <td><?= $row['email_orgao'] ?></td>
                            <td><?= $row['responsavel'] ?></td>
                            <td><?= $row['setor'] ?></td>
                            <td>
                                <center>
                                    <button class="btn btn-warning btn-circle" onclick="edit_orgao(<?= $row['id'] ?>)">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-orgao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edição de Orgão</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-edit-orgao" method="POST" action="php/edita_orgao.php">
          <input type="hidden" id="id_orgao" name="id_orgao">
            <div class="form-group" style="display:flex">
              <div style="width: -webkit-fill-available;">
                <label for="recipient-name" class="col-form-label">Nome:</label>
                <input type="text" id="nome_orgao_edit" name="nome_orgao_edit" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">CNPJ:</label>
                <input type="text" id="cnpj_orgao_edit" name="cnpj_orgao_edit" class="form-control">
              </div>

            </div>
            <div class="form-group" style="display:flex">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Celular:</label>
                <input type="text" id="celular_orgao_edit" name="celular_orgao_edit" class="form-control">
              </div>
              <div style="width: -webkit-fill-available; margin-left: 20px;">
                <label for="message-text" class="col-form-label">Telefone:</label>
                <input type="text" id="telefone_orgao_edit" name="telefone_orgao_edit" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Endereço:</label>
              <input type="text" id="endereco_orgao_edit" name="endereco_orgao_edit" class="form-control">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Site:</label>
              <input type="text" id="site_orgao_edit" name="site_orgao_edit" class="form-control">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">E-mail:</label>
              <input type="text" id="email_orgao_edit" name="email_orgao_edit" class="form-control">
            </div>
            <hr>
            <label for="message-text" class="col-form-label">Dados Responsavel</label>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Responsavel:</label>
                <input type="text" id="responsavel_orgao_edit" name="responsavel_orgao_edit" class="form-control">
              </div>
              <div style="margin-left: 20px;width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">E-mail:</label>
                <input type="text" id="email_responsavel_orgao_edit" name="email_responsavel_orgao_edit" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Fone Responsavel:</label>
                <input type="text" id="telefone_responsavel_orgao_edit" name="telefone_responsavel_orgao_edit" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">Setor:</label>
                <input type="text" id="setor_orgao_edit" name="setor_orgao_edit" class="form-control">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" onclick="editar_orgao()" class="btn btn-primary">Editar</button>
        </div>
      </div>
    </div>
  </div>

<script>
    $(document).ready(function() {
        $('#dataTableOrgao').DataTable({});
    });

    function edit_orgao(id) {
		$.get("php/getedit/get_orgao.php?id=" + id, function(data) {
			var json = JSON.parse(data);
			$("#id_orgao").val(id);
			$("#nome_orgao_edit").val(json[0].nome);
			$("#cnpj_orgao_edit").val(json[1].cnpj);
			$("#telefone_orgao_edit").val(json[2].telefone);
			$("#celular_orgao_edit").val(json[3].celular);
			$("#endereco_orgao_edit").val(json[4].endereco);
			$("#site_orgao_edit").val(json[5].site);
			$("#email_orgao_edit").val(json[6].email_orgao);
			$("#responsavel_orgao_edit").val(json[7].responsavel);
			$("#email_responsavel_orgao_edit").val(json[8].email_responsavel);
			$("#telefone_responsavel_orgao_edit").val(json[9].telefone_responsavel);
			$("#setor_orgao_edit").val(json[10].setor);

			$('#modal-edit-orgao').modal('show');
		});
	}

    function editar_orgao(){
        $('#form-edit-orgao').submit();
    }
</script>