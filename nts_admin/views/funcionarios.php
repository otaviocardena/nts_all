<?php
include_once('../conn/conexao.php');
$sql = "SELECT 
            f.id,
            f.nome,
            f.cpf,
            f.cargo,
            f.email,
            s.setor
        FROM usuario AS f
        INNER JOIN setor AS s
            ON f.fk_setor = s.id";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Funcionários</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableFuncionarios" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Cargo</th>
                        <th>Email</th>
                        <th>Setor</th>
                        <th width="5%">Editar</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Cargo</th>
                        <th>Email</th>
                        <th>Setor</th>
                        <th width="5%">Editar</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                            <td><?= $row['nome'] ?></td>
                            <td><?= $row['cpf'] ?></td>
                            <td><?= $row['cargo'] ?></td>
                            <td><?= $row['email'] ?></td>
                            <td><?= $row['setor'] ?></td>
                            <td>
                                <center>
                                    <button class="btn btn-warning btn-circle" onclick="edit_funcionario(<?= $row['id'] ?>)">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-funcionarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edição de Funcionários</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-edit-funcionario" method="POST" action="php/edita_funcionario.php">
          <input type="hidden" id="id_funcionario_edit" name="id_funcionario_edit">
            <div class="form-group" style="display: flex">
              <div style="width: -webkit-fill-available;margin-right:10px;">
                <label for="nome_funcionario" class="col-form-label" style="padding:0px">Nome:</label>
                <input type="text" name="nome_funcionario_edit" id="nome_funcionario_edit" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 10px;">
                <label for="cpf_funcionario" class="col-form-label" style="padding:0px">CPF:</label>
                <input type="text" name="cpf_funcionario_edit" id="cpf_funcionario_edit" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex">
              <div style="width: -webkit-fill-available;margin-right:10px;">
                <label for="cargo_funcionario" class="col-form-label" style="padding:0px">Cargo:</label>
                <input type="text" name="cargo_funcionario_edit" id="cargo_funcionario_edit" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 10px;">
                <label for="email_funcionario" class="col-form-label" style="padding:0px">Email:</label>
                <input type="text" name="email_funcionario_edit" id="email_funcionario_edit" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex">
              <div style="width: -webkit-fill-available;margin-right:10px;">
                <label for="setor_funcionario" class="col-form-label" style="padding:0px">Setor:</label>
                <select type="text" name="setor_funcionario_edit" id="setor_funcionario_edit" class="form-control">
                  <?php 
                  $sql = "SELECT * FROM setor";
                  $res_setor_funcionario_edit = mysqli_query($conn, $sql);
                  while ($row = mysqli_fetch_array($res_setor_funcionario_edit)) { ?>
                    <option value="<?= $row['id'] ?>"><?= $row['setor'] ?></option>
                  <?php } ?>
                </select>
              </div>
              <div style="width: -webkit-fill-available;margin-left: 10px;">
                <label for="telefone_funcionario" class="col-form-label" style="padding:0px">Telefone:</label>
                <input type="text" name="telefone_funcionario_edit" id="telefone_funcionario_edit" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex">
              <div style="width: -webkit-fill-available;margin-right:10px;">
                <label for="login_funcionario" class="col-form-label" style="padding:0px">Login:</label>
                <input readonly type="text" name="login_funcionario_edit" id="login_funcionario_edit" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 10px;">
                <label for="senha_funcionario" class="col-form-label" style="padding:0px">Senha:</label>
                <input readonly type="password" name="senha_funcionario_edit" id="senha_funcionario_edit" class="form-control">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" onclick="editar_funcionario()" class="btn btn-primary">Editar</button>
        </div>
      </div>
    </div>
  </div>

<script>
    $(document).ready(function() {
        $('#dataTableFuncionarios').DataTable({});
    });

    function edit_funcionario(id) {
        $.get("php/getedit/get_funcionarios.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $("#id_funcionario_edit").val(id);
            $("#nome_funcionario_edit").val(json[0].nome);
            $("#cpf_funcionario_edit").val(json[1].cpf);
            $("#cargo_funcionario_edit").val(json[2].cargo);
            $("#email_funcionario_edit").val(json[3].email);
            $("#setor_funcionario_edit").val(json[4].setor);
            $("#telefone_funcionario_edit").val(json[5].telefone);
            $("#login_funcionario_edit").val(json[6].login);
            $("#senha_funcionario_edit").val(json[7].senha);

            $('#modal-edit-funcionarios').modal('show');
        });
    }

    function editar_funcionario() {
        $('#form-edit-funcionario').submit();
    }
</script>