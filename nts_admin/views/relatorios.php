<?php
include_once("../conn/conexao.php");
$sql = "SELECT 
          * 
        FROM edital as e
        LEFT JOIN edital_impugnacao as ei ON
          e.id = ei.edital_id
        LEFT JOIN edital_esclarecimento as ee ON
          e.id = ee.edital_id
        LEFT JOIN edital_declinado as ed ON
          e.id = ed.fk_id_edital
        LEFT JOIN edital_pendente as ep ON
          e.id = ep.edital_id
        LEFT JOIN edital_aprovado as ea ON
          e.id = ea.edital_id
        LEFT JOIN edital_agendado as eag ON
          e.id = eag.id_edital
        WHERE 
          ei.id IS NULL AND
          ee.id IS NULL AND
          ep.id IS NULL AND
          ed.id IS NULL AND
          ea.id IS NULL AND
          eag.id IS NULL";
$res = mysqli_query($conn, $sql);
$count_edital_aberto = mysqli_num_rows($res);

$sql = "SELECT * FROM edital";
$res = mysqli_query($conn, $sql);
$count_edital_recebido = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_juridico";
$res = mysqli_query($conn, $sql);
$count_edital_juridico = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_recurso";
$res = mysqli_query($conn, $sql);
$count_edital_recurso = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_recurso WHERE status = 1";
$res = mysqli_query($conn, $sql);
$count_edital_recurso_deferido = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_recurso WHERE status = 2";
$res = mysqli_query($conn, $sql);
$count_edital_recurso_indeferido = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_declinado";
$res = mysqli_query($conn, $sql);
$count_edital_declinado = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_impugnacao";
$res = mysqli_query($conn, $sql);
$count_edital_impugnacao = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_esclarecimento";
$res = mysqli_query($conn, $sql);
$count_edital_esclarecimento = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_ata_concorrentes WHERE concorrente = 0 AND hab = 0";
$res = mysqli_query($conn, $sql);
$count_edital_inabilitado_nts = mysqli_num_rows($res);

$sql = "SELECT * FROM edital_declinado WHERE motivo_id = 1";
$res = mysqli_query($conn, $sql);
$count_edital_desclassificado = mysqli_num_rows($res);
?>
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Relatórios</h1>
</div>

<!-- Content Row -->
<div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Editais Recebidos</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_recebido ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Editais Analisados</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_recebido - $count_edital_aberto ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Editais Declinados</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_declinado ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pedidos de Impugnação</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_impugnacao ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pedidos de Esclarecimento</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_esclarecimento ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Solicitações Setor Jurídico</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_juridico ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Recursos Habilitados</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_recurso ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Recursos Deferidos</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_recurso_deferido ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Recursos Indeferidos</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_recurso_indeferido ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Inabilitações NTS</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_inabilitado_nts ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Desclassificações</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $count_edital_desclassificado ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2" style="cursor:pointer;" onclick="page_relatorio('rel_qtd_participacao_licitante')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Qtd de Participações</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">Por licitante</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2" style="cursor:pointer;" onclick="page_relatorio('rel_qtd_inab_licitante')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Qtd de Inabilitações</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">Por licitante</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col mb-4">
    <div class="card border-left-primary shadow h-100 py-2" style="cursor:pointer;" onclick="page_relatorio('rel_rank_motivos_declinio')">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Motivos de Declínio</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">Rank</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col mb-4"></div>
</div>

<script>
  function page_relatorio(p) {
    var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#224abe; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
    $("#conteudo").html(data);
    $.get('views/relatorios_view/' + p + '.php', function(data) {
      $('#conteudo').html(data);
    });
  }
</script>