<?php
include_once('../conn/conexao.php');
$sql = "SELECT * FROM setor";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Setores</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableSetores" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th width="5%">Editar</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nome</th>
                        <th width="5%">Editar</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                            <td><?= $row['setor'] ?></td>
                            <td>
                                <center>
                                    <button class="btn btn-warning btn-circle" onclick="edit_resposta(<?= $row['id'] ?>)">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-cadastro-setor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Setor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-edit-setor" method="POST" action="php/edita_setor.php">
          <input type="hidden" id="id_setor_edit" name="id_setor_edit">
            <div class="form-group">
              <label class="col-2 col-form-label" style="padding:0px">Nome:</label>
              <input name="nome_setor_edit" id="nome_setor_edit" class="form-control" placeholder="Nome do setor">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" onclick="editar_setor()">Editar</button>
        </div>
      </div>
    </div>
  </div>

<script>
    $(document).ready(function() {
        $('#dataTableSetores').DataTable({});
    });

    function edit_resposta(id) {
        $.get("php/getedit/get_setores.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $("#id_setor_edit").val(id);
            $("#nome_setor_edit").val(json[0].setor);

            $('#modal-cadastro-setor').modal('show');
        });
    }

    function editar_setor() {
        $('#form-edit-setor').submit();
    }
</script>