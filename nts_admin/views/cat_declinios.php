<?php
include_once('../conn/conexao.php');

$sql = "SELECT * FROM categoria_motivo_declinado";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Categoria de Tipos Declinio</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableCatDeclinios" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th width="5%">Editar</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nome</th>
                        <th width="5%">Editar</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                            <td><?= $row['nome'] ?></td>
                            <td>
                                <center>
                                    <button class="btn btn-warning btn-circle" onclick="edit_categoria_declinio(<?= $row['id'] ?>)">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-cat-declinio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Categoria de Motivo Declinio</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-edit-cat-motivo-declinio" method="POST" action="php/edita_categoria_motivo_declinado.php">
          <input type="hidden" id="id_categoria_edit" name="id_categoria_edit">
            <div class="form-group">
              <label for="example-date-input" class="col-form-label" style="padding:0px">Nome da categoria:</label>
              <input name="categoria_motivo_declinio_edit" id="categoria_motivo_declinio_edit" class="form-control" placeholder="Nome" />
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" onclick="editar_categoria_declinio()">Editar</button>
        </div>
      </div>
    </div>
  </div>

<script>
    $(document).ready(function() {
        $('#dataTableCatDeclinios').DataTable({});
    });

    function edit_categoria_declinio(id) {
        $.get("php/getedit/get_categoria_declinios.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $("#id_categoria_edit").val(id);
            $("#categoria_motivo_declinio_edit").val(json[0].nome);

            $('#modal-edit-cat-declinio').modal('show');
        });
    }

    function editar_categoria_declinio() {
        $('#form-edit-cat-motivo-declinio').submit();
    }
</script>