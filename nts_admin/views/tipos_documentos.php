<?php
include_once('../conn/conexao.php');
$sql = "SELECT
          dt.id,
          dt.id_edital,
          dt.nome,
          dc.nome as nome_categoria,
          dt.validade
        FROM documento_tipo as dt
        INNER JOIN documento_categoria as dc ON
          dt.id_categoria_doc = dc.id";
$res = mysqli_query($conn, $sql);

$sql = "SELECT * FROM documento_categoria";
$res_categoria_doc = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Tipo de Documentos</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTableDocumentos" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Categoria do documento</th>
            <th>Validade</th>
            <th>Edital</th>
            <th width="5%">Arquivo</th>
            <th width="5%">Editar</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Nome</th>
            <th>Categoria do documento</th>
            <th>Validade</th>
            <th>Edital</th>
            <th width="5%">Arquivo</th>
            <th width="5%">Editar</th>
          </tr>
        </tfoot>
        <tbody>
          <?php while ($row = mysqli_fetch_array($res)) {
            if (isset($row['id_edital'])) {
              $edital = $row['id_edital'];
            } else {
              $edital = "Sem edital referente";
            }
          ?>
            <tr>
              <td><?= $row['nome'] ?></td>
              <td><?= $row['nome_categoria'] ?></td>
              <td><?= date('d/m/Y', strtotime($row['validade'])) ?></td>
              <td><?= $edital ?></td>
              <td>
                <center>
                  <a href="php/baixa_arquivos_edit.php?tabela=documento_tipo&id=<?= $row['id'] ?>" class="btn btn-primary btn-circle">
                    <i class="fas fa-cloud-download-alt"></i>
                  </a>
                </center>
              </td>
              <td>
                <center>
                  <button class="btn btn-warning btn-circle" onclick="edit_doc(<?= $row['id'] ?>)">
                    <i class="fas fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-tipocat-documento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edição de Tipo Categoria Documento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-edit-tipo-documento" method="POST" action="php/edita_tipo_documento.php" enctype="multipart/form-data">
          <input type="hidden" name="id_documento_tipo_edit" id="id_documento_tipo_edit">
          <div class="form-group">
            <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Categoria:</label>
            <select id="categoria_documento_edit" name="categoria_documento_edit" class="form-control">
              <?php while ($row = mysqli_fetch_array($res_categoria_doc)) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Tipo Documento:</label>
            <input type="text" id="tipo_documento_edit" name="tipo_documento_edit" class="form-control">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Validade:</label>
            <input type="date" id="validade_documento_edit" name="validade_documento_edit" class="form-control">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Edital Referente:</label>
            <input type="number" id="edital_documento_edit" name="edital_documento_edit" class="form-control" readonly>
          </div>
          <hr>
          <div class="form-group">
            <label>*Não obrigatório</label><br>
            <input onchange="inserir_nome_doc()" type="file" id="doc_tipo_documento_edit" name="doc_tipo_documento_edit" style="display:none;">
            <button onclick="abrir_input_doc()" type="button" class="btn btn-primary">
              <label id="filename_doc_tipo_documento_edit">*Anexar novo arquivo</label>
            </button>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" onclick="editar_documento_tipo()" class="btn btn-primary">Editar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#dataTableDocumentos').DataTable({});
  });

  function edit_doc(id) {
    $.get("php/getedit/get_documento_tipo.php?id=" + id, function(data) {
      var json = JSON.parse(data);
      $("#id_documento_tipo_edit").val(id);
      $("#tipo_documento_edit").val(json[0].nome);
      $("#validade_documento_edit").val(json[1].validade);
      $("#categoria_documento_edit").val(json[2].id_categoria_doc);

      if (json[3].id_edital != "Empty") {
        $('#edital_documento_edit').val(json[3].id_edital);
        $('#edital_documento_edit').removeAttr('readonly');
      } else {
        $('#edital_documento_edit').val("");
        $('#edital_documento_edit').removeAttr('readonly');
        $('#edital_documento_edit').attr('readonly', 'readonly');
      }

      $('#modal-edit-tipocat-documento').modal('show');
    });
  }

  function editar_documento_tipo() {
    $('#form-edit-tipo-documento').submit();
  }

  function abrir_input_doc() {
    $('#doc_tipo_documento_edit').click();
  }

  function inserir_nome_doc() {
    $('#filename_doc_tipo_documento_edit').html($('#doc_tipo_documento_edit')[0].files[0].name);
  }
</script>