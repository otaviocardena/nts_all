<?php
include_once('../conn/conexao.php');
$sql = "SELECT 
            r.id,
            p.pergunta,
            r.resposta,
            r.tipo
        FROM resposta AS r
        INNER JOIN pergunta AS p
            ON r.fk_pergunta = p.id";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Respostas</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableRespostas" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Resposta</th>
                        <th>Pergunta</th>
                        <th>Tipo</th>
                        <th width="5%">Editar</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Resposta</th>
                        <th>Pergunta</th>
                        <th>Tipo</th>
                        <th width="5%">Editar</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) {
                        if ($row['tipo'] == 1) {
                            $tipo = "Positiva";
                        } else {
                            $tipo = "Negativa";
                        }

                    ?>
                        <tr>
                            <td><?= $row['resposta'] ?></td>
                            <td><?= $row['pergunta'] ?></td>
                            <td><?= $tipo ?></td>
                            <td>
                                <center>
                                    <button class="btn btn-warning btn-circle" onclick="edit_resposta(<?= $row['id'] ?>)">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </center>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-resposta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edição de Resposta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit-resposta" method="POST" action="php/edita_resposta.php">
                    <input type="hidden" id="id_resposta_edit" name="id_resposta_edit">
                    <div class="form-group">
                        <label for="example-date-input" class=" col-form-label" style="padding:0px">Selecione a pergunta:</label>
                        <select id="pergunta_resposta_edit" name="pergunta_resposta_edit" class="form-control">
                            <?php
                            $sql = "SELECT * FROM pergunta";
                            $res_pergunta_resposta_edit = mysqli_query($conn, $sql);
                            while ($row = mysqli_fetch_array($res_pergunta_resposta_edit)) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['pergunta'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="example-date-input" class=" col-form-label" style="padding:0px">Digite a resposta:</label>
                        <input type="text" id="resposta_edit" name="resposta_edit" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="example-date-input" class=" col-form-label" style="padding:0px">Selecione o tipo:</label>
                        <select id="tipo_resposta_edit" name="tipo_resposta_edit" class="form-control">
                            <option value="1">Positiva</option>
                            <option value="0">Negativa</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" onclick="editar_pergunta()" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTableRespostas').DataTable({});
    });

    function edit_resposta(id) {
        $.get("php/getedit/get_respostas.php?id=" + id, function(data) {
            var json = JSON.parse(data);
            $("#id_resposta_edit").val(id);
            $("#pergunta_resposta_edit").val(json[0].pergunta);
            $("#resposta_edit").val(json[1].resposta);
            $("#tipo_resposta_edit").val(json[2].tipo);

            $('#modal-edit-resposta').modal('show');
        });
    }

    function editar_pergunta() {
        $('#form-edit-resposta').submit();
    }
</script>