<?php
include_once('../../conn/conexao.php');
$sql = "SELECT 
            COUNT(ed.id) AS quantidade,
            md.motivo as motivo
        FROM edital_declinado AS ed 
        INNER JOIN motivo_declinado AS md ON
            ed.motivo_id = md.id
        GROUP BY md.id
        ORDER BY quantidade ASC
        ";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Relatório de Motivos de declínio</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableInabPart" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="10%">Quantidade</th>
                        <th>Motivo</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th width="10%">Quantidade</th>
                        <th>Motivo</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                            <td><?= $row['quantidade'] ?></td>
                            <td><?= $row['motivo'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTableInabPart').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>