<?php
include_once('../../conn/conexao.php');
$sql = "SELECT 
            COUNT(eac.id) AS quantidade,
            c.razao_social_concorrente AS licitante 
        FROM edital_ata_concorrentes AS eac 
        INNER JOIN concorrentes AS c ON
            eac.concorrente = c.id
        GROUP BY concorrente";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Quantidade de Participações por Licitante</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableRelPartLic" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="10%">Quantidade</th>
                        <th>Licitante</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th width="10%">Quantidade</th>
                        <th>Licitante</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <tr>
                            <td><?= $row['quantidade'] ?></td>
                            <td><?= $row['licitante'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTableRelPartLic').DataTable({
            "order": [[ 0, "desc" ]]
        });
    });
</script>