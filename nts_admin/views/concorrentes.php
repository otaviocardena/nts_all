<?php
include_once('../conn/conexao.php');
$sql = "SELECT * FROM concorrentes";
$res = mysqli_query($conn, $sql);
?>
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Concorrentes
      <button style="float: right;margin-left: 10px" class=" btn btn-primary" onclick="filtrar_concorrente()">Filtrar</button>
      <select style="float: right;margin-left: 10px;width: 30%;" class="form-control" name="filtro_estado_concorrente" id="filtro_estado_concorrente">
        <option value="">Selecione o estado</option>
        <option value="AC">Acre</option>
        <option value="AL">Alagoas</option>
        <option value="AP">Amapá</option>
        <option value="AM">Amazonas</option>
        <option value="BA">Bahia</option>
        <option value="CE">Ceará</option>
        <option value="DF">Distrito Federal</option>
        <option value="ES">Espírito Santo</option>
        <option value="GO">Goiás</option>
        <option value="MA">Maranhão</option>
        <option value="MT">Mato Grosso</option>
        <option value="MS">Mato Grosso do Sul</option>
        <option value="MG">Minas Gerais</option>
        <option value="PA">Pará</option>
        <option value="PB">Paraíba</option>
        <option value="PR">Paraná</option>
        <option value="PE">Pernambuco</option>
        <option value="PI">Piauí</option>
        <option value="RJ">Rio de Janeiro</option>
        <option value="RN">Rio Grande do Norte</option>
        <option value="RS">Rio Grande do Sul</option>
        <option value="RO">Rondônia</option>
        <option value="RR">Roraima</option>
        <option value="SC">Santa Catarina</option>
        <option value="SP">São Paulo</option>
        <option value="SE">Sergipe</option>
        <option value="TO">Tocantins</option>
      </select>
    </h6><br>
    <input type="text" class="form-control" id="cadastracnpj" />
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTableConcorrentes" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>CNPJ</th>
            <th>Razão Social</th>
            <th>Nome Fantasia</th>
            <th>Natureza Jurídica</th>
            <th>Porte Empresa</th>
            <th>CNAE</th>
            <th>Logradouro</th>
            <th>Numero</th>
            <th>Complemento</th>
            <th>Bairro</th>
            <th>Municipio</th>
            <th>Estado</th>
            <th>CEP</th>
            <th>Ativo</th>
            <th width="5%">Editar</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>CNPJ</th>
            <th>Razão Social</th>
            <th>Nome Fantasia</th>
            <th>Natureza Jurídica</th>
            <th>Porte Empresa</th>
            <th>CNAE</th>
            <th>Logradouro</th>
            <th>Numero</th>
            <th>Complemento</th>
            <th>Bairro</th>
            <th>Municipio</th>
            <th>Estado</th>
            <th>CEP</th>
            <th>Ativo</th>
            <th width="5%">Editar</th>
          </tr>
        </tfoot>
        <tbody id="tbody-concorrentes">
          <?php while ($row = mysqli_fetch_array($res)) {
            if ($row['porte_empresa_concorrente'] != "null") {
              $porte = $row['porte_empresa_concorrente'];
            } else {
              $porte = "Sem informação";
            }

            if ($row['ativo_concorrente'] == 0) {
              $status = "Inativo";
            } else {
              $status = "Ativo";
            }
          ?>
            <tr>
              <td><?= $row['cnpj_concorrente'] ?></td>
              <td><?= $row['razao_social_concorrente'] ?></td>
              <td><?= $row['nome_fantasia_concorrente'] ?></td>
              <td><?= $row['natureza_juridica_concorrente'] ?></td>
              <td><?= $porte ?></td>
              <td><?= $row['cnae_concorrente'] ?></td>
              <td><?= $row['logradouro_concorrente'] ?></td>
              <td><?= $row['numero_logradouro_concorrente'] ?></td>
              <td><?= $row['complemento_logradouro_concorrente'] ?></td>
              <td><?= $row['bairro_concorrente'] ?></td>
              <td><?= $row['municipio_concorrente'] ?></td>
              <td><?= $row['estado_concorrente'] ?></td>
              <td><?= $row['cep_concorrente'] ?></td>
              <td><?= $status ?></td>
              <td>
                <center>
                  <button class="btn btn-warning btn-circle" onclick="edit_concorrente(<?= $row['id'] ?>)">
                    <i class="fas fa-edit"></i>
                  </button>
                </center>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-edit-concorrente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edição de Concorrente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-edit-concorrente" method="POST" action="php/edita_concorrente.php">
          <input type="hidden" id="id_concorrente" name="id_concorrente">
          <div class="form-group" style="display:flex">
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">CNPJ:</label>
              <input type="text" id="cnpj_concorrente_edit" name="cnpj_concorrente_edit" class="form-control">
            </div>
            <div style="width: -webkit-fill-available;margin-left: 20px;">
              <label for="recipient-name" class="col-form-label">Razão Social:</label>
              <input type="text" id="razao_social_concorrente_edit" name="razao_social_concorrente_edit" class="form-control">
            </div>
          </div>
          <div class="form-group" style="display:flex">
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Nome Fantasia:</label>
              <input type="text" id="nome_fantasia_concorrente_edit" name="nome_fantasia_concorrente_edit" class="form-control">
            </div>
            <div style="width: -webkit-fill-available; margin-left: 20px;">
              <label for="message-text" class="col-form-label">Natureza Jurídica:</label>
              <input type="text" id="natureza_juridica_concorrente_edit" name="natureza_juridica_concorrente_edit" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Porte da empresa:</label>
              <input type="text" id="porte_empresa_concorrente_edit" name="porte_empresa_concorrente_edit" class="form-control">
            </div>
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">CNAE:</label>
              <input type="text" id="cnae_concorrente_edit" name="cnae_concorrente_edit" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Logradouro:</label>
              <input type="text" id="logradouro_concorrente_edit" name="logradouro_concorrente_edit" class="form-control">
            </div>
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Número:</label>
              <input type="number" step="1" id="numero_logradouro_concorrente_edit" name="numero_logradouro_concorrente_edit" class="form-control">
            </div>
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Complemento:</label>
              <input type="text" id="complemento_logradouro_concorrente_edit" name="complemento_logradouro_concorrente_edit" class="form-control">
            </div>
          </div>
          <div class="form-group" style="display: flex;">
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Bairro:</label>
              <input type="text" id="bairro_concorrente_edit" name="bairro_concorrente_edit" class="form-control">
            </div>
            <div style="margin-left: 20px;width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Município:</label>
              <input type="text" id="municipio_concorrente_edit" name="municipio_concorrente_edit" class="form-control">
            </div>
            <div style="margin-left: 20px;width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Estado:</label>
              <select name="estado_concorrente_edit" id="estado_concorrente_edit" class="form-control">
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
              </select>
            </div>
            <div style="width: -webkit-fill-available;margin-left: 20px;">
              <label for="message-text" class="col-form-label">CEP:</label>
              <input type="text" id="cep_concorrente_edit" name="cep_concorrente_edit" class="form-control">
            </div>
          </div>
          <div class="form-group" style="display: flex;">
            <div style="width: -webkit-fill-available;">
              <label for="message-text" class="col-form-label">Ativo:</label>
              <select id="ativo_concorrente_edit" name="ativo_concorrente_edit" class="form-control">
                <option value="0">Inativo</option>
                <option value="1">Ativo</option>
              </select>
            </div>
            <!-- <div style="width: -webkit-fill-available;margin-left: 20px;">
              <label for="message-text" class="col-form-label">Recadastrado:</label>
              <input type="text" id="recadastrado_concorrente_edit" name="recadastrado_concorrente_edit" class="form-control">
            </div>
            <div style="width: -webkit-fill-available;margin-left: 20px;">
              <label for="message-text" class="col-form-label">Habilitado Licitar:</label>
              <select id="habilitado_licitar_concorrente_edit" name="habilitado_licitar_concorrente_edit" class="form-control">
                <option value="0">Não Habilitado</option>
                <option value="1">Habilitado</option>
              </select>
            </div> -->
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" onclick="editar_concorrente()" class="btn btn-primary">Editar</button>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#dataTableConcorrentes').DataTable({});
  });

  function filtrar_concorrente() {
    var filtro_estado = $('#filtro_estado_concorrente').val();
    $.get('php/filtro_concorrente.php?estado='+ filtro_estado, function(data){
      $('#tbody-concorrentes').html(data);
    })
  }

  function edit_concorrente(id) {
    $.get("php/getedit/get_concorrente.php?id=" + id, function(data) {
      var json = JSON.parse(data);
      $("#id_concorrente").val(id);
      $("#cnpj_concorrente_edit").val(json[0].cnpj);
      $("#razao_social_concorrente_edit").val(json[1].razao_social);
      $("#nome_fantasia_concorrente_edit").val(json[2].nome_fantasia);
      $("#natureza_juridica_concorrente_edit").val(json[3].natureza_juridica);
      $("#porte_empresa_concorrente_edit").val(json[4].porte_empresa);
      $("#cnae_concorrente_edit").val(json[5].cnae);
      $("#logradouro_concorrente_edit").val(json[6].logradouro);
      $("#numero_logradouro_concorrente_edit").val(json[7].numero_logradouro);
      $("#complemento_logradouro_concorrente_edit").val(json[8].complemento_logradouro);
      $("#bairro_concorrente_edit").val(json[9].bairro);
      $("#municipio_concorrente_edit").val(json[10].municipio);
      $("#estado_concorrente_edit").val(json[11].estado);
      $("#cep_concorrente_edit").val(json[12].cep);
      $("#ativo_concorrente_edit").val(json[13].ativo);
      // $("#recadastrado_concorrente_edit").val(json[13].recadastrado);
      // $("#habilitado_licitar_concorrente_edit").val(json[14].habilitado_licitar);

      $('#modal-edit-concorrente').modal('show');
    });
  }

  function editar_concorrente() {
    $('#form-edit-concorrente').submit();
  }

  var input = document.getElementById("cadastracnpj");
  input.addEventListener("keyup", async function(event) {
    if (event.keyCode === 13) {
      await axios({
          method: 'GET',
          url: 'https://api.cnpja.com.br/companies/' + input.value,
          data: {},
          headers: {
            "authorization": "0552132e-4140-4fa7-82f7-ab436ce169b3-463054c6-31fc-4624-8944-12b4b5abe1ee"
          }
        })
        .then(function(response) {
          var formData = new FormData();
          // console.log(response);

          var number = response.data.address.city;
          if (number == "SN") {
            number = 0;
          }

          var ativo = response.data.registration.status;
          if (ativo == "ATIVA") {
            ativo = 1;
          } else {
            ativo = 0;
          }

          formData.append("cnpj_concorrente", response.data.tax_id);
          response.data.name = response.data.name.replace(/'/g, "´");
          formData.append("razao_social_concorrente", response.data.name);
          // response.data.nome_fantasia = response.data.razao_social.replace(/'/g, "´");
          formData.append("nome_fantasia_concorrente", response.data.name);
          formData.append("natureza_juridica_concorrente", response.data.legal_nature.description);
          formData.append("porte_empresa_concorrente", response.data.size);
          formData.append("cnae_concorrente", response.data.primary_activity.description);
          formData.append("logradouro_concorrente", response.data.address.street);
          formData.append("numero_logradouro_concorrente", number);
          formData.append("complemento_logradouro_concorrente", response.data.address.details);
          formData.append("bairro_concorrente", response.data.address.neighborhood);
          formData.append("municipio_concorrente", response.data.address.city);
          formData.append("estado_concorrente", response.data.address.state);
          formData.append("cep_concorrente", response.data.address.zip);
          formData.append("ativo_concorrente", ativo);
          // formData.append("recadastrado_concorrente", response.data.recadastrado);
          // formData.append("habilitado_licitar_concorrente", response.data.habilitado_licitar);

          $.ajax({
            url: "php/cadastra_concorrente.php",
            type: 'POST',
            data: formData,
            success: function(data) {
              alert("Cadastrado com Sucesso!")
            },
            cache: false,
            contentType: false,
            processData: false,
            xhr: function() { // Custom XMLHttpRequest
              var myXhr = $.ajaxSettings.xhr();
              if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                myXhr.upload.addEventListener('progress', function() {

                }, false);
              }
              return myXhr;
            }
          });
        })
        .catch(function(response) {
          console.log(response);
        });
    }
  });
</script>