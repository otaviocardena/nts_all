<?php
session_start();
include_once("conn/conexao.php");

if (!isset($_SESSION['ZWxldHJpY2Ft_adm'])) {
  header("Location: login.php");
}

$sql = "SELECT * FROM orgao";
$res_orgao_doc = mysqli_query($conn, $sql);

$sql = "SELECT * FROM documento_categoria";
$res_categoria_doc = mysqli_query($conn, $sql);

$sql = "SELECT * FROM setor";
$res_setor_pergunta = mysqli_query($conn, $sql);
$res_setor_funcionario = mysqli_query($conn, $sql);
// $res_setor_resposta = mysqli_query($conn, $sql);

$sql = "SELECT * FROM pergunta";
$res_pergunta = mysqli_query($conn, $sql);

$sql = "SELECT * FROM categoria_motivo_declinado";
$res_cat_motivo = mysqli_query($conn, $sql);

$num_notificacoes = 0;

$sql = "SELECT * FROM documento_tipo WHERE TO_DAYS(validade) - TO_DAYS(NOW()) <= 30";
$res_doc_vencimento = mysqli_query($conn, $sql);
$count_doc_vencimento = mysqli_num_rows($res_doc_vencimento);

$sql = "SELECT * FROM orgao_documento WHERE TO_DAYS(validade) - TO_DAYS(NOW()) <= 30";
$res_doc_vencimento2 = mysqli_query($conn, $sql);
$count_doc_vencimento2 = mysqli_num_rows($res_doc_vencimento2);

$num_notificacoes += $count_doc_vencimento;
$num_notificacoes += $count_doc_vencimento2;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="HomePage Painel Administrativo NTS">
  <meta name="author" content="EvolutionSoft Tecnologias LTDA">

  <link href="img/nts1.png" rel="icon">
  <link href="img/nts1.png" rel="apple-touch-icon">

  <title>NTS Engenharia</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-text mx-3"><img src="img/png-logo-nts-white.png" alt=""></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" onclick="page('dashboard')" style="cursor:pointer;">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-file"></i>
          <span>Edital</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Editais:</h6>
            <a class="collapse-item menu-lateral" onclick="page_system('telaedital.php')">Editais Abertos</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaEditalAgendado.html')">Editais Agendados</a>
            <a class="collapse-item menu-lateral" onclick="page_system('teladeclinados.php')">Editais Declinados</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaPendente.html')">Aguardando Parecer</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaImpugnacao.html')">Impugnação</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaEsclarecimento.html')">Esclarecimento</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaAprovado.html')">Aprovados Fase 1</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaAprovado2.html')">Aprovados Fase 2</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaAprovado3.html')">Aprovados Fase 3</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaAguardandoHomologacao.html')">Aguardando Homologação</a>
            <a class="collapse-item menu-lateral" onclick="page_system('telaHomologados.html')">Homologados</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseConsult" aria-expanded="true" aria-controls="collapseConsult">
          <i class="fas fa-pen"></i>
          <span>Cadastro</span>
        </a>
        <div id="collapseConsult" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!-- <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-edital">Edital Manual</a> -->
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-edital-xml">Edital</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-funcionarios">Funcionários</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-concorrente">Concorrentes</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-orgao">Orgãos</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-documento">Documentos</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cat-cadastro-declinio">Categoria de Tipos Declinio</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-declinio">Tipos Declinio</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-tipocat-documento">Tipo Categoria Documento</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-pergunta">Pergunta</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-resposta">Resposta</a>
            <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-setor">Setor</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-search"></i>
          <span>Consulta</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!-- <a class="collapse-item" href="#" data-toggle="modal" data-target="#modal-cadastro-edital">Edital Manual</a> -->
            <!-- <a class="collapse-item">Edital</a> -->
            <a style="cursor:pointer;" class="collapse-item" onclick="page('funcionarios')">Funcionários</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('concorrentes')">Concorrentes</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('orgaos')">Orgãos</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('orgao_documentos')">Documentos de Orgão</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('cat_declinios')">Categoria de Tipos Declinio</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('declinios')">Tipos Declinio</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('tipos_documentos')">Tipos de Documentos</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('perguntas')">Perguntas</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('respostas')">Respostas</a>
            <a style="cursor:pointer;" class="collapse-item" onclick="page('setores')">Setores</a>
          </div>
        </div>
      </li>

      <li class="nav-item" style="cursor:pointer;">
        <a class="nav-link" onclick="page('relatorios')">
          <i class="fas fa-chart-bar"></i>
          <span>Relatórios</span>
        </a>
      </li>

      <li class="nav-item" style="cursor:pointer;">
        <a class="nav-link" onclick="page('profile')">
          <i class="fas fa-user"></i>
          <span>Perfil</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>



          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <?php if ($num_notificacoes > 0) { ?>
                  <span class="badge badge-danger badge-counter"><?= $num_notificacoes . "+" ?></span>
                <?php } ?>
              </a>
              <!-- Dropdown - Alerts -->
              <div style="max-height: 400px;overflow-y: scroll;" class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Notificações
                </h6>
                <?php while ($row = mysqli_fetch_array($res_doc_vencimento)) { ?>
                  <a class="dropdown-item d-flex align-items-center">
                    <div class="mr-3">
                      <div class="icon-circle bg-primary">
                        <i class="fas fa-file-alt text-white"></i>
                      </div>
                    </div>
                    <div>
                      <div class="small text-gray-500"><?= date('d/m/Y', strtotime($row['validade'])) ?></div>
                      <span class="font-weight-bold">Documento <?= $row['nome'] ?> com vencimento próximo.</span>
                    </div>
                  </a>
                <?php } ?>
                <?php while ($row = mysqli_fetch_array($res_doc_vencimento2)) { ?>
                  <a class="dropdown-item d-flex align-items-center">
                    <div class="mr-3">
                      <div class="icon-circle bg-primary">
                        <i class="fas fa-file-alt text-white"></i>
                      </div>
                    </div>
                    <div>
                      <div class="small text-gray-500"><?= date('d/m/Y', strtotime($row['validade'])) ?></div>
                      <span class="font-weight-bold">Documento <?= $row['titulo'] ?> com vencimento próximo.</span>
                    </div>
                  </a>
                <?php } ?>
                <!-- <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a> -->
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['name_adm'] ?></span>
                <img class="img-profile rounded-circle" src="img/avatar.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" onclick="page('profile')">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid" id="conteudo">



        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; EvolutionSoft 2021</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Deseja sair?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Selecione Logout para encerrar sessão.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="php/logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-edital-xml" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro Edital XML</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="../nts/importa_xml.php" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
            <div>
              <label style="margin-right: 10px; color: #000; font-weight: 700;">Arquivo XML</label> <input style="width: 350px;" type="file" name="arquivo" id="file" accept=".xml">
              <button style="width: 130px;float: right;font-size: 15px;padding: 5px; color: #fff; background: #009e05;" class="btn btn-secundary">Importar</button>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-resposta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro Resposta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-resposta" method="POST" action="php/cadastra_resposta.php">
            <!-- <div class="form-group">
              <label for="example-date-input" class=" col-form-label" style="padding:0px">Primeiro selecione o setor:</label>
              <select id="setor_resposta" name="setor_resposta" class="form-control">
                option
              </select>
            </div> -->
            <div class="form-group">
              <label for="example-date-input" class=" col-form-label" style="padding:0px">Selecione a pergunta:</label>
              <select id="pergunta_resposta" name="pergunta_resposta" class="form-control">
                <?php while ($row = mysqli_fetch_array($res_pergunta)) { ?>
                  <option value="<?= $row['id'] ?>"><?= $row['pergunta'] ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="example-date-input" class=" col-form-label" style="padding:0px">Digite a resposta:</label>
              <input type="text" id="resposta" name="resposta" class="form-control">
            </div>
            <div class="form-group">
              <label for="example-date-input" class=" col-form-label" style="padding:0px">Selecione o tipo:</label>
              <select id="tipo_resposta" name="tipo_resposta" class="form-control">
                <option value="1">Positiva</option>
                <option value="0">Negativa</option>
              </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" onclick="cadastrar_resposta()" class="btn btn-primary">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-pergunta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro Pergunta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-pergunta" method="POST" action="php/cadastra_pergunta.php">
            <div class="form-group">
              <label for="pergunta" class="col-form-label" style="padding:0px">Digite a pergunta que será enviada ao setor:</label>
              <textarea name="pergunta" id="pergunta" cols="30" rows="10" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label for="setor_pergunta" class=" col-form-label" style="padding:0px">Selecione o setor que será enviado:</label>
              <select id="setor_pergunta" name="setor_pergunta" class="form-control">
                <?php while ($row = mysqli_fetch_array($res_setor_pergunta)) { ?>
                  <option value="<?= $row['id'] ?>"><?= $row['setor'] ?></option>
                <?php } ?>
              </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" onclick="cadastrar_pergunta()" class="btn btn-primary">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-funcionarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro Funcionários</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-funcionario" method="POST" action="php/cadastra_funcionario.php">
            <div class="form-group" style="display: flex">
              <div style="width: -webkit-fill-available;margin-right:10px;">
                <label for="nome_funcionario" class="col-form-label" style="padding:0px">Nome:</label>
                <input type="text" name="nome_funcionario" id="nome_funcionario" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 10px;">
                <label for="cpf_funcionario" class="col-form-label" style="padding:0px">CPF:</label>
                <input type="text" name="cpf_funcionario" id="cpf_funcionario" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex">
              <div style="width: -webkit-fill-available;margin-right:10px;">
                <label for="cargo_funcionario" class="col-form-label" style="padding:0px">Cargo:</label>
                <input type="text" name="cargo_funcionario" id="cargo_funcionario" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 10px;">
                <label for="email_funcionario" class="col-form-label" style="padding:0px">Email:</label>
                <input type="text" name="email_funcionario" id="email_funcionario" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex">
              <div style="width: -webkit-fill-available;margin-right:10px;">
                <label for="setor_funcionario" class="col-form-label" style="padding:0px">Setor:</label>
                <select type="text" name="setor_funcionario" id="setor_funcionario" class="form-control">
                  <?php while ($row = mysqli_fetch_array($res_setor_funcionario)) { ?>
                    <option value="<?= $row['id'] ?>"><?= $row['setor'] ?></option>
                  <?php } ?>
                </select>
              </div>
              <div style="width: -webkit-fill-available;margin-left: 10px;">
                <label for="telefone_funcionario" class="col-form-label" style="padding:0px">Telefone:</label>
                <input type="text" name="telefone_funcionario" id="telefone_funcionario" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex">
              <div style="width: -webkit-fill-available;margin-right:10px;">
                <label for="login_funcionario" class="col-form-label" style="padding:0px">Login:</label>
                <input type="text" name="login_funcionario" id="login_funcionario" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 10px;">
                <label for="senha_funcionario" class="col-form-label" style="padding:0px">Senha:</label>
                <input type="password" name="senha_funcionario" id="senha_funcionario" class="form-control">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" onclick="cadastrar_funcionario()" class="btn btn-primary">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- <div class="modal fade bd-example-modal-lg" id="modal-cadastro-edital" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro Manual de Edital</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group" style="display:flex">
              <div style="width: -webkit-fill-available;">
                <label for="recipient-name" class="col-form-label">Numero Edital:</label>
                <input type="text" class="form-control" >
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">Numero Processo:</label>
                <input type="text" class="form-control" >
              </div>
              
            </div>
            <div class="form-group" style="display:flex">
              <div style="width: -webkit-fill-available;">
                <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Entrega:</label>
                <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
              </div>
              <div style="width: -webkit-fill-available; margin-left: 20px;">
                <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Orgão:</label>
                <select class="form-control">
                  <option>Orgão</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Local Obra:</label>
              <input type="text" class="form-control" >
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Objeto:</label>
              <input type="text" class="form-control" >
            </div>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Valor:</label>
                <input type="text" class="form-control" >
              </div>
             <div style="margin-left: 20px;width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Prazo:</label>
                <input type="text" class="form-control" >
             </div>
              
            </div>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Serviço:</label>
                <input type="text" class="form-control" >
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">Especialidade:</label>
                <input type="text" class="form-control" >
              </div>
            </div>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="example-date-input" class="col-2 col-form-label" style="padding-left: 0px;">Registro:</label>
                <input class="form-control" type="date" value="2011-08-19" id="example-date-input"> 
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">Setor:</label>
                <input type="text" class="form-control" >
              </div>
              
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary">Cadastrar</button>
        </div>
      </div>
    </div>
  </div> -->
  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-orgao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Orgão</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-orgao" method="POST" action="php/cadastra_orgao.php">
            <div class="form-group" style="display:flex">
              <div style="width: -webkit-fill-available;">
                <label for="recipient-name" class="col-form-label">Razão Social: <span id="permissao_cadastro_orgao"></span></label>
                <input type="text" id="razao_orgao" name="razao_orgao" class="form-control" onchange="verifica_razao_orgao(this)">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">CNPJ:</label>
                <input type="text" id="cnpj_orgao" name="cnpj_orgao" class="form-control">
              </div>

            </div>
            <div class="form-group" style="display:flex">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Celular:</label>
                <input type="text" id="celular_orgao" name="celular_orgao" class="form-control">
              </div>
              <div style="width: -webkit-fill-available; margin-left: 20px;">
                <label for="message-text" class="col-form-label">Telefone:</label>
                <input type="text" id="telefone_orgao" name="telefone_orgao" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Endereço:</label>
              <input type="text" id="endereco_orgao" name="endereco_orgao" class="form-control">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Site:</label>
              <input type="text" id="site_orgao" name="site_orgao" class="form-control">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">E-mail:</label>
              <input type="text" id="email_orgao" name="email_orgao" class="form-control">
            </div>
            <hr>
            <label for="message-text" class="col-form-label">Dados Responsavel</label>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Responsavel:</label>
                <input type="text" id="responsavel_orgao" name="responsavel_orgao" class="form-control">
              </div>
              <div style="margin-left: 20px;width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">E-mail:</label>
                <input type="text" id="email_responsavel_orgao" name="email_responsavel_orgao" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Fone Responsavel:</label>
                <input type="text" id="telefone_responsavel_orgao" name="telefone_responsavel_orgao" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">Setor:</label>
                <input type="text" id="setor_orgao" name="setor_orgao" class="form-control">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button id="button_cadastrar_orgao" type="button" onclick="cadastrar_orgao()" class="btn btn-primary">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-concorrente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Concorrente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-cad-concorrente" method="POST" action="php/cadastra_concorrente.php">
            <div class="form-group" style="display:flex">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">CNPJ:</label>
                <input type="text" id="cnpj_concorrente" name="cnpj_concorrente" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="recipient-name" class="col-form-label">Razão Social:</label>
                <input type="text" id="razao_social_concorrente" name="razao_social_concorrente" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display:flex">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Nome Fantasia:</label>
                <input type="text" id="nome_fantasia_concorrente" name="nome_fantasia_concorrente" class="form-control">
              </div>
              <div style="width: -webkit-fill-available; margin-left: 20px;">
                <label for="message-text" class="col-form-label">Natureza Jurídica:</label>
                <input type="text" id="natureza_juridica_concorrente" name="natureza_juridica_concorrente" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Porte da empresa:</label>
                <input type="text" id="porte_empresa_concorrente" name="porte_empresa_concorrente" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">CNAE:</label>
                <input type="text" id="cnae_concorrente" name="cnae_concorrente" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Logradouro:</label>
                <input type="text" id="logradouro_concorrente" name="logradouro_concorrente" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Número:</label>
                <input type="number" step="1" id="numero_logradouro_concorrente" name="numero_logradouro_concorrente" class="form-control">
              </div>
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Complemento:</label>
                <input type="text" id="complemento_logradouro_concorrente" name="complemento_logradouro_concorrente" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Bairro:</label>
                <input type="text" id="bairro_concorrente" name="bairro_concorrente" class="form-control">
              </div>
              <div style="margin-left: 20px;width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Município:</label>
                <input type="text" id="municipio_concorrente" name="municipio_concorrente" class="form-control">
              </div>
              <div style="margin-left: 20px;width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Estado:</label>
                <select name="estado_concorrente" id="estado_concorrente" class="form-control">
                  <option value="AC">Acre</option>
                  <option value="AL">Alagoas</option>
                  <option value="AP">Amapá</option>
                  <option value="AM">Amazonas</option>
                  <option value="BA">Bahia</option>
                  <option value="CE">Ceará</option>
                  <option value="DF">Distrito Federal</option>
                  <option value="ES">Espírito Santo</option>
                  <option value="GO">Goiás</option>
                  <option value="MA">Maranhão</option>
                  <option value="MT">Mato Grosso</option>
                  <option value="MS">Mato Grosso do Sul</option>
                  <option value="MG">Minas Gerais</option>
                  <option value="PA">Pará</option>
                  <option value="PB">Paraíba</option>
                  <option value="PR">Paraná</option>
                  <option value="PE">Pernambuco</option>
                  <option value="PI">Piauí</option>
                  <option value="RJ">Rio de Janeiro</option>
                  <option value="RN">Rio Grande do Norte</option>
                  <option value="RS">Rio Grande do Sul</option>
                  <option value="RO">Rondônia</option>
                  <option value="RR">Roraima</option>
                  <option value="SC">Santa Catarina</option>
                  <option value="SP">São Paulo</option>
                  <option value="SE">Sergipe</option>
                  <option value="TO">Tocantins</option>
                </select>
              </div>
              <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">CEP:</label>
                <input type="text" id="cep_concorrente" name="cep_concorrente" class="form-control">
              </div>
            </div>
            <div class="form-group" style="display: flex;">
              <div style="width: -webkit-fill-available;">
                <label for="message-text" class="col-form-label">Ativo:</label>
                <select id="ativo_concorrente" name="ativo_concorrente" class="form-control">
                  <option value="0">Inativo</option>
                  <option value="1">Ativo</option>
                </select>
              </div>
              <!-- <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">Recadastrado:</label>
                <input type="text" id="recadastrado_concorrente" name="recadastrado_concorrente" class="form-control">
              </div> -->
              <!-- <div style="width: -webkit-fill-available;margin-left: 20px;">
                <label for="message-text" class="col-form-label">Habilitado Licitar:</label>
                <select id="habilitado_licitar_concorrente" name="habilitado_licitar_concorrente" class="form-control">
                  <option value="0">Não Habilitado</option>
                  <option value="1">Habilitado</option>
                </select>
              </div> -->
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" onclick="cadastrar_concorrente()" class="btn btn-primary">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-documento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Documento</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-orgao-documento" method="POST" action="php/cadastra_orgao_documento.php" enctype="multipart/form-data">
            <div class="form-group">
              <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Orgão:</label>
              <select class="form-control" id="orgao_documento" name="orgao_documento">
                <?php while ($row = mysqli_fetch_array($res_orgao_doc)) { ?>
                  <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Titulo:</label>
              <input type="text" class="form-control" id="titulo_orgao_documento" name="titulo_orgao_documento">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Número:</label>
              <input type="number" class="form-control" id="numero_orgao_documento" name="numero_orgao_documento">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Validade:</label>
              <input type="date" class="form-control" id="date_orgao_documento" name="date_orgao_documento">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Observação:</label>
              <input type="text" class="form-control" id="observacao_orgao_documento" name="observacao_orgao_documento">
            </div>
            <hr>
            <div class="form-group" style="display: flex;">
              <input onchange="inserir_nome_doc('doc_orgao_documento')" type="file" id="doc_orgao_documento" name="doc_orgao_documento" style="display:none;">
              <div style="cursor:pointer;" onclick="abrir_input_doc('doc_orgao_documento')" class="btn btn-primary">
                <label id="filename_doc_orgao_documento">Anexar Arquivo</label>
              </div>
            </div>
          </form>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" onclick="cadastrar_orgao_doc()">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cat-cadastro-declinio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Categoria de Motivo Declinio</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-cat-motivo-declinio" method="POST" action="php/cadastra_categoria_motivo_declinio.php">
            <div class="form-group">
              <label for="example-date-input" class="col-form-label" style="padding:0px">Nome da categoria:</label>
              <input name="categoria_motivo_declinio" id="categoria_motivo_declinio" class="form-control" placeholder="Nome" />
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" onclick="cadastrar_cat_motivo_declinio()">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-declinio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Motivo Declinio</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-motivo-declinio" method="POST" action="php/cadastra_motivo_declinio.php">
            <div class="form-group">
              <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Declinio:</label>
              <textarea name="motivo_declinio" id="motivo_declinio" cols="30" rows="10" class="form-control" placeholder="Informe o Motivo de Declinio..."></textarea>
            </div>
            <div class="form-group">
              <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Categoria:</label><br>
              <select id="categoria_declinio_edit" name="categoria_declinio_edit" class="form-control">
                <?php while ($row = mysqli_fetch_array($res_cat_motivo)) { ?>
                  <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
                <?php } ?>
              </select>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" onclick="cadastrar_motivo_declinio()">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-tipocat-documento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Tipo Categoria Documento</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group" style="display:flex">
            <div style="width: -webkit-fill-available;">
              <button type="button" onclick="nova_categoria()" class="btn btn-primary">Nova Categoria</button>
              <form>
                <div>
                  <label for="recipient-name" class="col-form-label">Nome Categoria:</label>
                  <input type="text" class="form-control" id="nome_categoria" name="nome_categoria" disabled>
                </div>
                <button style="margin-top: 15px;" type="button" onclick="cadastrar_nova_categoria()" class="btn btn-primary">Cadastrar</button>
              </form>
            </div>
          </div>
          <hr>
          <form id="form-tipo-documento" method="POST" action="php/cadastra_tipo_documento.php" enctype="multipart/form-data">
            <div class="form-group">
              <label for="example-date-input" class="col-2 col-form-label" style="padding:0px">Categoria:</label>
              <select id="categoria_documento" name="categoria_documento" class="form-control">
                <?php while ($row = mysqli_fetch_array($res_categoria_doc)) { ?>
                  <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Tipo Documento:</label>
              <input type="text" id="tipo_documento" name="tipo_documento" class="form-control">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Validade:</label>
              <input type="date" id="validade_documento" name="validade_documento" class="form-control">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Edital Referente:</label>
              <input ondblclick="liberar_input_edital()" type="number" id="edital_documento" name="edital_documento" class="form-control" readonly>
            </div>
            <hr>
            <div class="form-group" style="display: flex;">
              <input onchange="inserir_nome_doc('doc_tipo_documento')" type="file" id="doc_tipo_documento" name="doc_tipo_documento" style="display:none;">
              <button onclick="abrir_input_doc('doc_tipo_documento')" type="button" class="btn btn-primary">
                <label id="filename_doc_tipo_documento">Anexar Arquivo</label>
              </button>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="button" onclick="cadastrar_tipo_documento()" class="btn btn-primary">Cadastrar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="modal-cadastro-setor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cadastro de Setor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-cadastra-setor" method="POST" action="php/cadastra_setor.php">
            <div class="form-group">
              <label class="col-2 col-form-label" style="padding:0px">Nome:</label>
              <input name="nome_setor" id="nome_setor" class="form-control" placeholder="Nome do setor">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" onclick="cadastra_setor()">Cadastrar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Page level plugins -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js@3.2.1/dist/chart.min.js"></script>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>


  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.js" integrity="sha512-otOZr2EcknK9a5aa3BbMR9XOjYKtxxscwyRHN6zmdXuRfJ5uApkHB7cz1laWk2g8RKLzV9qv/fl3RPwfCuoxHQ==" crossorigin="anonymous"></script>

  <script>
    $(document).ready(function() {
      var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#224abe; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
      $("#conteudo").html(data);
      // console.log("b");
      if (window.location.hash.substring(1) != "") {
        // console.log("a");
        var hash = window.location.hash.substring(1);
        window.location.hash = window.location.hash.split(hash)[0];
        console.log(hash);
        $('#conteudo').load(hash);
      } else {
        $('#conteudo').load("views/dashboard.php");
      }

      $('#cpf_funcionario').mask('999.999.999-99');
      $('#telefone_funcionario').mask('(99) 9 9999-9999');
      $('#cnpj_orgao').mask('99.999.999/9999-99');
      $('#celular_orgao').mask('(99) 9 9999-9999');
      $('#telefone_orgao').mask('(99) 9999-9999');
      $('#telefone_responsavel_orgao').mask('(99) 9999-9999');
    });

    function page(p) {
      var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#224abe; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
      $("#conteudo").html(data);
      $.get('views/' + p + '.php', function(data) {
        $('#conteudo').html(data);
      });
    }

    function page_system(p) {
      var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%; color:#224abe; width:5rem; height:5rem;'><span class='sr-only'>Loading...</span></div>";
      $("#conteudo").html(data);
      $.get('../nts/' + p, function(data) {
        $('#conteudo').html(data);
      });
    }

    function verifica_razao_orgao(obj) {
      $.get('php/verifica_razao_orgao.php?razao=' + obj.value, function(data) {
        if (data == "BLOCKED") {
          $('#permissao_cadastro_orgao').html("Já existe órgão com tal razão social.");
          $('#button_cadastrar_orgao').attr('disabled', 'disabled');
        } else {
          $('#permissao_cadastro_orgao').html(" ");
          $('#button_cadastrar_orgao').attr('disabled', 'disabled');
          $('#button_cadastrar_orgao').removeAttr('disabled');
        }
      })
    }

    function liberar_input_edital() {
      $('#edital_documento').removeAttr('readonly');
    }

    function cadastrar_funcionario() {
      $('#form-funcionario').submit();
    }

    function cadastrar_concorrente() {
      $('#form-cad-concorrente').submit();
    }

    function cadastrar_orgao() {
      $('#form-orgao').submit();
    }

    function abrir_input_doc(idinput) {
      $('#' + idinput).click();
    }

    function inserir_nome_doc(idinput) {
      $('#filename_' + idinput).html($('#' + idinput)[0].files[0].name);
    }

    function cadastrar_orgao_doc() {
      $('#form-orgao-documento').submit();
    }

    function cadastrar_motivo_declinio() {
      $('#form-motivo-declinio').submit();
    }

    function cadastrar_cat_motivo_declinio() {
      $('#form-cat-motivo-declinio').submit();
    }

    function cadastra_setor(){
      $('#form-cadastra-setor').submit();
    }

    function nova_categoria() {
      $('#nome_categoria').removeAttr("disabled");
    }

    function cadastrar_nova_categoria() {
      if (document.getElementById('nome_categoria').value.length > 0) {
        var nome = $('#nome_categoria').val();
        $.get("php/cadastra_categoria.php?nome_categoria=" + nome, function(data) {
          var option = document.createElement('option');
          option.value = data;
          option.text = nome;
          var select = document.getElementById('categoria_documento');
          select.appendChild(option);
        });
      } else {
        alert("Sem nome de categoria!");
      }
    }

    function cadastrar_tipo_documento() {
      $('#form-tipo-documento').submit();
    }

    function cadastrar_pergunta() {
      $('#form-pergunta').submit();
    }

    function cadastrar_resposta() {
      $('#form-resposta').submit();
    }

    var closebtns = document.getElementsByClassName("close");
    var i;

    for (i = 0; i < closebtns.length; i++) {
      closebtns[i].addEventListener("click", function() {
        this.parentElement.style.display = 'none';
      });
    }

    $('input[type=file]').change(function() {
      console.log($('input[type=file]').val().replace(/C:\\fakepath\\/i, ''));
    });

    function importar() {

      $.post("../nts/importa_xml.php", {
          name: "Donald Duck",
          city: "Duckburg"
        },
        function(data, status) {
          alert("Data: " + data + "\nStatus: " + status);
        });
    }
  </script>
</body>

</html>