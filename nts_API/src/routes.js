const express = require('express');

const routes = express.Router();
const editalController = require('./Controller/EditalController');
const agendadoController = require('./Controller/AgendadoController');
const etapaController = require('./Controller/EtapaController');
const declinadoController = require('./Controller/DeclinadoController');
const pendenteController = require('./Controller/PendenteController');
const respondidosController = require('./Controller/RespondidosController');
const pendenteadmController = require('./Controller/PendenteAdmController');
const impugnacaoController = require('./Controller/ImpugnacaoController');
const esclarecimentoController = require('./Controller/EsclarecimentoController')
const aprovadosController = require('./Controller/AprovadosController')
const aprovados2Controller = require('./Controller/Aprovados2Controller')
const aprovados3Controller = require('./Controller/Aprovados3Controller')
const homologadosController = require('./Controller/HomologadosController')
const impugnacaoApController = require('./Controller/ImpugnacaoApController')
const esclarecimentoApController = require('./Controller/EsclarecimentoApController')

routes.get("/edital", editalController.index)
routes.get("/editalAgendado", agendadoController.index)
routes.get("/etapa", etapaController.index)
routes.post("/declinado", declinadoController.create)
routes.get("/declinados", declinadoController.index)
routes.post("/agendado", declinadoController.create_agendado)
routes.get("/pendente", pendenteController.index)
routes.get("/respondidos", respondidosController.index)
routes.get("/pendenteadm", pendenteadmController.index)
routes.get("/impugnacao", impugnacaoController.index)
routes.get("/esclarecimento", esclarecimentoController.index)
routes.get("/aprovado", aprovadosController.index)
routes.get("/aprovado2", aprovados2Controller.index)
routes.get("/aprovado3", aprovados3Controller.index)
routes.get("/homologados", homologadosController.homologados)
routes.get("/aguardandoHomologacao", homologadosController.aguardando)
routes.get("/impugnacaoap", impugnacaoApController.index)
routes.get("/esclarecimentoap", esclarecimentoApController.index)





module.exports = routes;
