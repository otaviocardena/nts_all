const knex = require('../DataBase');
const moment = require('moment');

module.exports = {
    async create(req, res) {
        const { fk_id_edital, etapa_declinada, motivo_id, observacao } = req.body;

        try {
            const response = await knex('edital_declinado').insert(
                { fk_id_edital, etapa_declinada, motivo_id, observacao }
            );
            return res.send('Declinado com sucesso!');
        }
        catch (error) {
            console.log(error);
            return res.json({ error });
        }
    },
    async create_agendado(req, res) {
        const { fk_id_edital } = req.body;

        try {
            const response = await knex('edital_agendado').insert(
                { id_edital: fk_id_edital }
            );
            return res.send('Agendado com sucesso!');
        }
        catch (error) {
            console.log(error);
            return res.json({ error });
        }
    },
    async index(req, res) {
        try {
            //SELECT 	* FROM edital AS ed LEFT JOIN	documento_declinado AS dd ON ed.id = dd.fk_id_edital WHERE	dd.id IS NULL
            const response = await knex
                .select('edital.id', 'edital.num_processo', 'edital.prazo', 'edital.edital_id', 'edital.orgao', 'edital.local_obra', 'edital.valor', 'edital.entrega', 'edital.objeto','motivo_declinado.motivo AS motivo_declinado','edital_declinado.observacao')
                .from('edital')
                .innerJoin('edital_declinado', 'edital.id', 'edital_declinado.fk_id_edital')
                .innerJoin('motivo_declinado', 'motivo_declinado.id', 'edital_declinado.motivo_id')
            for (var i = 0; i < response.length; i++) {
                response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY');

                if (response[i]['prazo'] == 0) {
                    response[i]['prazo'] = "Sem informação";
                } else {
                    response[i]['prazo'] = response[i]['prazo'] + " meses";
                }

            }
            return res.json(response);
        }
        catch (error) {
            return res.send(error)
        }
    }
}