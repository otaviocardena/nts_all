const knex = require("../DataBase");
const moment = require("moment");
module.exports = {
    async index(req, res){
        const user = req.query.user;
        try{
            //SELECT 	* FROM edital AS ed LEFT JOIN	documento_declinado AS dd ON ed.id = dd.fk_id_edital WHERE	dd.id IS NULL
            const response = await knex
            .select('edital.id','edital.edital_id', 'edital.orgao', 'edital.local_obra', 'edital.valor', 'edital.registro', 'edital.entrega', 'edital.objeto')
            .from('edital')
            .innerJoin('edital_respondido', 'edital.id', 'edital_respondido.edital_id')
            .where('edital_respondido.user_id', user)
            .groupBy('edital_respondido.edital_id')
            for(var i = 0; i<response.length; i++){
                response[i]['registro'] = moment(response[i]['registro']).format('DD/MM/YYYY');
                response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY');
            }
            return res.json(response);
        }
        catch(error){
            return res.send(error)
        }
    }
}