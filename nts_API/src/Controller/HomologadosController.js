const knex = require("../DataBase");
const moment = require("moment");
module.exports = {
    async homologados(req, res){
        try{
            const response = await knex
            .select('edital.id','edital.edital_id','edital.num_processo' ,'edital.orgao', 'edital.local_obra', 'edital.valor','edital.prazo', 'edital.registro', 'edital.entrega', 'edital.objeto')
            .from('edital')
            .innerJoin('edital_homologado', 'edital.id', 'edital_homologado.id_edital')
            .leftJoin('edital_declinado', 'edital.id', 'edital_declinado.fk_id_edital')
            .whereNull('edital_declinado.id')
            .where('edital_homologado.status',1)
            .groupBy('edital.id')
            for(var i = 0; i<response.length; i++){
                response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY');
                
                response[i]['valor'] = response[i]['valor'].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
                response[i]['valor'] = response[i]['valor'].replace('.',',');
                response[i]['valor'] = response[i]['valor'].replace(',','.');

                response[i]['prazo'] = response[i]['prazo'] == 0 ? "Sem informação" : response[i]['prazo']+" meses";
            }
            return res.json(response);
        }
        catch(error){
            return res.send(error)
        }
    },
    async aguardando(req, res){
        try{
            const response = await knex
            .select('edital.id','edital.edital_id','edital.num_processo' ,'edital.orgao', 'edital.local_obra', 'edital.valor','edital.prazo', 'edital.registro', 'edital.entrega', 'edital.objeto')
            .from('edital')
            .innerJoin('edital_homologado', 'edital.id', 'edital_homologado.id_edital')
            .leftJoin('edital_declinado', 'edital.id', 'edital_declinado.fk_id_edital')
            .whereNull('edital_declinado.id')
            .where('edital_homologado.status',0)
            .groupBy('edital.id')
            for(var i = 0; i<response.length; i++){
                response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY');
                
                response[i]['valor'] = response[i]['valor'].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
                response[i]['valor'] = response[i]['valor'].replace('.',',');
                response[i]['valor'] = response[i]['valor'].replace(',','.');

                response[i]['prazo'] = response[i]['prazo'] == 0 ? "Sem informação" : response[i]['prazo']+" meses";
            }
            return res.json(response);
        }
        catch(error){
            return res.send(error)
        }
    }
}