const knex = require("../DataBase");
const moment = require("moment");
module.exports = {
    async index(req, res){
        try{
            //SELECT 	* FROM edital AS ed LEFT JOIN	documento_declinado AS dd ON ed.id = dd.fk_id_edital WHERE	dd.id IS NULL
            const response = await knex
            .select('edital.id','edital.num_processo','edital.prazo','edital.edital_id', 'edital.orgao', 'edital.local_obra', 'edital.valor', 'edital.entrega', 'edital.objeto')
            .from('edital')
            .innerJoin('edital_agendado', 'edital.id', 'edital_agendado.id_edital')
            .groupBy('edital.id')
            .orderBy('edital.entrega')
            .leftJoin('edital_impugnacao', 'edital.id', 'edital_impugnacao.edital_id')
            .leftJoin('edital_esclarecimento', 'edital.id', 'edital_esclarecimento.edital_id')
            .leftJoin('edital_declinado', 'edital.id', 'edital_declinado.fk_id_edital')
            .leftJoin('edital_pendente',  'edital.id', 'edital_pendente.edital_id')
            .leftJoin('edital_aprovado',  'edital.id', 'edital_aprovado.edital_id')
            .whereNull('edital_impugnacao.id')
            .whereNull('edital_esclarecimento.id')
            .whereNull('edital_pendente.id')
            .whereNull('edital_declinado.id')
            .whereNull('edital_aprovado.id')
            for(var i = 0; i<response.length; i++){
                response[i]['registro'] = moment(response[i]['registro']).format('DD/MM/YYYY');
                response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY');

                if(response[i]['prazo']==0){
                    response[i]['prazo'] = "Sem informação";
                }else{
                    response[i]['prazo'] = response[i]['prazo'] + " meses";
                }
                
            }
            return res.json(response);
        }
        catch(error){
            return res.send(error)
        }
    },
    create(req, res){
        res.send("Criado com Sucesso")
    }
}