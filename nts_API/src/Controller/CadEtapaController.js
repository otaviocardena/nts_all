const knex = require("../DataBase");

module.exports = {
    async update(req, res){
        const id = req.body.id
        try{
            const response = await knex.knex('edital')
            .where({id})
            .update({
              status: 'archived',
              thisKeyIsSkipped: undefined
            })
        }
        catch{

        }

    }
}