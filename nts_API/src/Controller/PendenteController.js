const knex = require("../DataBase");
const moment = require("moment");
module.exports = {
    async index(req, res){
        const setor = req.query.setor;
        try{
            //SELECT 	* FROM edital AS ed LEFT JOIN	documento_declinado AS dd ON ed.id = dd.fk_id_edital WHERE	dd.id IS NULL
            const response = await knex
            .select('edital.id','edital.edital_id', 'edital.orgao', 'edital.local_obra', 'edital.valor', 'edital.registro', 'edital.entrega', 'edital.objeto')
            .from('edital')
            .innerJoin('edital_pendente', 'edital.id', 'edital_pendente.edital_id')
            .innerJoin('pergunta', 'pergunta.id', 'edital_pendente.fk_pergunta')
            .where('pergunta.fk_setor', setor)
            .where('edital_pendente.status', 0)
            .groupBy('edital.id')
            for(var i = 0; i<response.length; i++){
                response[i]['registro'] = moment(response[i]['registro']).format('DD/MM/YYYY');
                response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY');
            }
            return res.json(response);
        }
        catch(error){
            return res.send(error)
        }
    },
    create(req, res){
        res.send("Criado com Sucesso")
    }
}