const knex = require("../DataBase");
const moment = require("moment");
module.exports = {
    async index(req, res){
        try{
            
            const response = await knex
            .select('edital.id','edital.edital_id','edital.num_processo', 'edital.orgao', 'edital.local_obra', 'edital.valor', 'edital.prazo', 'edital.entrega', 'edital.objeto')
            .from('edital')
            .leftJoin('edital_impugnacao', 'edital.id', 'edital_impugnacao.edital_id')
            .leftJoin('edital_esclarecimento', 'edital.id', 'edital_esclarecimento.edital_id')
            .leftJoin('edital_declinado', 'edital.id', 'edital_declinado.fk_id_edital')
            .leftJoin('edital_pendente',  'edital.id', 'edital_pendente.edital_id')
            .leftJoin('edital_aprovado',  'edital.id', 'edital_aprovado.edital_id')
            .leftJoin('edital_agendado', 'edital.id', 'edital_agendado.id_edital')
            .whereNull('edital_impugnacao.id')
            .whereNull('edital_esclarecimento.id')
            .whereNull('edital_pendente.id')
            .whereNull('edital_declinado.id')
            .whereNull('edital_aprovado.id')
            .whereNull('edital_agendado.id')
            for(var i = 0; i<response.length; i++){
                response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY - hh:mm');

                response[i]['valor'] = response[i]['valor'].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
                response[i]['valor'] = response[i]['valor'].replace('.',',');
                response[i]['valor'] = response[i]['valor'].replace(',','.');

                response[i]['prazo'] = response[i]['prazo'] == 0 ? "Sem informação" : response[i]['prazo']+" meses";
            }

            return res.json(response);
        }
        catch(error){
            return res.send(error)
        }
    },
    create(req, res){
        res.send("Criado com Sucesso")
    }

}