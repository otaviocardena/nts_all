const knex = require("../DataBase");
const moment = require("moment");
module.exports = {
    async index(req, res){
        const setor = req.query.setor;
        try{
            
            const response = await knex
            .select('edital.id','edital.edital_id', 'edital.orgao', 'edital.local_obra', 'edital.valor', 'edital.registro', 'edital.entrega', 'edital.objeto')
            .from('edital')
            .innerJoin('edital_impugnacao', 'edital.id', 'edital_impugnacao.edital_id')
            .groupBy('edital.id')
            .where('edital_impugnacao.status',1)
            for(var i = 0; i<response.length; i++){
                response[i]['registro'] = moment(response[i]['registro']).format('DD/MM/YYYY');
                response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY');
            }
            return res.json(response);
        }
        catch(error){
            return res.send(error)
        }
    },
    create(req, res){
        res.send("Criado com Sucesso")
    }
}