
const knex = require("../DataBase");
const moment = require("moment");

module.exports = {
    async index(req, res){
        try {
            const response = await knex
            .select('edital.id','edital.num_processo','edital.prazo', 'edital.edital_id', 'edital.orgao', 'edital.local_obra', 'edital.valor', 'edital.registro', 'edital.entrega', 'edital.objeto')
            .from('edital')
            .innerJoin('edital_pendente', 'edital.id', 'edital_pendente.edital_id')
            .leftJoin('edital_respondido', 'edital.id', 'edital_respondido.edital_id')
            .groupBy('edital.id');
            
            for(var i = 0; i< response.length; i++){
                try {
                    response[i]['status'] = "Respondido";
                    response[i]['color'] = "green";

                    const status1 = await knex.select('*').from('edital_pendente').where('status','=',0).where('edital_id','=',response[i].id);
                     
                    if(status1.length > 0){
                       response[i]['status'] = "Aguardando resposta";
                        response[i]['color'] = "orange";
                    }

                    const status2 = await knex.select('*').from('edital_impugnacao').where('status','=',0).where('edital_id','=',response[i].id);
                    
                    if(status2.length > 0){
                        response[i]['status'] = "Aguardando impugnação";
                        response[i]['color'] = "orange";
                    }

                    const status3 = await knex.select('*').from('edital_esclarecimento').where('status','=',0).where('edital_id','=',response[i].id);
                    
                    if(status3.length > 0){
                        response[i]['status'] = "Aguardando esclarecimento";
                        response[i]['color'] = "orange";
                    }
                

                    var valorTotal= 0;
                    var response2 = await knex
                    .select('ep.status')
                    .sum('er.valor as valor')
                    .table('setor')
                    .leftJoin('pergunta', 'setor.id', 'pergunta.fk_setor')
                    .leftJoin(knex.select("*").table("edital_pendente").where('edital_id','=',response[i].id).as("ep"),'pergunta.id','ep.fk_pergunta' )
                    .leftJoin(knex.select("*").table("edital_respondido").where('edital_id','=',response[i].id).as("er"),'pergunta.id','er.fk_pergunta' )
                    .groupBy("setor.id");
                    //console.log(response2);
                    for(var j = 0; j < response2.length;j++){
                        if(response2[j]['status'] == null){
                           valorTotal+= 20;
                        }else{
                            if(response2[j]['valor']  == null){
                                valorTotal += 0;
                            }
                            else{
                                valorTotal+=response2[j]['valor'];
                            }
                        }
                        //console.log(j+'-' +response2[j]['valor'])

                    }
                    response[i]['valor'] = valorTotal.toFixed(2);
                    let soma_aux =0;
                    let soma = response2.map(function(item){
                        return soma_aux += item.valor;
                    });
                    //response[i]['valor'] = soma[response2.length -1];
                    
                    response[i]['entrega'] = moment(response[i]['entrega']).format('DD/MM/YYYY');
                    response[i]['registro'] = moment(response[i]['registro']).format('DD/MM/YYYY');

                    response[i]['prazo'] = response[i]['prazo'] + " meses";
                } catch (error) {
                   console.log(error)
                }
                

                
            }
               
            
            return res.json(response);
        }
        catch(error){
            console.log('saiu')
            return res.send(error)
        }
    }
}