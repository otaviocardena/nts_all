async function getPendente(setor) {
    await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/pendente?setor='+setor,
    data: {},
    headers: {}
    })
    .then(function (response) {
        response.data.forEach(function(name){
          const div = document.createElement("div");
          div.innerHTML =  "<div class='box-content shadow'> <img src='img/edital.png' alt='' style='height: 100px;width: 100px;margin: 35px 0px 25px 20px;'><div style=' padding:30px; width: 60%;'> <label><Strong>Orgão:</Strong></label> <span>"+name.orgao+"</span><br> <label><Strong>Local:</Strong></label> <span>"+name.local_obra+"</span><br> <label><Strong>Nº Edital:</Strong></label> <span>"+name.edital_id+"</span><br></div><div style=' padding: 30px;'> <label><strong>Data Fim:</strong></label> <span>"+name.entrega+"</span><br> <label><strong>Data Envio:</strong></label> <span>"+name.registro+"</span></label><br> <label><strong>Status:</strong></label> <span style='color: #a4a700;'>Pendente</span><br> <a href='edital_resposta.php?id=" + name.id + "' class='btn btn-primary btn-user btn-block'> Abrir </a></div></div>";
  
          document.getElementById("seleciona-edital").appendChild(div);
        });
    })
    .catch(function (response) {
      console.log(response);
    });
  }

  async function getRespondidos(user) {
    await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/respondidos?user='+user,
    data: {},
    headers: {}
    })
    .then(function (response) {
        response.data.forEach(function(name){
          const div = document.createElement("div");
          div.innerHTML =  "<div class='box-content shadow'> <img src='img/edital.png' alt='' style='height: 100px;width: 100px;margin: 35px 0px 25px 20px;'><div style=' padding:30px; width: 60%;'> <label><Strong>Orgão:</Strong></label> <span>"+name.orgao+"</span><br> <label><Strong>Local:</Strong></label> <span>"+name.local_obra+"</span><br> <label><Strong>Nº Edital:</Strong></label> <span>"+name.edital_id+"</span><br></div><div style=' padding: 30px;'> <label><strong>Data Fim:</strong></label> <span>"+name.entrega+"</span><br> <label><strong>Data Envio:</strong></label> <span>"+name.registro+"</span></label><br> <label><strong>Status:</strong></label> <span style='color: green;'>Respondido</span><br> <a href='edital_respondido.php?id=" + name.id + "' class='btn btn-primary btn-user btn-block'> Abrir </a></div></div>";
  
          document.getElementById("seleciona-edital-respondido").appendChild(div);
        });
    })
    .catch(function (response) {
      console.log(response);
    });
  }