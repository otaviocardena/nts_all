<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
include_once('../conn/conexao.php');

$id_edital = $_POST['id_edital'];
$user = $_POST['user'];
$observacao = $_POST['observacao'];
$cont_perguntas = $_POST['cont_perguntas'];
$valor = 20/$cont_perguntas;

//INSERIR OBSERVACAO DE EDITAL EM EDITAL_OBSERVACAO
$sql = "INSERT INTO edital_observacao(edital_id,user_id,observacao)
            VALUES($id_edital,$user,'$observacao')";
$res = mysqli_query($conn, $sql);

if ($res) {
    for ($i = 1; $i <= $cont_perguntas; $i++) {
        //PEGA A RESPOSTA SELECIONADA DO FORM
        echo $id_resposta = $_POST['radio_resp_' . $i];
        $id_pergunta = $_POST['input_perg_'.$i];
        //ENCONTRAR O TIPO DA RESPOSTA
        echo $sql = "SELECT tipo FROM resposta WHERE id=$id_resposta";
        $res = mysqli_query($conn, $sql);

        while ($row = mysqli_fetch_array($res)) {
            $tipo = $row[0];
        }

        if ($tipo == 0) {
            $valor_resp = 0;
        } else if ($tipo == 1) {
            $valor_resp = $valor;
        }

        if ($res) {
            //INSERIR EM EDITAL_RESPONDIDO
            $sql = "INSERT INTO edital_respondido(edital_id,fk_pergunta,fk_resposta,user_id,valor)
                    VALUES($id_edital,$id_pergunta,$id_resposta,$user,$valor_resp)";
            $res = mysqli_query($conn, $sql);
        } else {
            echo "ERRO EM SELECIONAR TIPO DA RESPOSTA";
            
        }
        
        $data_retorno = date("Y-m-d H:i:s");
        //MUDAR STATUS DO EDITAL_PENDENTE
        $sql = "UPDATE edital_pendente SET status = 1, data_retorno = '$data_retorno' WHERE edital_id=$id_edital AND fk_pergunta=$id_pergunta";
        $res = mysqli_query($conn, $sql);
        if($res){
            header("Location: ../recebidos.php");
        }else{
            echo "ERRO NO UPDATE DO EDITAL_PENDENTE";
        }
    }
    
} else {
    echo "ERRO EM EDITAL_OBSERVACAO";
}
