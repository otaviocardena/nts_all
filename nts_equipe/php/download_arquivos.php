<?php
include_once('../conn/conexao.php');
$id = $_GET['id_arquivo'];
// $id = 5;
$sql = "SELECT * FROM edital_arquivo WHERE id = $id";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
    $arquivo = $row['arquivo'];
    $filename = $row['nome_arquivo'];
}

$decoded = base64_decode($arquivo);
$file = $filename;
file_put_contents($file, $decoded);

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    unlink($file);
    exit;
}