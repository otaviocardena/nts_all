<?php
session_start();

include_once('conn/conexao.php');

$id = $_GET['id'];
$setor = $_SESSION['setor_eqp'];
$id_user = $_SESSION['ZWxldHJpY2Ft_eqp'];

$sql = "SELECT 
            p.id as pergunta_id,
            p.pergunta
        FROM edital_pendente as ep
        INNER JOIN pergunta as p on
        ep.fk_pergunta = p.id
        WHERE 
            ep.edital_id = $id AND
            p.fk_setor = $setor
        ";
$resPerguntas = mysqli_query($conn, $sql);
$cont_perguntas = mysqli_num_rows($resPerguntas);

$sql = "SELECT * FROM edital WHERE id = $id";
$res = mysqli_query($conn, $sql);

$sqlDoc = "select * from edital_documento where edital_id = $id";
$resDoc = mysqli_query($conn, $sqlDoc);

while ($row = mysqli_fetch_array($resDoc)) {
    $ficheiro = $row['file'];
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NTS</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/style.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <div style="position: fixed;">
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="recebidos.php">

                    <div class="sidebar-brand-text mx-3"><img src="img/png-logo-nts-white.png" alt=""></div>
                </a>



                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Edital
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item active">
                    <a class="nav-link" href="recebidos.php">
                        <i class="fas fa-fw fa-file-alt"></i>
                        <span>Editais Recebidos</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="respondidos.php">
                        <i class="fas fa-fw fa-file-contract"></i>
                        <span>Editais Respondidos</span></a>
                </li>

            </div>
            <!-- Sidebar - Brand -->

            <!-- Nav Item - Utilities Collapse Menu 
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Utilities</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom Utilities:</h6>
                        <a class="collapse-item" href="utilities-color.html">Colors</a>
                        <a class="collapse-item" href="utilities-border.html">Borders</a>
                        <a class="collapse-item" href="utilities-animation.html">Animations</a>
                        <a class="collapse-item" href="utilities-other.html">Other</a>
                    </div>
                </div>
            </li>-->

            <!-- Divider -->

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>


                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['name_eqp'] ?></span>
                                <img class="img-profile rounded-circle" src="img/avatar.png">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="profile.php">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Perfil
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Sair
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="box-content shadow" style="display: block;    padding: 20px;">
                        <div style="width: 100%; text-align: -webkit-center;">
                            <a onclick="window.history.back()" class="btn btn-primary btn-user btn-block" style="width: 100px; position: absolute;">
                                Voltar
                            </a>
                            <img src="img/edital.png" alt="" style="height: 100px;width: 100px;"><br><br>
                        </div>
                        <div class="dados-edital" style="display: block;">
                            <div class="form-row">
                                <?php while ($row = mysqli_fetch_array($res)) { ?>
                                    <div class="col-6">
                                        <label for=""><strong>Nº Edital:</strong> <?= $row['edital_id'] ?></label><br>
                                        <label for=""><strong>Órgão:</strong> <?= $row['orgao'] ?></label><br>
                                        <label for=""><strong>Local:</strong> <?= $row['local_obra'] ?></label><br>
                                        <label for=""><strong>Cad. Órgão:</strong> NÃO </label><br>
                                        <label for=""><strong>Objeto:</strong> <?= $row['objeto'] ?></label>
                                    </div>
                                    <div class="col-6">
                                        <label for=""><strong>Data Fim:</strong> <?= date('d/m/Y', strtotime($row['entrega'])) ?></label><br>
                                        <label for=""><strong>Data Envio:</strong> <?= date('d/m/Y', strtotime($row['registro'])) ?> </label><br>
                                        <label for=""><strong>Status:</strong> <span style="color: #a2be00;">Pendente</span></label>
                                        <form id="form-arquivo-edital" action="php/insere_arquivos.php" method="POST" enctype="multipart/form-data">
                                            <div style="display:flex; margin-bottom:10px;">
                                                <input type="hidden" id="id_edital_arquivo" name="id_edital_arquivo" value="<?= $id ?>">
                                                <input onchange="inserir_arquivo_edital()" type="file" name="arquivo_edital" id="arquivo_edital" style="display:none;">
                                                <div style="width: 40%;margin-right:5px;">
                                                    <a class="btn btn-primary btn-user btn-block" data-toggle="modal" data-target="#baixarArq" style="margin-top:3%;border-radius:55px;">
                                                        Baixar arquivos
                                                    </a>
                                                </div>
                                                <div style="width: 40%;margin-left:5px;">
                                                    <a onclick="click_arquivos()" class="btn btn-primary btn-user btn-block" style="margin-top:3%;border-radius:55px;">
                                                        Inserir arquivos
                                                    </a>
                                                </div>
                                        </form>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <hr class="solid">
                        <div style="text-align: -webkit-center;">
                            <embed src="data:application/pdf;base64,<?php echo $ficheiro; ?>" type="application/pdf" frameborder="0" style="margin: 0;padding: 0;height: 43vh;width: 96%;padding-right: 15px;">
                            </embed>
                        </div>
                        <hr class="solid">
                        <form action="php/responder_edital.php" method="POST">
                            <input type="hidden" name="id_edital" value="<?= $id ?>">
                            <input type="hidden" name="cont_perguntas" value="<?= $cont_perguntas ?>">
                            <input type="hidden" name="user" value="<?= $id_user ?>">
                            <div id="accordion">
                                <?php
                                $cont = 1;
                                while ($row = mysqli_fetch_array($resPerguntas)) {
                                    $id_pergunta = $row['pergunta_id']; ?>
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                                <div style="text-decoration: none;" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_<?= $cont ?>" aria-expanded="false" aria-controls="collapse_<?= $cont ?>">
                                                    <?= $row['pergunta'] ?>
                                                </div>
                                            </h5>
                                        </div>
                                        <div id="collapse_<?= $cont ?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <input type="hidden" name="input_perg_<?= $cont ?>" value="<?= $id_pergunta ?>">
                                            <div class="card-body">
                                                <!--RADIO BUTTONS-->
                                                <?php
                                                $sql = "SELECT id as id_resposta,resposta FROM resposta WHERE fk_pergunta = $id_pergunta";
                                                $resRespostas = mysqli_query($conn, $sql);
                                                while ($row_resp = mysqli_fetch_array($resRespostas)) {
                                                ?>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="radio_resp_<?= $cont ?>" id="radio_resp_<?= $cont ?>" value="<?= $row_resp['id_resposta'] ?>" required>
                                                        <label class="form-check-label" for="radio_resp_<?= $cont ?>"><?= $row_resp['resposta'] ?></label>
                                                    </div><br>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                <?php
                                    $cont++;
                                } ?>
                            </div>
                            <div class="form-group" style="padding-top: 20px;">
                                <label for="observacao"><strong>Observações</strong></label>
                                <textarea class="form-control" id="observacao" name="observacao" rows="3" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-user btn-block" style="width: 100px;">
                                Enviar
                            </button>
                        </form>

                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; EvolutionSoft 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!----------- baixar arquivos------------->
    <div class="modal fade" id="baixarArq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="max-width: 730px !important;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Arquivos - Edital Nº: <?= $id ?></h5>
                </div>
                <div class="modal-body" style="background: #E2E2E2;">
                    <div id="accordion-votacao" class="acc-widthh" style="height: 45vh;padding: 10px;">
                        <?php
                        $sql = "SELECT * FROM edital_arquivo WHERE edital_id = $id";
                        $res_edital_arquivos = mysqli_query($conn, $sql);

                        while ($row = mysqli_fetch_array($res_edital_arquivos)) {
                        ?>
                            <div class="form-group" style="background: #fff;border-radius: 25px;padding: 20px; margin-bottom: 5px;position:relative">
                                <div style="display:flex;">
                                    <div style="align-self: center;">
                                        <img style="width: 40px;" src="image/edital_aberto2.png" alt="">
                                    </div>

                                    <div style="display:block;align-self: center;margin-left: 10px;">
                                        <p style="margin: 0px;"><?= $row['nome_arquivo'] ?></p>
                                    </div>
                                    <a href="php/download_arquivos.php?id_arquivo=<?= $row['id'] ?>" class="btn btn-primary" style="padding-left: 6px;font-size: 25px;position: absolute;width: 45px;right: 19px;top: 7px;">
                                        <i class="fas fa-cloud-download-alt"></i>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----------- baixar ------------->

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/style.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>

</body>

<script>
    function click_arquivos(){
        $('#arquivo_edital').click();
    }

    function inserir_arquivo_edital(){
        $('#form-arquivo-edital').submit();
    }
</script>

</html>