<?php
session_start();

include_once('conn/conexao.php');

function get_nome_setor($id)
{
  global $conn;
  $sql = "SELECT setor FROM setor WHERE id=$id";
  $res = mysqli_query($conn, $sql);

  while ($row = mysqli_fetch_array($res)) {
    $setor = $row[0];
  }
  return $setor;
}

$sessao = $_SESSION["ZWxldHJpY2Ft_eqp"];
$setor = $_SESSION['setor_eqp'];

$sql = "SELECT * FROM usuario WHERE id = $sessao";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
  $cargo = $row['cargo'];
  $email = $row['email'];
  $cpf = $row['cpf'];
  $telefone = $row['telefone'];
}

$sql = "SELECT 
          ep.id 
        FROM edital_pendente AS ep 
        INNER JOIN pergunta AS p ON 
          ep.fk_pergunta = p.id 
        WHERE 
          p.fk_setor = $setor AND
          ep.status = 0
        GROUP BY ep.edital_id";
$resPendente = mysqli_query($conn, $sql);
$pendentes = mysqli_num_rows($resPendente);

$sql = "SELECT 
          er.id 
        FROM edital_respondido AS er
        INNER JOIN pergunta AS p ON 
          er.fk_pergunta = p.id 
        WHERE 
          p.fk_setor = $setor AND
          er.user_id = $sessao
        GROUP BY er.edital_id";
$resRespondidos = mysqli_query($conn, $sql);
$respondidos = mysqli_num_rows($resRespondidos);

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>NTS</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/style.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <div style="position: fixed;">
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="recebidos.php">

          <div class="sidebar-brand-text mx-3"><img src="img/png-logo-nts-white.png" alt=""></div>
        </a>




        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
          Edital
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item active">
          <a class="nav-link" href="recebidos.php">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Editais Recebidos</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="respondidos.php">
            <i class="fas fa-fw fa-file-contract"></i>
            <span>Editais Respondidos</span></a>
        </li>

      </div>
      <!-- Sidebar - Brand -->

      <!-- Nav Item - Utilities Collapse Menu 
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Utilities</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom Utilities:</h6>
                        <a class="collapse-item" href="utilities-color.html">Colors</a>
                        <a class="collapse-item" href="utilities-border.html">Borders</a>
                        <a class="collapse-item" href="utilities-animation.html">Animations</a>
                        <a class="collapse-item" href="utilities-other.html">Other</a>
                    </div>
                </div>
            </li>-->

      <!-- Divider -->

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['name_eqp'] ?></span>
                <img class="img-profile rounded-circle" src="img/avatar.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="profile.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="card shadow" style="align-items: center;">
            <div class="card-body" style="display: flex;align-items:center">
              <div class="d-flex flex-column align-items-center text-center" style="width: fit-content;">
                <img src="img/avatar.png" alt="Admin" class="rounded-circle" width="150">
              </div>
              <div class="mt-3" style="margin-left: 35px;">
                <div class="form-row">
                  <div class="col">
                    <h4 style="margin:0"><?php echo $_SESSION['name_eqp'] ?></h4>
                    <p><b><?php echo "Setor ". get_nome_setor($_SESSION['setor_eqp']) ?></b></p>
                  </div>
                </div>
                <p class="text-secondary mb-1" style="margin:0"><?= $cargo ?></p>
                <p class="text-muted mb-1" style="margin:0"><?= $email ?></p>
                <p class="text-muted mb-1" style="margin:0"><strong>CPF: </strong><?= $cpf ?></p>
                <p class="text-muted mb-1" style="margin:0"><strong>Telefone: </strong><?= $telefone ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="row" style="padding-top: 30px; display:flex; align-items:center; justify-content:center;">

          <div class="col-xl-4 col-md-12 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Editais Respondidos</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $respondidos ?></div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-file-alt fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xl-4 col-md-12 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Editais Pendentes</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $pendentes ?></div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-comments fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Content Row -->

        <div class="row">
          <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; EvolutionSoft 2020</span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->

      </div>
      <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/style.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>