<?php
session_start();
if (!isset($_SESSION['ZWxldHJpY2Ft_adm'])) {
  header("Location: ../nts_admin/login.php");
}
error_reporting(0);
require_once('conn/conexao.php');
$id_edital = $_GET['id'];
$sql = "SELECT * from edital where id = $id_edital";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $edital_id = $row['edital_id'];
    $num_processo = $row['num_processo'];
    $entrega = $row['entrega'];
    $orgao = $row['orgao'];
    $local_obra = $row['local_obra'];
    $objeto = $row['objeto'];
    $valor = $row['valor'];
    $servico = $row['servico'];
    $especialidade = $row['especialidade'];
    $setor = $row['setor'];
    $registro = $row['registro'];
}

$sqlDoc = "select file from orgao_documento where fk_orgao = $id_edital";
$resDoc = mysqli_query($conn, $sqlDoc);

while ($row = mysqli_fetch_array($resDoc)) {
    $ficheiro = $row[0];
}

$sqlCat = "select id as id_cat,nome as nome_cat from documento_categoria";
$resCat = mysqli_query($conn, $sqlCat);



// function getPerguntas($id_setor)
// {
//     global $conn;
//     global $countTotal;

//     $sql = "SELECT * FROM pergunta WHERE fk_setor = $id_setor";
//     $res = mysqli_query($conn, $sql);
//     $countTotal += mysqli_num_rows($res);

//     $array_perguntas = array();

//     while ($row = mysqli_fetch_array($res)) {
//         array_push($array_perguntas, $row['pergunta']);
//     }

//     return $array_perguntas;
// }

// $sqlPergunta8 = "SELECT * FROM pergunta WHERE fk_setor = 8";
// $resPergunta8 = mysqli_query($conn,$sqlPergunta8);
// $countPergunta8 = mysqli_num_rows($resPergunta8);

// $sqlPergunta9 = "SELECT * FROM pergunta WHERE fk_setor = 9";
// $resPergunta9 = mysqli_query($conn,$sqlPergunta9);
// $countPergunta9 = mysqli_num_rows($resPergunta9);

// $sqlPergunta10 = "SELECT * FROM pergunta WHERE fk_setor = 10";
// $resPergunta10 = mysqli_query($conn,$sqlPergunta10);
// $countPergunta10 = mysqli_num_rows($resPergunta10);
$sql = "SELECT * FROM setor";
$res_setores = mysqli_query($conn, $sql);

$sql = "SELECT 
            COUNT(p.id) 
        FROM pergunta AS p
        INNER JOIN setor AS s ON
            s.id = p.fk_setor";
$res_perguntas_total = mysqli_query($conn, $sql);
$countTotal = mysqli_num_rows($res_perguntas_total);

?>
<!DOCTYPE html>
<html lang="pt-Br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=11">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Tela Solicitação de Pareceres</title>

    <link href="image/nts1.png" rel="icon">
    <link href="image/nts1.png" rel="apple-touch-icon">

    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

    <style>
        @font-face {
            font-family: gotham;
            src: url(font/GothamMedium.ttf);
        }

        @font-face {
            font-family: gotham-bold;
            src: url(font/Gotham-Bold.otf);
        }

        span {
            font-family: gotham-bold;
            color: #000;
        }

        * {
            font-family: gotham;
        }

        body {
            background: #E5E5E5;
        }

        .box-content {
            margin: 30px;
            height: 225px;
            display: flex;
            font-size: 13px;
            color: #000;
            border: 1px solid #000;
            box-shadow: 2px rgba(0, 0, 0, 0.9);
        }

        .close {
            cursor: pointer;
            position: absolute;
            right: 0%;
            height: 25px;
            padding: 0px 5px;
            margin: 5px 35px 50px 0px;
        }

        .close:hover {
            background: #bbb;
            border-radius: 50px;
        }

        li {
            margin-bottom: 5px;
        }
    </style>
</head>
<!--<div id="preloader">
</div>-->

<body id="page-top">
    <div id="wrapper" style="display: block;">
        <!--<div id="seleciona-edital"></div>-->
        <div class="box-content shadow" style="padding-bottom:0px;padding: 20px;">
            <div class="form-row">
                <div class="col">
                    <div class="form-row">
                        <div class="col-2" style="display: flex;align-items: center;justify-content: center;">
                            <img src="image/edital_aberto.png" alt="" style="height: 100px;width: 200px;    align-self: center;">
                        </div>
                        <div class="col">
                            <div style="margin-right: 20px;margin-left: 20px;">
                                <label>Nº Edital: <span><?php echo $edital_id; ?></span></label><br>
                                <label>Orgão: <span><?php echo $orgao; ?></span> </label><br>
                                <label>Local: <span><?php echo $local_obra; ?></span></label><br>
                                <label>Cad. Orgão: <span>NÃO</span></label><br><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <label>Valor: <span><?php echo "R$ " . number_format($valor, 2, ',', '.'); ?></span></label><br>
                    <label>Prazo de execução: <span><?= $prazo ?></span></label><br>
                    <label>Data Entrega: <span><?php echo date('d/m/Y', strtotime($entrega)); ?></span></label><br>
                    <label>Processo: <span><?= $num_processo ?></span></label><br>
                </div>
                <label style="padding: 0px 10px;">Objeto: <span><?php echo $objeto; ?></span></label>
            </div>
        </div>
    </div>
    <form action="php/solicitarParecerAction.php" id="form-perguntas" method="POST">
        <input type="hidden" value="<?= $id_edital ?>" name="edital_id">
        <input type="hidden" value="<?= $countTotal ?>" name="count_total" id="count_total">
        <input type="hidden" value="" id="ids_perguntas" name="ids_perguntas">
        <div class="container-fluid">
            <div class="row" style="height: 50%;">
                <div style="width: 100%">
                    <div class="box-content-votacao1">
                        <div id="accordion" style="height: 45vh; display: flex;background-color: #c4c4c4;">
                            <?php
                            $countCheck = 1;
                            while ($row = mysqli_fetch_array($res_setores)) { ?>
                                <div class="coluna">
                                    <div class="title-coluna">
                                        <?= $row['setor'] ?>
                                    </div>
                                    <div class="questions">
                                        <div style="color: #000">
                                            <?php
                                            $id_setor = $row['id'];
                                            $sql = "SELECT * FROM pergunta WHERE fk_setor = $id_setor";
                                            $res_pergunta = mysqli_query($conn, $sql);
                                            while ($row_pergunta = mysqli_fetch_array($res_pergunta)) { ?>
                                                <input style="margin:20px 10px 0px 10px" value="<?= $row_pergunta['id'] ?>" type="checkbox" id="checkbox_<?= $countCheck ?>" name="checkbox_<?= $countCheck ?>"><?= $row_pergunta['pergunta'] ?><br>
                                            <?php
                                                $countCheck++;
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>



            <div class="row" style=" height: 38%;">
                <div class="col-xl-12 col-lg-12">
                    <div class="box-content-votacao" style="box-shadow: none;">
                        <div style="display: flex;background:#e5e5e5;">
                            <button id="button-form" style="background:#C4C4C4;border: none; text-align: center;color:red; margin-right: 20px; box-shadow: 2px 5px 11px rgb(0 0 0 / 50%);" type="button" onclick=goBack() class="btn btn-secondary">Voltar</button>
                            <button id="button-form" style="background:#C4C4C4;border: none; color: #07B204; text-align: center; box-shadow: 2px 5px 11px rgb(0 0 0 / 50%);" type="butotn" onclick="solicitar()" class="btn btn-primary">Enviar</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/fuctions.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        var closebtns = document.getElementsByClassName("close");
        var i;

        for (i = 0; i < closebtns.length; i++) {
            closebtns[i].addEventListener("click", function() {
                this.parentElement.style.display = 'none';
            });
        }
    </script>

</body>
<script>
    getEdital();

    function goBack() {
        window.history.back();
    }
    var string_ids = "";

    function solicitar() {
        var contagem = $('#count_total').val();

        for (var i = 1; i <= contagem; i++) {
            if ($('#checkbox_' + i).is(':checked')) {
                // 1;3;
                string_ids += $('#checkbox_' + i).val() + ";";
            }
        }

        $('#ids_perguntas').val(string_ids);

        console.log(string_ids);
        console.log(contagem);
        $("#form-perguntas").submit();

    }
</script>

</html>