<?php
session_start();
if (!isset($_SESSION['ZWxldHJpY2Ft_adm'])) {
  header("Location: ../nts_admin/login.php");
}

require_once('conn/conexao.php');
$id = $_GET['id'];

//FUNÇÕES
function getNota($setor, $id)
{
  global $conn;
  $sql = "SELECT 
            sum(er.valor) 
          FROM edital_respondido as er
          INNER JOIN usuario as u on
          er.user_id = u.id
          INNER JOIN setor as s on
          u.fk_setor = s.id
          WHERE 
            s.setor = '$setor' AND
            er.edital_id = $id";
  $res = mysqli_query($conn, $sql);

  while ($row = mysqli_fetch_array($res)) {
    $nota = $row[0];
  }
  if (!isset($nota)) {
    $nota = 20;
  }
  return $nota;
}

function getObs($setor, $id)
{
  global $conn;
  $sql = "SELECT 
            eo.observacao 
          FROM edital_respondido as er
          INNER JOIN usuario as u on
          er.user_id = u.id
          INNER JOIN setor as s on
          u.fk_setor = s.id
          INNER JOIN edital_observacao as eo on
          eo.user_id = u.id
          WHERE 
            s.setor = '$setor' AND
            er.edital_id = $id";
  $res = mysqli_query($conn, $sql);

  while ($row = mysqli_fetch_array($res)) {
    $obs = $row[0];
  }
  if (!isset($obs)) {
    $obs = "Sem observação de setor.";
  }
  return $obs;
}
//STATUS DO EDITAL
$status = "Respondido";
$color = "green";

$sql = "SELECT * FROM edital_pendente WHERE status = 0 AND edital_id = $id";
$res = mysqli_query($conn, $sql);
if (mysqli_num_rows($res) > 0) {
  $status = "Aguardando resposta";
  $color = "orange";
}
$sql = "SELECT * FROM edital_impugnacao WHERE edital_id = $id AND status = 0";
$res = mysqli_query($conn, $sql);
if (mysqli_num_rows($res) > 0) {
  $status = "Aguardando impugnação";
  $color = "orange";
}
$sql = "SELECT * FROM edital_esclarecimento WHERE edital_id = $id AND status = 0";
$res = mysqli_query($conn, $sql);
if (mysqli_num_rows($res) > 0) {
  $status = "Aguardando esclarecimento";
  $color = "orange";
}

// DOCUMENTOS COLLAPSE
$contagem_docs = 0;
$array_ids_docs = array();
$sql = "SELECT 
                id,
                edital_id,
                num_processo,
                prazo,
                entrega,
                orgao,
                local_obra,
                objeto,
                valor,
                servico,
                especialidade,
                setor,
                registro 
            FROM edital WHERE id = $id";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
  $id_id = $row['id'];
  $edital_id = $row['edital_id'];
  $num_processo = $row['num_processo'];
  $prazo = $row['prazo'];
  if ($prazo == 0) {
    $prazo = "Sem informação";
  } else {
    $prazo .= " Meses";
  }
  $entrega = $row['entrega'];
  $orgao = $row['orgao'];
  $local_obra = $row['local_obra'];
  $objeto = $row['objeto'];
  $valor = $row['valor'];
  $servico = $row['servico'];
  $especialidade = $row['especialidade'];
  $setor = $row['setor'];
  $registro = $row['registro'];
}

$sqlDoc = "select * from edital_documento where edital_id = $id";
$resDoc = mysqli_query($conn, $sqlDoc);

while ($row = mysqli_fetch_array($resDoc)) {
  $ficheiro = $row['file'];
}

$sqlCat = "select id as id_cat,nome as nome_cat from documento_categoria";
$resCat = mysqli_query($conn, $sqlCat);

//SQLS PARA MODAL DE NOTAS DE ACORDO COM SETOR
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Financeiro' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorFinanceiro = mysqli_query($conn, $sql);
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Estratégico' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorEstrategico = mysqli_query($conn, $sql);
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Técnico' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorTecnico = mysqli_query($conn, $sql);
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Jurídico' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorJuridico = mysqli_query($conn, $sql);
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Administrativo' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorAdministrativo = mysqli_query($conn, $sql);

$sql = "SELECT * FROM categoria_motivo_declinado";
$res_categoria_motivo_declinio = mysqli_query($conn, $sql);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=11">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Tela de editais com esclarecimento solicitado">
  <meta name="author" content="EvolutionSoft Tecnologias LTDa">

  <link href="image/nts1.png" rel="icon">
  <link href="image/nts1.png" rel="apple-touch-icon">

  <title>Tela Edital Esclarecimento</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">

  <style>
    @font-face {
      font-family: gotham;
      src: url(font/GothamMedium.ttf);
    }

    @font-face {
      font-family: gotham-bold;
      src: url(font/Gotham-Bold.otf);
    }

    span {
      font-family: gotham-bold;
      color: #000;
    }

    * {
      font-family: gotham;
    }

    body {
      background: #E5E5E5;
    }

    .box-content {
      margin: 30px;
      height: auto;
      padding-bottom: 20px;
      display: flex;
      font-size: 13px;
      color: #000;
      border: 1px solid #000;
      box-shadow: 2px rgba(0, 0, 0, 0.9);
    }

    .close {
      cursor: pointer;
      position: absolute;
      right: 0%;
      height: 25px;
      padding: 0px 5px;
      margin: 5px 35px 50px 0px;
    }

    .close:hover {
      background: #bbb;
      border-radius: 50px;
    }

    li {
      margin-bottom: 5px;
    }

    .file_customizada::-webkit-file-upload-button {
      visibility: hidden;
    }

    .custom-file-input::before {
      content: 'Select some files';
      display: inline-block;
      background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
      border: 1px solid #999;
      border-radius: 3px;
      padding: 5px 8px;
      outline: none;
      white-space: nowrap;
      -webkit-user-select: none;
      cursor: pointer;
      text-shadow: 1px 1px #fff;
      font-weight: 700;
      font-size: 10pt;
    }

    .file_customizada:hover::before {
      border-color: black;
    }

    .file_customizada:active::before {
      background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
    }

    .div-valor-status {
      position: relative;
      width: 100%;
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
  </style>
</head>
<!--<div id="preloader">
</div>-->

<body id="page-top">
  <div id="wrapper" style="display: block;">
    <!--<div id="seleciona-edital"></div>-->
    <a>
      <div class="box-content shadow" style="padding-bottom:0px;padding: 20px;">
        <div class="form-row">
          <div class="col">
            <div class="form-row">
              <div class="col-3" style="display: flex;align-items: center;justify-content: center;">
                <img src="image/edital_aberto.png" alt="" style="height: 100px;width: 120px; align-self: center;">
              </div>
              <div class="col">
                <div style="margin-right: 20px;margin-left: 20px;">
                  <label>Nº Edital: <span><?php echo $edital_id; ?></span></label><br>
                  <label>Orgão: <span><?php echo $orgao; ?></span> </label><br>
                  <label>Local: <span><?php echo $local_obra; ?></span></label><br>
                  <label>Cad. Orgão: <span>NÃO</span></label><br><br>
                </div>
              </div>
            </div>
          </div>
          <div class="col-6">
            <label>Valor: <span><?php echo "R$ " . number_format($valor, 2, ',', '.'); ?></span></label>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
            <label>Status: <span style="color:<?= $color ?>"><?= $status ?></span></label><br>
            <label>Prazo de execução: <span><?= $prazo ?></span></label><br>
            <label>Data Entrega: <span><?php echo date('d/m/Y', strtotime($entrega)); ?></span></label><br>
            <label>Processo: <span><?= $num_processo ?></span></label><br>
          </div>

          <label style="padding: 10px 30px 0px 30px;">Objeto: <span><?php echo $objeto; ?></span></label>
        </div>
      </div>
  </div>
  <form style="margin-left: 20px" action="php/cadastra_documento_edital.php" method="POST" enctype="multipart/form-data" id="form-documento-edital">
    <input type="hidden" value="<?= $id_id ?>" name="id_edital" id="id_edital">
    <input id="documento_edital" name="documento_edital" type="file" onchange="inserir_documento()" style="margin-right:10px;">
  </form>
  </a>
  </div>

  <div class="container-fluid">
    <div class="row" style="height: 50%;">
      <div class="col-xl-8 col-lg-8">
        <div class="box-content-votacao" style="margin-right: 10px;    background: #e5e5e5;box-shadow: none;">


          <!-- C:\Users\otavio\Desktop\CREA SP - Controle de Acesso (Novo)-Fase 1.pdf -->
          <!-- echo ficheiro no src -->
          <!-- COLLAPSED-->
          <div id="accordion-votacao" class="acc-widthh" style="height: 45vh;width: 96%;padding-right: 15px;">
            <div id="conteudo-pautas">
              <embed src="data:application/pdf;base64,<?php echo $ficheiro; ?>" type="application/pdf" frameborder="0" style="margin: 0;padding: 0;height: 43vh;width: 96%;padding-right: 15px;">
              </embed>
            </div>
          </div>

        </div>
      </div>

      <div class="col-xl-4 col-lg-4">
        <div style="display:flex; margin-bottom:10px">
          <button onclick="" id="buttonImpugnar" type="button" data-dismiss="modal" class="btn btn-primary " data-toggle="modal" data-target="#respostaModal" style="text-align: center; text-transform: none; margin-right:10px;">
            Adicionar Resposta
          </button>
        </div>

        <div class="box-content-votacao" style="height: 80%;">
          <div style="padding: 15px 0px 0px 17px; color: #000000;">Selecione os documentos necessários:</div>
          <div id="accordion" style="height: 30vh;">
            <form action="php/aprovarEdital.php" id="form-aprovar" method="POST">
              <input type="hidden" id="id_edital" name="id_edital" value="<?= $id_id ?>">
              <input type="hidden" id="ids_documentos" name="ids_documentos">
              <input type="hidden" id="obsAprovar" name="obsAprovar">
              <?php
              while ($row = mysqli_fetch_array($resCat)) {
                $id = $row['id_cat'];
              ?>

                <div class="card">
                  <div class="card-header" style="padding:0px;" id="heading_<?= $id ?>">
                    <h5 class="mb-0">
                      <div style="margin-bottom: 5px;background: #e6e6e6;text-transform: none;padding-left: 15px;width: 90%;margin-left: 15px;color: #000;font-size: 17px;" class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?= $id ?>" aria-expanded="true" aria-controls="collapse_<?= $id ?>">
                        <?= $row['nome_cat']; ?>
                      </div>
                    </h5>
                  </div>

                  <div id="collapse_<?= $id ?>" class="collapse hide" aria-labelledby="heading_<?= $id ?>" data-parent="#accordion">
                    <div class="card-body" style="background: #e6e6e6;width: 90%;border-radius: 10px;margin-left: 15px;margin-bottom: 5px;color: #000;">
                      <?php
                      $sqlCatDoc = "select id as id_doc,nome as nome_doc from documento_tipo WHERE (id_categoria_doc = $id) AND (id_edital IS NULL OR id_edital=$id)";
                      $resCatDoc = mysqli_query($conn, $sqlCatDoc);
                      while ($row1 = mysqli_fetch_array($resCatDoc)) {
                        $id_doc = $row1['id_doc'];
                        array_push($array_ids_docs, $id_doc);
                        $contagem_docs += 1;
                      ?>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="Check_<?= $id_doc ?>" value="<?= $id_doc ?>">
                          <label style="padding-left: 20px;padding-top: 4px;" class="form-check-label" for="Check_<?= $id_doc ?>">
                            <?= $row1['nome_doc'] ?>
                          </label>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>


    <div class="row" style=" height: 38%;">
      <div class="col-xl-12 col-lg-12">
        <div class="box-content-votacao" style="box-shadow: none;">
          <div style="display: flex;background:#e5e5e5;">
            <button disabled id="button-form" style="margin-right:10px; background:#C4C4C4;border: none; color: red; text-align: center;" onclick='getIdDeclinado(<?php echo $id_id ?>)' type="button" class="btn btn-secondary">Declinar</button>
            <button disabled id="button-form" style="margin-right:10px;background:#C4C4C4;border: none; color: #000; text-align: center;" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#resultadoRes">
              <a href="#<?php echo $id_id ?>" style="text-decoration: none;">verificar respostas</a>
            </button>

            <button disabled id="button-form" style="background:#C4C4C4;border: none; color: #07B204; text-align: center;" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#aprovadoObs">
              Aprovar
            </button>
          </div>

        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="declinio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Deseja declinar o edital: <span id="declinarId"></span>?</h5>

          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="exampleFormControlSelect1">Categoria do Motivo declinio:</label>
            <select onchange="setaMotivoDeclinio(this)" class="form-control" id="selectCatMotivo">
              <option value="">Selecione uma categoria</option>
              <?php while ($row = mysqli_fetch_array($res_categoria_motivo_declinio)) { ?>
                <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Motivo declinio:</label>
            <select class="form-control" id="selectMotivo" readonly>
            </select>
            <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
            <textarea class="form-control" style="cursor: auto;" id="observacaoDeclinio" rows="3"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
          <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
          <button onclick="cadDeclinado()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
            Ok
          </button>

        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="responseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Declinado com sucesso!
          </h5>

          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div id="responseCad" style=" color: #000;">

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
          <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
          <button onclick="location.reload()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
            Ok
          </button>

        </div>
      </div>
    </div>
  </div>

  <!------------------------------------------------------------------------------------------------------------------->
  <!-----------------------------------APROVADO------------------------------------------------------------->
  <div class="modal fade" id="aprovadoObs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Deseja aprovar o edital: <?php echo $id_id ?>?</h5>

          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
            <textarea class="form-control" style="cursor: auto;" id="observacaoAprovado" rows="3"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
          <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
          <button onclick="aprovar()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
            Ok
          </button>

        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="responseModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Aprovado com sucesso!
          </h5>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div id="responseCad" style=" color: #000;">

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
          <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
          <button onclick="location.reload()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
            Ok
          </button>

        </div>
      </div>
    </div>
  </div>
  <!-------------------------resposta------------------------------->
  <div class="modal fade" id="resultadoRes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Edital: <span id="declinarId"></span><?php echo $id_id ?></h5>

          </button>
        </div>
        <div class="modal-body">
          <?php while ($row = mysqli_fetch_array($resSetorFinanceiro)) { ?>
            <div class="setorResultado">
              <div class="lineSetorNota">
                <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                <div class="obsResultado">
                  <label style="color:#000; font-size:12px" for="">Observação:</label>
                  <div class="textObservacaoRes" for="">
                    <label for="">
                      <?= getObs($row['setor'], $id_id) ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <?php while ($row = mysqli_fetch_array($resSetorEstrategico)) { ?>
            <div class="setorResultado">
              <div class="lineSetorNota">
                <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                <div class="obsResultado">
                  <label style="color:#000; font-size:12px" for="">Observação:</label>
                  <div class="textObservacaoRes" for="">
                    <label for="">
                      <?= getObs($row['setor'], $id_id) ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <?php while ($row = mysqli_fetch_array($resSetorTecnico)) { ?>
            <div class="setorResultado">
              <div class="lineSetorNota">
                <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                <div class="obsResultado">
                  <label style="color:#000; font-size:12px" for="">Observação:</label>
                  <div class="textObservacaoRes" for="">
                    <label for="">
                      <?= getObs($row['setor'], $id_id) ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <?php while ($row = mysqli_fetch_array($resSetorJuridico)) { ?>
            <div class="setorResultado">
              <div class="lineSetorNota">
                <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                <div class="obsResultado">
                  <label style="color:#000; font-size:12px" for="">Observação:</label>
                  <div class="textObservacaoRes" for="">
                    <label for="">
                      <?= getObs($row['setor'], $id_id) ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          <?php while ($row = mysqli_fetch_array($resSetorAdministrativo)) { ?>
            <div class="setorResultado">
              <div class="lineSetorNota">
                <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                <div class="obsResultado">
                  <label style="color:#000; font-size:12px" for="">Observação:</label>
                  <div class="textObservacaoRes" for="">
                    <label for="">
                      <?= getObs($row['setor'], $id_id) ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>

        </div>
      </div>
    </div>
  </div>
  <!-------------------------ADICIONAR RESPOSTA----------------------->
  <div class="modal fade" id="respostaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Deseja aprovar o Esclarecimento: <?php echo $id_id ?>
          </h5>
        </div>
        <div class="modal-body">
          <form action="php/aprova_esclarecimento.php" method="POST" id="form-aprova-esclarecimento" enctype="multipart/form-data">
            <input type="hidden" name="id_aprova_edital" value="<?= $id_id ?>">
            <div class="form-group">
              <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="customRadio1" name="radio_aprova" value="1" checked>
                <label class="custom-control-label" for="customRadio1">Aprovar</label>
              </div>
              <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="customRadio2" name="radio_aprova" value="2">
                <label class="custom-control-label" for="customRadio2">Reprovar</label>
              </div>
              <div style="margin-top: 20px;">
                <input id="fileinput" type="file" name="pedido_pdf_esc" style="display:none;" />
                <div id="falseinput" class="btn btn-primary" style="width:150px; height:30px;font-size:10px; display:flex;align-items:center;justify-content:center;">
                  <label id="selected_filename" style="color:white;cursor:pointer;">Anexar Resposta PDF</label>
                </div>
              </div>
            </div>
          </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" style="text-align: center; text-transform: none;" onclick="aprovar_esc()">
            Ok
          </button>

        </div>
      </div>
    </div>
  </div>
  <!-------------------------ADICIONAR RESPOSTA----------------------->


  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="js/fuctions.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

  <script>
    $(document).ready(function() {
      $('#falseinput').click(function() {
        $("#fileinput").click();
      });
    });

    $('#fileinput').change(function() {
      $('#selected_filename').html($('#fileinput')[0].files[0].name);
    });

    var closebtns = document.getElementsByClassName("close");
    var i;

    for (i = 0; i < closebtns.length; i++) {
      closebtns[i].addEventListener("click", function() {
        this.parentElement.style.display = 'none';
      });
    }
    var string_ids = ""

    function aprovar() {
      var contagem_docs = <?= $contagem_docs ?>;
      var ids = "<?= implode(",", $array_ids_docs) ?>";
      ids = ids.split(",");

      for (var i = 0; i < contagem_docs; i++) {
        if ($('#Check_' + ids[i]).is(':checked')) {
          // 1;3;
          string_ids += $('#Check_' + ids[i]).val() + ";";
        }
      }

      console.log(string_ids);
      $('#ids_documentos').val(string_ids);
      $('#obsAprovar').val($('#observacaoAprovado').val());

      if (string_ids.length > 0) {
        $('#form-aprovar').submit();
      } else {
        alert("Selecione os documentos para aprovar")
      }
    }

    function inserir_documento() {
      $('#form-documento-edital').submit();
    }

    function aprovar_esc() {
      $('#form-aprova-esclarecimento').submit();
    }

    function setaMotivoDeclinio(obj) {
      $.get('php/get_motivo_por_categoria.php?id_cat=' + obj.value, function(data) {
        $('#selectMotivo').html(data);
        $('#selectMotivo').removeAttr("readonly");
      });
    }

    getEdital();
    getEtapa();
  </script>
</body>

</html>