<?php
session_start();
if (!isset($_SESSION['ZWxldHJpY2Ft_adm'])) {
  header("Location: ../nts_admin/login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="EvolutionSoft Tecnologias LTDA">

  <title>Tela Edital</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">

  <style>
    span{
        font-family: gotham-bold;
        color: #000;
    }
    * {
        font-family: gotham;
    }
    body {
      background: #efefef;
      padding: 20px;
    }
    .box-content{
        margin: 30px; 
        height: 150px;
        display: flex;
        font-size: 13px;
        color: #000;
        border: 1px solid #000;
        box-shadow: 2px rgba(0,0, 0, 0.9) ;
    }
    .close {
        cursor: pointer;
        position: absolute;
        right: 0%;
        height: 25px;
        padding: 0px 5px;
        margin: 5px 35px 50px 0px;
    }

    .close:hover {
        background: #bbb;
        border-radius: 50px;
    }

    table{
      color: #000;
    }

  </style>
</head>
<!--<div id="preloader">
</div>-->
<body id="page-top">
    <div id="wrapper" style="display: block;">
        <div class="outer-container" style="font-size: 14px; color: #000;">
            <form action="importa_xml.php" method="post"
                name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
                <div>
                    <label>Arquivo XML</label> <input style="width: 350px;" type="file" name="arquivo"
                        id="file" accept=".xml">
                    <button style="width: 130px; float: right;font-size: 15px; padding: 5px;color: #fff;background: #009e05;" class="btn btn-secundary">Importar</button>
                </div>
            </form>

              <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div style="    max-width: 80%;margin: 5px;" class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Upload XML</h5>
                    </div>
                    <div class="modal-body">
                      <?php echo $_SESSION['msg'];?>
                    </div>
                    <div class="modal-footer">
                      <button style="font-size: 12px;width: 75px;padding: 10px;" type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                    </div>
                  </div>
                </div>
              </div>
            
         
        </div>
    </div>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/fuctions.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        var closebtns = document.getElementsByClassName("close");
        var i;

        for (i = 0; i < closebtns.length; i++) {
        closebtns[i].addEventListener("click", function() {
            this.parentElement.style.display = 'none';
        });
        }

        $('input[type=file]').change(function () {
              console.log($('input[type=file]').val().replace(/C:\\fakepath\\/i, ''));
          });
        function importar(){
         
          $.post("importa_xml.php",
            {
              name: "Donald Duck",
              city: "Duckburg"
            },
            function(data, status){
              alert("Data: " + data + "\nStatus: " + status);
            });
        }

        <?php
          if(isset($_SESSION['msg'])){ ?>
          $('#exampleModalCenter').modal('show');
          <?php }?>
    </script>
    
</body>
</html>