<?php
require_once('conn/conexao.php');
$id = $_GET['id'];
$contagem_docs = 0;

$sql = "SELECT 
                id,
                edital_id,
                num_processo,
                entrega,
                orgao,
                local_obra,
                objeto,
                valor,
                servico,
                especialidade,
                setor,
                registro 
            FROM edital WHERE id = $id";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_id = $row['id'];
    $edital_id = $row['edital_id'];
    $num_processo = $row['num_processo'];
    $entrega = $row['entrega'];
    $orgao = $row['orgao'];
    $local_obra = $row['local_obra'];
    $objeto = $row['objeto'];
    $valor = $row['valor'];
    $servico = $row['servico'];
    $especialidade = $row['especialidade'];
    $setor = $row['setor'];
    $registro = $row['registro'];
}

$sql = "SELECT
            dc.id as id_categoria,
            dc.nome as categoria
        FROM edital_aprovado as ea
        INNER JOIN documento_tipo as dt on
        ea.pdf_id = dt.id
        INNER JOIN documento_categoria as dc on
        dt.id_categoria_doc = dc.id
        WHERE ea.edital_id = $id
        GROUP BY dc.id
        ORDER BY dc.id
        ";
$res = mysqli_query($conn, $sql);


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=11">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Tela Edital</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

    <style>
        @font-face {
            font-family: gotham;
            src: url(font/GothamMedium.ttf);
        }

        @font-face {
            font-family: gotham-bold;
            src: url(font/Gotham-Bold.otf);
        }

        span {
            font-family: gotham-bold;
            color: #000;
        }

        * {
            font-family: gotham;
        }

        body {
            background: #E5E5E5;
        }

        .box-content {
            margin: 30px;
            height: auto;
            padding-bottom: 20px;
            display: flex;
            font-size: 13px;
            color: #000;
            border: 1px solid #000;
            box-shadow: 2px rgba(0, 0, 0, 0.9);
        }

        .close {
            cursor: pointer;
            position: absolute;
            right: 0%;
            height: 25px;
            padding: 0px 5px;
            margin: 5px 35px 50px 0px;
        }

        .close:hover {
            background: #bbb;
            border-radius: 50px;
        }

        li {
            margin-bottom: 5px;
        }

        .file_customizada::-webkit-file-upload-button {
            visibility: hidden;
        }

        .custom-file-input::before {
            content: 'Select some files';
            display: inline-block;
            background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
            border: 1px solid #999;
            border-radius: 3px;
            padding: 5px 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
            font-weight: 700;
            font-size: 10pt;
        }

        .file_customizada:hover::before {
            border-color: black;
        }

        .file_customizada:active::before {
            background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
        }

        .documentoPdfList {
            display: flex;
            align-items: center;
        }

        .divButtonsDoc {
            display: flex;
            justify-content: space-evenly;
            align-items: center;
        }
    </style>
</head>
<!--<div id="preloader">
</div>-->

<body id="page-top">
    <div id="wrapper" style="display: block;">
        <a>
            <div class="box-content shadow">
                <img src="image/edital_aberto.png" alt="" style="height: 100px;width: 130px;margin: 35px 0px 25px 20px;">
                <div style="height: 177px; padding:30px; width: 60%;">
                    <label>Nº Edital: <span><?php echo $edital_id; ?></span></label><br>
                    <label>Orgão: <span><?php echo $orgao; ?></span> </label><br>
                    <label>Local: <span><?php echo $local_obra; ?></span></label><br>
                    <label>Cad. Orgão: <span>NÃO</span></label><br><br>
                </div>
                <div style="height: 100px; padding: 30px;">
                    <label>Valor: <span><?php echo $valor; ?></span></label><br>
                    <label>Data Inicio: <span><?php echo $registro; ?></span></label><br>
                    <label>Data Fim: <span><?php echo $entrega; ?></span></label><br>
                    <label>Objeto: <span><?php echo $objeto; ?></span></label> </div>
            </div>
            <form style="margin-left: 20px" action="php/cadastra_documento_edital.php" method="POST" enctype="multipart/form-data" id="form-documento-edital">
                <input type="hidden" value="<?= $id_id ?>" name="id_edital" id="id_edital">
                <input id="documento_edital" name="documento_edital" type="file" onchange="inserir_documento()" style="margin-right:10px;">
            </form>
        </a>
    </div>

    <div class="container-fluid">
        <div class="row" style="height: 100%;">
            <div class="box-content-votacao" style="height: auto;    width: 100%;">
                <div style="padding: 15px 0px 0px 17px; color: #000000;">Verifique os documentos necessários:</div>
                <div id="accordion" style="height: 50vh;">
                    <form action="php/aprovarEditalFinal.php" method="POST" enctype="multipart/form-data"></form>
                    <input type="hidden" id="qtd_arquivo" name="qtd_arquivo" value="0">
                    <!--começa aq ---------------------->
                    <?php while ($row = mysqli_fetch_array($res)) {
                        $id_cat = $row['id_categoria'] ?>
                        <div>
                            <label id="labelDoc" for=""><?= $row['categoria'] ?></label>
                            <div class="documentoVerificacao" id="div-arquivos">
                                <?php
                                $sql = "SELECT
                                            dt.nome as arquivo
                                        FROM edital_aprovado as ea
                                        INNER JOIN documento_tipo as dt on
                                        ea.pdf_id = dt.id
                                        LEFT JOIN documento_categoria as dc on
                                        dt.id_categoria_doc = dc.id
                                        WHERE 
                                            dc.id = $id_cat AND
                                            ea.edital_id = $id
                                        ";
                                $resArquivos = mysqli_query($conn,$sql);
                                while ($row_arq = mysqli_fetch_array($resArquivos)) {
                                ?>
                                <div class="documentoPdfList" style="display:flex">
                                    <label id="labelDoc2" style="margin-right:10px;"><?= $row_arq['arquivo'] ?></label>
                                    <div class="divButtonsDoc">
                                        <button id="button-form" style="margin-right:4px;" type="button" class="btn btnDocBlack">
                                            <i class="fas fa-arrow-down"></i>
                                        </button>
                                        <button id="button-form" type="button" style="margin-right:4px;" class="btn btnDocBlue">
                                            <i class="fas fa-eye"></i>
                                        </button>
                                        <button id="button-form" type="button" class="btn btnDocGreen" onclick="novo_arquivo()">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <!--termina aq ---------------------->
                </div>
            </div>
        </div>


        <div class="row" style=" height: 38%;">
            <div class="col-xl-12 col-lg-12">
                <div class="box-content-votacao" style="box-shadow: none;height: auto;">
                    <div style="display: flex;background:#e5e5e5;">
                        <button id="button-form" style="margin-right:10px;background:#C4C4C4;border: none; color: #000; text-align: center;" type="button" class="btn btn-secondary">
                            <a href="telaPendente.html" style="text-decoration: none;">Voltar</a>
                        </button>

                        <button id="button-form" style="background:#C4C4C4;border: none; color: #07B204; text-align: center;" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#aprovadoObs">
                            Aprovar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="responseModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Aprovado com sucesso!
                    </h5>

                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="responseCad" style=" color: #000;">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                    <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
                    <button onclick="location.reload()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
                        Ok
                    </button>

                </div>
            </div>
        </div>
    </div>



    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/fuctions.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</body>
<script>
    var quantidade_arquivo = 0;

    function novo_arquivo() {
        var div_row = document.createElement("div");
        div_row.setAttribute('class', 'documentoPdfList');
        div_row.setAttribute('id', 'div-arquivo-' + quantidade_arquivo);


        $.get("php/get_arquivo_novo.php?id=" + quantidade_arquivo, function(data) {
            div_row.innerHTML = data;
            quantidade_arquivo++;
            document.getElementById('qtd_arquivo').value = quantidade_arquivo;
        });


        document.getElementById('div-arquivos').appendChild(div_row);
    }

    function remove_arquivo(id) {
        var div = document.getElementById('div-arquivo-' + id);
        div.parentNode.removeChild(div);
        quantidade_arquivo--;
        document.getElementById('qtd_arquivo').value = quantidade_arquivo;
    }
</script>

</html>