<?php
session_start();
if (!isset($_SESSION['ZWxldHJpY2Ft_adm'])) {
  header("Location: ../nts_admin/login.php");
}

require_once('conn/conexao.php');
$id = $_GET['id'];
$contagem_docs = 0;
$array_ids_docs = array();
//STATUS DO EDITAL
$sql = "SELECT * FROM edital_pendente WHERE status = 0 AND edital_id = $id";
$res = mysqli_query($conn, $sql);
if (mysqli_num_rows($res) > 0) {
  $status = "Aguardando resposta";
  $color = "green";
}
$sql = "SELECT * FROM edital_impugnacao WHERE edital_id = $id AND status = 0";
$res = mysqli_query($conn, $sql);
if (mysqli_num_rows($res) > 0) {
  $status = "Aguardando impugnação";
  $color = "yellow";
}
$sql = "SELECT * FROM edital_esclarecimento WHERE edital_id = $id AND status = 0";
$res = mysqli_query($conn, $sql);
if (mysqli_num_rows($res) > 0) {
  $status = "Aguardando esclarecimento";
  $color = "yellow";
}

$sql = "SELECT * FROM edital_agendado WHERE id_edital = $id";
$res = mysqli_query($conn, $sql);
if (mysqli_num_rows($res) > 0) {
  $status = "Agendado";
  $color = "blue";
}

//SELECT DE INFORMAÇÕES DO EDITAL
$sql = "SELECT 
          *
        FROM edital WHERE id = $id";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
  $id_id = $row['id'];
  $edital_id = $row['edital_id'];
  $num_processo = $row['num_processo'];
  $prazo = $row['prazo'];
  if ($prazo == 0) {
    $prazo = "Sem informação";
  } else {
    $prazo .= " Meses";
  }
  $entrega = $row['entrega'];
  $orgao = $row['orgao'];
  $local_obra = $row['local_obra'];
  $objeto = $row['objeto'];
  $valor = $row['valor'];
  $servico = $row['servico'];
  $especialidade = $row['especialidade'];
  $setor = $row['setor'];
  $registro = $row['registro'];
  $link_drive = $row['link_drive'];
}

$sqlDoc = "select * from edital_documento where edital_id = $id";
$resDoc = mysqli_query($conn, $sqlDoc);

while ($row = mysqli_fetch_array($resDoc)) {
  $ficheiro = $row['file'];
}

$sqlCat = "select id as id_cat,nome as nome_cat from documento_categoria";
$resCat = mysqli_query($conn, $sqlCat);

//VERIFICAÇÃO SE JA FOI IMPUGNADO/ESCLARECIDO
$countImpEsc = 0;

$sql = "SELECT * FROM edital_impugnacao WHERE edital_id = $id AND status = 0";
$resImp = mysqli_query($conn, $sql);

$sql = "SELECT * FROM edital_esclarecimento WHERE edital_id = $id AND status = 0";
$resEsc = mysqli_query($conn, $sql);

$countImpEsc = mysqli_num_rows($resImp) + mysqli_num_rows($resEsc);
if ($countImpEsc > 0) {
  $disabled = "disabled";
} else {
  $disabled = "";
}

$sql = "SELECT * FROM categoria_motivo_declinado";
$res_categoria_motivo_declinio = mysqli_query($conn, $sql);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=11">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Tela de editais abertos ou agendados pendentes de verificação.">
  <meta name="author" content="EvolutionSoft Tecnologias LTDA">


  <title>Tela Edital Aberto</title>


  <link href="image/nts1.png" rel="icon">
  <link href="image/nts1.png" rel="apple-touch-icon">

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">

  <style>
    @font-face {
      font-family: gotham;
      src: url(font/GothamMedium.ttf);
    }

    @font-face {
      font-family: gotham-bold;
      src: url(font/Gotham-Bold.otf);
    }

    span {
      font-family: gotham-bold;
      color: #000;
    }

    * {
      font-family: gotham;
    }

    body {
      background: #E5E5E5;
    }

    .box-content {
      margin: 30px;
      height: auto;
      padding-bottom: 20px;
      display: flex;
      font-size: 13px;
      color: #000;
      border: 1px solid #000;
      box-shadow: 2px rgba(0, 0, 0, 0.9);
    }

    .close {
      cursor: pointer;
      position: absolute;
      right: 0%;
      height: 25px;
      padding: 0px 5px;
      margin: 5px 35px 50px 0px;
    }

    .close:hover {
      background: #bbb;
      border-radius: 50px;
    }

    li {
      margin-bottom: 5px;
    }

    .file_customizada::-webkit-file-upload-button {
      visibility: hidden;
    }

    .custom-file-input::before {
      content: 'Select some files';
      display: inline-block;
      background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
      border: 1px solid #999;
      border-radius: 3px;
      padding: 5px 8px;
      outline: none;
      white-space: nowrap;
      -webkit-user-select: none;
      cursor: pointer;
      text-shadow: 1px 1px #fff;
      font-weight: 700;
      font-size: 10pt;
    }

    .file_customizada:hover::before {
      border-color: black;
    }

    .file_customizada:active::before {
      background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
    }

    .div-valor-status {
      position: relative;
      width: 100%;
      display: flex;
      justify-content: space-between;
      align-items: center;
    }

    .hide {
      display: none;
    }

    .show {
      display: block;
    }
  </style>
</head>
<!--<div id="preloader">
</div>-->

<body id="page-top">
  <div id="wrapper" style="display: block;">
    <!--<div id="seleciona-edital"></div>-->
    <a>
      <div class="box-content shadow" style="padding-bottom:0px;padding: 20px;">
        <div class="form-row">
          <div class="col">
            <div class="form-row">
              <div class="col-2" style="display: flex;align-items: center;justify-content: center;">
                <img src="image/edital_aberto.png" alt="" style="height: 100px;width: 200px;    align-self: center;">
              </div>
              <div class="col">
                <div style="margin-right: 20px;margin-left: 20px;">
                  <label>Nº Edital: <span><?php echo $edital_id; ?></span></label><br>
                  <label>Orgão: <span><?php echo $orgao; ?></span> </label><br>
                  <label>Local: <span><?php echo $local_obra; ?></span></label><br>
                  <label>Cad. Orgão: <span>NÃO</span></label><br><br>
                </div>
              </div>
            </div>
          </div>
          <div class="col">
            <label>Valor: <span><?php echo "R$ " . number_format($valor, 2, ',', '.'); ?></span></label><br>
            <label>Prazo de execução: <span><?= $prazo ?></span></label><br>
            <label>Data Entrega: <span><?php echo date('d/m/Y', strtotime($entrega)); ?></span></label><br>
            <label>Processo: <span><?= $num_processo ?></span></label><br>
          </div>
          <label style="padding: 0px 10px;">Objeto: <span><?php echo $objeto; ?></span></label>
        </div>
      </div>
    </a>
  </div>

  <div class="container-fluid">
    <div class="row" style="height: 50%;">
      <div class="col-xl-8 col-lg-8">
        <div class="box-content-votacao" style="margin-right: 10px;    background: #e5e5e5;box-shadow: none;">
          <form style="margin-left: 20px" action="php/cadastra_documento_edital.php" method="POST" enctype="multipart/form-data" id="form-documento-edital">
            <input type="hidden" value="<?= $id_id ?>" name="id_edital" id="id_edital">
            <input id="documento_edital" name="documento_edital" type="file" onchange="inserir_documento()" style="margin-right:10px; margin-bottom: 7px;">
          </form>


          <!-- C:\Users\otavio\Desktop\CREA SP - Controle de Acesso (Novo)-Fase 1.pdf -->
          <!-- echo ficheiro no src -->
          <!-- COLLAPSED-->
          <div id="accordion-votacao" class="acc-widthh pdf-enviado">
            <div id="conteudo-pautas">
              <embed src="data:application/pdf;base64,<?php echo $ficheiro; ?>" type="application/pdf" frameborder="0" class="edital-total">
              </embed>
            </div>
          </div>

        </div>
      </div>

      <div class="col-xl-4 col-lg-4">
        <form id="form-arquivo-edital" action="php/insere_arquivos.php" method="POST" enctype="multipart/form-data">
          <div style="display:flex; margin-bottom:10px;">
            <input type="hidden" id="id_edital_arquivo" name="id_edital_arquivo" value="<?= $id_id ?>">
            <input type="hidden" name="tela_arquivo" id="tela_arquivo" value="Aberto">
            <input onchange="inserir_arquivo_edital()" type="file" name="arquivo_edital" id="arquivo_edital" style="display:none;">
            <div id="buttonArquivos" class="btn btn-secondary" style="margin-right:10px;text-align: center; text-transform: none; color:#000;color:#000;background-color: #c3c3c3;">
              Inserir arquivo
            </div>
            <a href="<?= $link_drive ?>" target="_BLANK" class="btn btn-secondary" style="text-align: center; text-transform: none; color:#000;color:#000;background-color: #c3c3c3;">
              Link do Drive
            </a>
          </div>
        </form>
        <div style="display:flex; margin-bottom:10px">
          <button onclick="" id="buttonImpugnar" type="button" data-dismiss="modal" class="btn btn-secondary" data-toggle="modal" data-target="#impugnacaoModal" style="text-align: center; text-transform: none; margin-right:10px; color:#000;color:#000;background-color: #c3c3c3;" <?= $disabled ?>>
            Impugnar
          </button>
          <button onclick="" id="buttonEsclarecimento" type="button" data-dismiss="modal" class="btn btn-secondary" data-toggle="modal" data-target="#esclarecimentoModal" style="text-align: center; text-transform: none; color:#000;background-color: #c3c3c3;" <?= $disabled ?>>
            Esclarecimento
          </button>
        </div>

        <div>
          <div class="box-content-votacao">
            <div style="padding: 15px 0px 0px 17px; color: #000000;">Selecione os documentos necessários:</div>
            <div id="accordion" style="height: 30vh;">
              <form action="php/aprovarEdital.php" id="form-aprovar" method="POST">
                <input type="hidden" id="id_edital" name="id_edital" value="<?= $id_id ?>">
                <input type="hidden" id="ids_documentos" name="ids_documentos">
                <input type="hidden" id="obsAprovar" name="obsAprovar">
                <?php
                while ($row = mysqli_fetch_array($resCat)) {
                  $id = $row['id_cat'];
                ?>

                  <div class="card">
                    <div class="card-header" style="padding:0px;" id="heading_<?= $id ?>">
                      <h5 class="mb-0">
                        <div style="margin-bottom: 5px;background: #e6e6e6;text-transform: none;padding-left: 15px;width: 90%;margin-left: 15px;color: #000;font-size: 17px;" class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?= $id ?>" aria-expanded="true" aria-controls="collapse_<?= $id ?>">
                          <?= $row['nome_cat']; ?>
                        </div>
                      </h5>
                    </div>

                    <div id="collapse_<?= $id ?>" class="collapse hide" aria-labelledby="heading_<?= $id ?>" data-parent="#accordion">
                      <div class="card-body" style="background: #e6e6e6;width: 90%;border-radius: 10px;margin-left: 15px;margin-bottom: 5px;color: #000;">
                        <?php
                        $sqlCatDoc = "select id as id_doc,nome as nome_doc from documento_tipo WHERE (id_categoria_doc = $id) AND (id_edital IS NULL OR id_edital=$id)";
                        $resCatDoc = mysqli_query($conn, $sqlCatDoc);
                        while ($row1 = mysqli_fetch_array($resCatDoc)) {
                          $id_doc = $row1['id_doc'];
                          array_push($array_ids_docs, $id_doc);
                          $contagem_docs += 1;
                        ?>
                          <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="Check_<?= $id_doc ?>" value="<?= $id_doc ?>">
                            <label style="padding-left: 20px;padding-top: 4px;" class="form-check-label" for="Check_<?= $id_doc ?>">
                              <?= $row1['nome_doc'] ?>
                            </label>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </form>
            </div>
          </div>
        </div>
      </div>


      <div class="row" style=" height: 38%; margin-bottom: 0;">
        <div class="col-xl-12 col-lg-12" style="margin-top: 35px;">
          <div class="box-content-votacao" style="box-shadow: none;">
            <div style="display: flex;background:#e5e5e5;">

              <button id="button-form" onclick='getIdDeclinado(<?php echo $id_id ?>)' type="button" class="botao-acoes btn btn-secondary" <?= $disabled ?>>Declinar</button>

              <button id="button-form" type="button" class="botao-acoes acao-parecer btn btn-secondary">
                <a href="solicitarParecer.php?id=<?php echo $id_id ?>">Solicitar Parecer</a>
              </button>

              <button id="button-form" type="button" class="botao-acoes acao-aprovar btn btn-secondary" data-toggle="modal" data-target="#aprovadoObs" <?= $disabled ?>>
                Aprovar
              </button>
            </div>

          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="declinio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Deseja declinar o edital: <span id="declinarId"></span>?</h5>

            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="exampleFormControlSelect1">Categoria do Motivo declinio:</label>
              <select onchange="setaMotivoDeclinio(this)" class="form-control" id="selectCatMotivo">
                <option value="">Selecione uma categoria</option>
                <?php while ($row = mysqli_fetch_array($res_categoria_motivo_declinio)) { ?>
                  <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect1">Motivo declinio:</label>
              <select class="form-control" id="selectMotivo" readonly>
              </select>
              <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
              <textarea class="form-control" style="cursor: auto;" id="observacaoDeclinio" rows="3"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
            <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
            <button onclick="cadDeclinado()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
              Ok
            </button>

          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="responseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Declinado com sucesso!
            </h5>

            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div id="responseCad" style=" color: #000;">

              </div>
            </div>
          </div>
          <div class="modal-footer">
            <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
            <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
            <button onclick="location.reload()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
              Ok
            </button>

          </div>
        </div>
      </div>
    </div>

    <!------------------------------------------------------------------------------------------------------------------->
    <!-----------------------------------APROVADO------------------------------------------------------------->
    <div class="modal fade" id="aprovadoObs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Deseja aprovar o edital: <?php echo $id_id ?>?</h5>

            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
              <textarea class="form-control" style="cursor: auto;" id="observacaoAprovado" rows="3"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
            <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
            <button onclick="aprovar()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
              Ok
            </button>

          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="responseModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500; color: green;">Aprovado com sucesso!
            </h5>

            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div id="responseCad" style=" color: #000;">

              </div>
            </div>
          </div>
          <div class="modal-footer">
            <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
            <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
            <button onclick="location.reload()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
              Ok
            </button>

          </div>
        </div>
      </div>
    </div>

    <!-------------------------IMPUGNAÇÃO-------------------------->
    <div class="modal fade" id="impugnacaoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Registrar Pedido de Impugnação: <span id="declinarId"></span><?php echo $id_id ?>
            </h5>
          </div>
          <div class="modal-body">
            <form action="php/cadastra_impugnacao.php" method="POST" id="form-impugnacao" enctype="multipart/form-data">
              <input type="hidden" name="id_edital" value="<?= $id_id ?>">
              <div class="form-group">
                <div class="form-row">
                  <div class="col">
                    <div class="custom-control custom-radio">
                      <input type="radio" class="custom-control-input" id="customRadio1" name="motivo_imp_1" value="T. de Contas" checked>
                      <label class="custom-control-label" for="customRadio1">T. de Contas</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input type="radio" class="custom-control-input" id="customRadio2" name="motivo_imp_1" value="Orgão Lic.">
                      <label class="custom-control-label" for="customRadio2">Orgão Lic.</label>
                    </div>
                  </div>
                  <div class="col">
                    <div class="custom-control custom-radio">
                      <input type="radio" onclick="abrir_concorrente(this,'imp')" class="custom-control-input" id="customRadio3" name="motivo_imp_2" value="Próprio" checked>
                      <label class="custom-control-label" for="customRadio3">Próprio</label>
                    </div>
                    <div class="custom-control custom-radio">
                      <input type="radio" onclick="abrir_concorrente(this,'imp')" class="custom-control-input" id="customRadio4" name="motivo_imp_2" value="Terceiros">
                      <label class="custom-control-label" for="customRadio4">Terceiros</label>
                    </div>
                  </div>
                </div>
                <div class="form-row" style="margin-top:20px">
                  <div class="hide" style="margin-top: 20px;place-content: space-evenly;width: 100%;" id="div_con_imp">
                    <input style="cursor: auto;margin-bottom:5px;" class="form-control" type="text" name="concorrente_imp" id="concorrente_imp" placeholder="CNPJ do concorrente solicitante">
                    <input style="cursor: auto;" class="form-control" type="text" name="razao_social_imp" id="razao_social_imp" readonly>
                  </div>
                </div>
                <div style="margin-top: 20px;">
                  <input id="fileinput" type="file" name="pedido_pdf_imp" style="display:none;" required />
                  <div id="falseinput" class="btn btn-primary" style="width:150px; height:30px;font-size:10px; display:flex;align-items:center;justify-content:center;">
                    <label id="selected_filename" style="color:white;cursor:pointer;">Anexar Pedido PDF</label>
                  </div>
                </div>

                <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
                <textarea class="form-control" style="cursor: auto;" id="observacaoAprovado" name="obs_imp" rows="3"></textarea>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary" style="text-align: center; text-transform: none;" onclick="impugnar()">
              Ok
            </button>

          </div>
        </div>
      </div>
    </div>
    <!-------------------------IMPUGNAÇÃO-------------------------->

    <!-------------------------ESCLARECIMENTO----------------------->
    <div class="modal fade" id="esclarecimentoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Registrar Pedido de Esclarecimento: <span id="declinarId"></span><?php echo $id_id ?>
            </h5>
          </div>
          <div class="modal-body">
            <form action="php/cadastra_esclarecimento.php" method="POST" id="form-esclarecimento" enctype="multipart/form-data">
              <input type="hidden" name="id_edital" value="<?= $id_id ?>">
              <div class="form-group">
                <div class="custom-control custom-radio">
                  <input onclick="abrir_concorrente(this,'esc')" type="radio" class="custom-control-input" id="customRadio5" name="motivo_esc" value="Próprio" checked>
                  <label class="custom-control-label" for="customRadio5">Próprio</label>
                </div>
                <div class="custom-control custom-radio">
                  <input onclick="abrir_concorrente(this,'esc')" type="radio" class="custom-control-input" id="customRadio6" name="motivo_esc" value="Terceiros">
                  <label class="custom-control-label" for="customRadio6">Terceiros</label>
                </div>
                <div class="form-row" style="margin-top:20px">
                  <div class="hide" style="margin-top: 20px;place-content: space-evenly;width: 100%;" id="div_con_esc">
                    <input style="cursor: auto;margin-bottom:5px;" class="form-control" type="text" name="concorrente_esc" id="concorrente_esc" placeholder="CNPJ do concorrente solicitante">
                    <input style="cursor: auto;" class="form-control" type="text" name="razao_social_esc" id="razao_social_esc" readonly>
                  </div>
                </div>
                <div style="margin-top: 20px;">
                  <input id="fileinput2" type="file" name="pedido_pdf_esc" style="display:none;" required />
                  <div id="falseinput2" class="btn btn-primary" style="width:150px; height:30px;font-size:10px; display:flex;align-items:center;justify-content:center;">
                    <label id="selected_filename2" style="color:white;cursor:pointer;">Anexar Pedido PDF</label>
                  </div>
                </div>
                <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
                <textarea class="form-control" style="cursor: auto;" id="observacaoAprovado" name="obs_esc" rows="3"></textarea>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary" style="text-align: center; text-transform: none;" onclick="esclarecer()">
              Ok
            </button>

          </div>
        </div>
      </div>
    </div>
    <!-------------------------ESCLARECIMENTO----------------------->



    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/fuctions.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>



</body>
<script>
  getEdital();
  getEtapa();
  $(document).ready(function() {
    $('#falseinput').click(function() {
      $("#fileinput").click();
    });
    $('#buttonArquivos').click(function() {
      $("#arquivo_edital").click();
    });
  });

  $('#fileinput').change(function() {
    $('#selected_filename').html($('#fileinput')[0].files[0].name);
  });

  function inserir_arquivo_edital() {
    $('#form-arquivo-edital').submit();
  }

  $(document).ready(function() {
    $('#falseinput2').click(function() {
      $("#fileinput2").click();
    });
  });
  $('#fileinput2').change(function() {
    $('#selected_filename2').html($('#fileinput2')[0].files[0].name);
  });

  $('#concorrente_imp').on("change keyup paste", function() {
    let input = $(this).val();
    if (input.length == 14) {
      $.get('php/get_concorrente.php?cnpj=' + input, function(data) {
        $('#razao_social_imp').val(data);
      });
    }
  });

  $('#concorrente_esc').on("change keyup paste", function() {
    let input = $(this).val();
    if (input.length == 14) {
      $.get('php/get_concorrente.php?cnpj=' + input, function(data) {
        $('#razao_social_esc').val(data);
      });
    }
  });

  function abrir_concorrente(obj, cad) {
    if (obj.value == "Terceiros") {
      document.getElementById('div_con_' + cad).classList.add("show");
      document.getElementById('div_con_' + cad).classList.remove("hide");
      // $('div_con_'+cad).removeClass('hide').addClass('show');
    } else {
      document.getElementById('div_con_' + cad).classList.add("hide");
      document.getElementById('div_con_' + cad).classList.remove("show");
    }
  }


  var closebtns = document.getElementsByClassName("close");
  var i;

  for (i = 0; i < closebtns.length; i++) {
    closebtns[i].addEventListener("click", function() {
      this.parentElement.style.display = 'none';
    });
  }
  var string_ids = "";

  function aprovar() {
    var contagem_docs = <?= $contagem_docs ?>;
    var ids = "<?= implode(",", $array_ids_docs) ?>";
    ids = ids.split(",");

    for (var i = 0; i < contagem_docs; i++) {
      if ($('#Check_' + ids[i]).is(':checked')) {
        // 1;3;
        string_ids += $('#Check_' + ids[i]).val() + ";";
      }
    }

    $('#ids_documentos').val(string_ids);
    $('#obsAprovar').val($('#observacaoAprovado').val());

    if (string_ids.length > 0) {
      $('#form-aprovar').submit();
    } else {
      alert("Selecione os documentos para aprovar")
    }
  }

  function inserir_documento() {
    $('#form-documento-edital').submit();
  }

  function impugnar() {
    if ($('#fileinput').val() == "") {
      alert("Insira o documento de pedido");
      console.log($('#fileinput').val());
    } else {
      console.log($('#fileinput').val());
      $('#form-impugnacao').submit();
    }
  }

  function esclarecer() {
    if ($('#fileinput2').val() == "") {
      alert("Insira o documento de pedido");
    } else {
      $('#form-esclarecimento').submit();
    }
  }

  function setaMotivoDeclinio(obj) {
    $.get('php/get_motivo_por_categoria.php?id_cat=' + obj.value, function(data) {
      $('#selectMotivo').html(data);
      $('#selectMotivo').removeAttr("readonly");
    });
  }
</script>

</html>