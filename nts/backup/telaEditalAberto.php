<?php
    require_once('conn/conexao.php');
    $id = $_GET['id'];
    $sql = "SELECT * from edital where id = $id";
    $res = mysqli_query($conn, $sql);
    while($row = mysqli_fetch_array($res)){
        $id_id = $row['id'];
        $edital_id = $row['edital_id'];
        $num_processo = $row['num_processo'];
        $entrega= $row['entrega'];
        $orgao= $row['orgao'];
        $local_obra= $row['local_obra'];
        $objeto= $row['objeto'];
        $valor= $row['valor'];
        $servico= $row['servico'];
        $especialidade= $row['especialidade'];
        $setor= $row['setor'];
        $registro= $row['registro'];
    }

    $sqlDoc = "select file from orgao_documento where fk_orgao = $id";
    $resDoc = mysqli_query($conn,$sqlDoc);

    while($row = mysqli_fetch_array($resDoc)){
        $ficheiro = $row[0];
    }

    $sqlCat = "select id as id_cat,nome as nome_cat from documento_categoria";
    $resCat = mysqli_query($conn,$sqlCat);

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=11">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="EvolutionSoft Tecnologias LTDA">
  
  <link href="image/nts1.png" rel="icon">
  <link href="image/nts1.png" rel="apple-touch-icon">

  <title>Tela Edital</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">

  <style>
    @font-face {
        font-family: gotham;
        src: url(font/GothamMedium.ttf);
    }
    @font-face {
        font-family: gotham-bold;
        src: url(font/Gotham-Bold.otf);
    }
    span{
        font-family: gotham-bold;
        color: #000;
    }
    * {
        font-family: gotham;
    }
    body {
      background: #E5E5E5;
    }
    .box-content{
        margin: 30px; 
        height: 225px;
        display: flex;
        font-size: 13px;
        color: #000;
        border: 1px solid #000;
        box-shadow: 2px rgba(0,0, 0, 0.9) ;
    }
    .close {
        cursor: pointer;
        position: absolute;
        right: 0%;
        height: 25px;
        padding: 0px 5px;
        margin: 5px 35px 50px 0px;
    }

    .close:hover {
        background: #bbb;
        border-radius: 50px;
    }
    li{
        margin-bottom: 5px;
    }

  </style>
</head>
<!--<div id="preloader">
</div>-->
<body id="page-top">
    <div id="wrapper" style="display: block;">
        <!--<div id="seleciona-edital"></div>-->
        <a>
            <div class="box-content shadow">
               <img src="image/edital_aberto.png" alt="" style="height: 100px;width: 130px;margin: 35px 0px 25px 20px;"> 
               <div style="height: 177px; padding:30px; width: 60%;"> <label>Nº Edital: <span><?php echo $edital_id; ?></span></label><br> <label>Orgão: <span><?php echo $orgao; ?></span> </label><br> <label>Local: <span><?php echo $local_obra; ?></span></label><br> <label>Cad. Orgão: <span>NÃO</span></label> </div>
               <div style="height: 100px; padding: 30px;"> <label>Valor: <span><?php echo $valor; ?></span></label><br> <label>Data Inicio: <span><?php echo $registro; ?></span></label><br> <label>Data Fim: <span><?php echo $entrega; ?></span></label><br> <label>Objeto: <span><?php echo $objeto; ?></span></label> </div>
            </div>
         </a>
    </div>

    <div class="container-fluid">
        <div class="row" style="height: 50%;">
            <div class="col-xl-8 col-lg-7">
                <div class="box-content-votacao" style="margin-right: 10px;    background: #e5e5e5;box-shadow: none;">
                    <div class="title-manage-vote">
                    </div>
                    
                    <!-- C:\Users\otavio\Desktop\CREA SP - Controle de Acesso (Novo)-Fase 1.pdf -->
                    <!-- echo ficheiro no src -->
                    <!-- COLLAPSED-->
                    <div id="accordion-votacao" class="acc-widthh" style="height: 36vh;width: 96%;padding-right: 15px;">
                      <div id="conteudo-pautas">
                      <embed src="pdfteste.pdf" type="application/pdf"
                               frameborder="0" width="100%" height="400px" style="margin: 0;padding: 0">
                      </embed>
                      </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="col-xl-4 col-lg-5">
                <div class="box-content-votacao">
                   <div style="padding: 15px 0px 0px 17px; color: #000000;">Selecione os documentos necessários:</div>
                   <div id="accordion" style="height: 30vh;">
                   <?php
                    while($row = mysqli_fetch_array($resCat)){
                        $id = $row['id_cat'];
                   ?>
                    <div class="card">
                      <div class="card-header" id="heading_<?= $id ?>">
                        <h5 class="mb-0">
                          <button style="margin-bottom: 5px;background: #e6e6e6;text-transform: none;padding-left: 15px;width: 90%;margin-left: 15px;color: #000;font-size: 17px;" class="btn btn-link" 
                          data-toggle="collapse" data-target="#collapse_<?= $id ?>" aria-expanded="true" aria-controls="collapse_<?= $id ?>">
                            <?= $row['nome_cat']; ?>
                          </button>
                        </h5>
                      </div>
                  
                      <div id="collapse_<?= $id ?>" class="collapse hide" aria-labelledby="heading_<?= $id ?>" data-parent="#accordion">
                        <div class="card-body" style="background: #e6e6e6;width: 375px;border-radius: 10px;margin-left: 15px;margin-bottom: 5px;color: #000;">
                        <?php 
                            $sqlCatDoc = "select id as id_doc,nome as nome_doc from documento where id_categoria_doc = $id";
                            $resCatDoc = mysqli_query($conn,$sqlCatDoc);
                            while($row1 = mysqli_fetch_array($resCatDoc)){ 
                                $id_doc = $row1['id_doc'];
                        ?>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck_<?= $id_doc ?>">
                                <label style="    padding-left: 20px;padding-top: 4px;" class="form-check-label" for="exampleCheck_<?= $id_doc ?>"><?= $row1['nome_doc'] ?></label>
                            </div>
                        <?php } ?>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                    </div>
                  </div>
                            <!--<div class="button-group">
                                <button type="button" style="text-transform: none;font-size: 15px;   background: #e6e6e6;margin-left: 9px;border-radius: 7px;" class="btn btn-default btn-sm " data-toggle="dropdown"><span class="glyphicon glyphicon-cog">Documentos 1</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu" style="position: relative !important;transform: translate3d(32px, 0px, 0px) !important;width: 86%;background: #e6e6e6;padding: 15px;color: #000;">
                                <li><a  class="small" data-value="option1" tabIndex="-1"><input type="checkbox"/>&nbsp;Documento 1</a></li>
                                <li><a  class="small" data-value="option2" tabIndex="-1"><input type="checkbox"/>&nbsp;Documento 2</a></li>
                                <li><a  class="small" data-value="option3" tabIndex="-1"><input type="checkbox"/>&nbsp;Documento 3</a></li>
                                <li><a  class="small" data-value="option6" tabIndex="-1"><input type="checkbox"/>&nbsp;Documento 6</a></li>
                                </ul>
                                <button type="button" style="text-transform: none;font-size: 15px;   background: #e6e6e6;margin-left: 9px;border-radius: 7px;" class="btn btn-default btn-sm " data-toggle="dropdown"><span class="glyphicon glyphicon-cog">Documentos 1</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu" style="position: relative !important;transform: translate3d(32px, 0px, 0px) !important;width: 86%;background: #e6e6e6;padding: 15px;color: #000;">
                                <li><a  class="small" data-value="option1" tabIndex="-1"><input type="checkbox"/>&nbsp;Documento 1</a></li>
                                <li><a  class="small" data-value="option2" tabIndex="-1"><input type="checkbox"/>&nbsp;Documento 2</a></li>
                                <li><a  class="small" data-value="option3" tabIndex="-1"><input type="checkbox"/>&nbsp;Documento 3</a></li>
                                <li><a  class="small" data-value="option6" tabIndex="-1"><input type="checkbox"/>&nbsp;Documento 6</a></li>
                                </ul>
                          </div>-->
                       <!-- </div>
                      </div>-->
                </div>
        </div>
        

        <div class="row" style=" height: 38%;">
            <div class="col-xl-12 col-lg-12">
                <div class="box-content-votacao" style="box-shadow: none; ">
                    <div style="display: flex;background:#e5e5e5;">
                        <button id="button-form" style="margin-right:10px; background:#C4C4C4;border: none; color: red; text-align: center;" type="button" class="btn btn-secondary">Declinar</button>
                        <button href="https://google.com" id="button-form" style="margin-right:10px;background:#C4C4C4;border: none; color: #000; text-align: center;"type="button" class="btn btn-secondary"><a href="solicitarParecer.php?id=<?php echo $id_id ?>" style="    text-decoration: none;" >Solicitar Parecer</a></button>
                        <button id="button-form" style="background:#C4C4C4;border: none; color: #07B204; text-align: center;" type="button" class="btn btn-secondary">Aprovar</button>    
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/fuctions.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        var closebtns = document.getElementsByClassName("close");
        var i;

        for (i = 0; i < closebtns.length; i++) {
        closebtns[i].addEventListener("click", function() {
            this.parentElement.style.display = 'none';
        });
        }
    </script>
    
</body>
<script>
 getEdital();
</script>
</html>