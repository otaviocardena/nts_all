<?php
include_once('../conn/conexao.php');

$tela = $_POST['tela_ata'];

$id_edital = $_POST['id_edital_ata'];
$data = $_POST['data_ata'];
$hora_ini = $_POST['inicio_ata'];
$hora_fim = $_POST['fim_ata'];
$modalidade = $_POST['modalidade_ata'];
$obs = $_POST['obs_ata'];

$concorrentes = array();
$valores = array();
$posicoes = array();
$reps = array();
$habs = array();
$alegs = array();

for ($i = 1; $i <= 4; $i++) {
        if ($i > 1) {
                $cnpj = $_POST['concorrente_' . $i];
                $sql = "SELECT id FROM concorrentes WHERE cnpj_concorrente = '$cnpj'";
                $res = mysqli_query($conn, $sql);
                while ($row = mysqli_fetch_array($res)) {
                        $id_concorrente = $row[0];
                }
                array_push($concorrentes, $id_concorrente);
        } else {
                array_push($concorrentes, $_POST['concorrente_' . $i]);
        }

        $valor_input = isset($_POST['valor_concorrente_' . $i]) ? $_POST['valor_concorrente_' . $i] : "NULL";
        $posicao_input = isset($_POST['posicao_concorrente_' . $i]) ? $_POST['posicao_concorrente_' . $i] : "NULL";
        $hab_input = isset($_POST['hab_concorrente_' . $i]) ? $_POST['hab_concorrente_' . $i] : "NULL";

        array_push($valores, $valor_input);
        array_push($posicoes, $posicao_input);
        array_push($reps, $_POST['rep_concorrente_' . $i]);
        array_push($alegs, $_POST['aleg_concorrente_' . $i]);
        array_push($habs, $hab_input);
}

$sql = "INSERT INTO edital_ata(edital_id,data,hora_ini,hora_fim,obs,modalidade)
        VALUES ($id_edital,'$data','$hora_ini','$hora_fim','$obs',$modalidade)";
$res = mysqli_query($conn, $sql);

$sql = "SELECT id FROM edital_ata ORDER BY id DESC LIMIT 1";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
        $id_ata = $row[0];
}

for ($i = 0; $i < 4; $i++) {
        if ($modalidade == 0) {
                echo $sql = "INSERT INTO edital_ata_concorrentes(id_edital_ata,representante,motivo,concorrente,valor,posicao,hab)
                VALUES($id_ata,'$reps[$i]','$alegs[$i]',$concorrentes[$i],NULL,NULL,$habs[$i])";
        }else if($modalidade == 1){
                $sql = "INSERT INTO edital_ata_concorrentes(id_edital_ata,representante,motivo,concorrente,valor,posicao,hab)
                VALUES($id_ata,'$reps[$i]','$alegs[$i]',$concorrentes[$i],'$valores[$i]',$posicoes[$i],NULL)";
        }else if ($modalidade == 2){
                $sql = "INSERT INTO edital_ata_concorrentes(id_edital_ata,representante,motivo,concorrente,valor,posicao,hab)
                VALUES($id_ata,'$reps[$i]','$alegs[$i]',$concorrentes[$i],'$valores[$i]',$posicoes[$i],$habs[$i])";
        }

        $res = mysqli_query($conn, $sql);
}

header("Location: ../telaEditalAprovado".$tela.".php?id=$id_edital");
