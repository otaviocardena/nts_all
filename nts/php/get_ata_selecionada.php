<?php
include_once('../conn/conexao.php');

function getNomeConcorrente($id)
{
    if ($id == 0) {
        return "NTS ENGENHARIA";
    } else {
        global $conn;
        $sql = "SELECT razao_social_concorrente FROM concorrentes WHERE id=$id";
        $res = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_array($res)) {
            $nome = $row[0];
        }
        return $nome;
    }
}

$id_ata = $_GET['id'];

$sql = "SELECT * FROM edital_ata WHERE id = $id_ata";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
    $data = date('d/m/Y', strtotime($row['data']));
    $hora_ini = date('H:i', strtotime($row['hora_ini']));
    $hora_fim = date('H:i', strtotime($row['hora_fim']));
    $modalidade = $row['modalidade'];
}

if ($modalidade == 0) {
    $modalidade = "Habilitação";
} else if ($modalidade == 1) {
    $modalidade = "Proposta";
} else if ($modalidade == 2) {
    $modalidade = "Proposta Técnica";
}

$sql = "SELECT * FROM edital_ata_concorrentes WHERE id_edital_ata = $id_ata";
$res_consulta_ata_concorrentes = mysqli_query($conn, $sql);
?>
<div class="form-row">
    <div class="col"></div>
    <div class="col-5">
        <label for="modalidade_ata">Selecione a modalidade da ATA:</label>
    </div>
    <div class="col-5">
        <div class="form-control">
            <?= $modalidade ?>
        </div>
    </div>
    <div class="col"></div>
</div><br>
<div class="form-row" style="margin-bottom: 1rem">
    <div class="col-5" style="align-self: center;">
        <div class="form-row">
            <div class="col-4" style="align-self: center;">
                <label for="data_ata" style="font-size:14px;"><b>Data Sessão: </b></label>
            </div>
            <div class="col">
                <div style="background-color:white;" class="fields-ata" style="font-size: 12px;"><?= $data ?></div>
            </div>
        </div>
    </div>
    <div class="col" style="align-self: center;">
        <div class="form-row">
            <div class="col-6" style="align-self: center;">
                <label for="inicio_ata" style="font-size:14px;"><b>Horário Início: </b></label>
            </div>
            <div class="col">
                <div style="background-color:white;" class="fields-ata" style="font-size: 12px;"><?= $hora_ini ?></div>
            </div>
        </div>
    </div>
    <div class="col" style="align-self: center;">
        <div class="form-row">
            <div class="col-6" style="align-self: center;">
                <label for="fim_ata" style="font-size:14px;"><b>Horário Fim: </b></label>
            </div>
            <div class="col">
                <div style="background-color:white;" class="fields-ata" style="font-size: 12px;"><?= $hora_fim ?></div>
            </div>
        </div>
    </div>
</div>

<?php
while ($row = mysqli_fetch_array($res_consulta_ata_concorrentes)) {
    if (isset($row['hab'])) {
        if ($row['hab'] == 0) {
            $hab = "N";
        } else {
            $hab = "S";
        }
    }else{
        $hab = "N/A";
    }

    if (isset($row['valor'])){
        $valor = $row['valor'];
    }else{
        $valor = 0;
    }

    if(isset($row['posicao'])){
        $posicao = $row['posicao'];
    }else{
        $posicao = "N/A";
    }
?>
    <div class="form-group">
        <div class="form-row">
            <div class="col">
                <div class="fields-ata" style="background-color:white;color:#2137FF;vertical-align: top;width: 100%;margin-bottom: 5px;">
                    <?= getNomeConcorrente($row['concorrente']) ?>
                </div>
            </div>
            <div class="col">
                <div class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;background-color:white;">
                    <?= $row['representante'] ?>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="form-row">
                    <div class="col-4">
                        <div type="number" step="0.01" class="fields-ata" placeholder="R$" name="valor_concorrente_1" style="width: 100%;background-color:white;">
                            R$ <?= number_format($valor, '2', ',', '.') ?>
                        </div>
                    </div>
                    <div class="col-2" style="align-self: center;">
                        <p style="margin: 0;font-size: 20px;align-self:center;">Pos:</p>
                    </div>
                    <div class="col-2" style="align-self: center;">
                        <div class="fields-ata" style="vertical-align: top;float: right;background-color:white;width:100%;">
                            <?= $posicao ?>
                        </div>
                    </div>
                    <div class="col-2" style="align-self: center;">
                        <p style="margin: 0;font-size: 20px;align-self:center;">Hab:</p>
                    </div>
                    <div class="col-2" style="align-self: center;">
                        <div class="fields-ata" style="vertical-align: top;float: right;background-color:white;width:100%;">
                            <?= $hab ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col">
                <div class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;background-color:white;">
                    <?= $row['motivo'] ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>