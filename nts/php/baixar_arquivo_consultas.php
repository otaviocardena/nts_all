<?php
include_once('../conn/conexao.php');
$id = $_GET['id'];
$campo = $_GET['campo'];
$tabela = $_GET['tabela'];
// $id = 2;
// $campo = 'pedido_pdf';
// $tabela = 'juridico';

$sql = "SELECT $campo,".$campo."_filename FROM edital_$tabela WHERE id = $id";
$res = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($res)) {
    $arquivo = $row[0];
    $filename = $row[1];
}

$decoded = base64_decode($arquivo);
$file = $filename;
file_put_contents($file, $decoded);

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    unlink($file);
    exit;
}