<?php
session_start();

require_once('../conn/conexao.php');
$id_edital = $_GET['id'];

/**
 * Simply import all pages and different bounding boxes from different PDF documents.
 */

use setasign\Fpdi;
use setasign\fpdf;

require_once 'vendor/autoload.php';
require_once 'vendor/setasign/fpdf/fpdf.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);
set_time_limit(2);
date_default_timezone_set('UTC');
$start = microtime(true);

$pdf = new Fpdi\Fpdi();
//$pdf = new Fpdi\TcpdfFpdi('L', 'mm', 'A3');

if ($pdf instanceof \TCPDF) {
    $pdf->SetProtection(['print'], '', 'owner');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
}

$sql = "select 
            dt.file,
            dt.id_categoria_doc 
        from 
            edital_aprovado as ea
            inner join documento_tipo as dt on
            ea.pdf_id = dt.id 
        where 
            ea.edital_id = $id_edital
        ORDER BY ea.pdf_id";
$res = mysqli_query($conn, $sql);

$files = array();
$cat = array();

while ($row = mysqli_fetch_array($res)) {
    $pdf_contents = base64_decode($row['file']);
    $id = uniqid();
    file_put_contents($id . '.pdf', $pdf_contents);
    array_push($files, $id . '.pdf');
    array_push($cat, $row['id_categoria_doc']);
}


$cont_cat = 0;
$cont = 0;

foreach ($files as $key => $file) {
    $pageCount = $pdf->setSourceFile($file);
    // $pdf->AliasNbPages(1);
    
    for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
        $pdf->AddPage();
        $pageId = $pdf->importPage($pageNo, '/MediaBox');
        //$pageId = $pdf->importPage($pageNo, Fpdi\PdfReader\PageBoundaries::ART_BOX);
        $s = $pdf->useTemplate($pageId, 10, 10, 200);

        if ($cont_cat == 0) {
            // $cont = 1;
            $cont++;
        } else if ($cat[$cont_cat] == $cat[$cont_cat - 1]) {
            $cont++;
        } else if ($cat[$cont_cat] != $cat[$cont_cat - 1]) {
            $nomeDoc = explode("\\fpdi_working\\", $pageId);
            $nomeDoc = explode(".pdf", $nomeDoc[1]);
            if ($pageNo > 1) {
                $pageId2 = $pdf->importPage($pageNo - 1, '/MediaBox');
                $nomeDoc2 = explode("\\fpdi_working\\", $pageId2);
                $nomeDoc2 = explode(".pdf", $nomeDoc2[1]);
                if ($nomeDoc[0] == $nomeDoc2[0]) {
                    $cont++;
                }else{
                    $cont=1;
                }
            }else{
                $cont=1;
            }
        }
        $pdf->SetFont('Helvetica');
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(190, 10);
        $pdf->Write(2, $cont);
    }
    $cont_cat++;
}

foreach ($files as $filename) {
    unlink($filename);
}

$file = uniqid() . '.pdf';
$pdf->Output('I', 'simple.pdf');
$pdf->Output('output/'.$file, 'I');


