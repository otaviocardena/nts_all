async function getEdital() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/edital',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalAberto.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'><button style='    position: relative;float: none;  margin: 10px 10px;' class='close' onclick='getIdDeclinado(" + name.id + ")'>Declinar</button><button style='    position: relative;float: none;  margin: 10px 10px;' class='close' onclick='cadAgendado(" + name.id + ")'>Agendar</button></div>    </div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getAgendado() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/editalAgendado',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalAberto.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'><button style='    position: relative;float: none;  margin: 10px 10px;' class='close' onclick='getIdDeclinado(" + name.id + ")'>Declinar</button></div></div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getPendente() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/pendente',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display: block;'> <div style='display:flex'> <img src='image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style=' padding:30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a href='telaEditalAberto.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br><label>Objeto: <span class='text-span'>" + name.objeto + "</span></label> </div> <div style=' padding: 30px;'> <label>Valor: <span>" + valor + "</span></label><br> <label>Data Inicio: <span>" + name.registro + "</span></label><br> <label>Data Fim: <span>" + name.entrega + "</span></label> </div> </div> <div style=' text-align: -webkit-center;'> <button style=' position: relative;float: none; margin: 10px 10px;' class='close' onclick='getIdDeclinado()'> tetsten </button> <button style=' position: relative;float: none; margin: 10px 10px;' class='close' onclick='getIdDeclinado()'> tetsten </button> </div> </div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getDeclinado() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/declinados',
    // url: 'http://localhost:3000/declinados',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style=' text-align: -webkit-center;'><label>Motivo de declínio: <b><span>"+ name.motivo_declinado +"</span></b></label></div> <label style='padding: 5px 30px;'>Observação de declínio: <span>" + name.observacao + "</span></label></div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);
    });
}

async function getImpugnacao() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/impugnacao',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalImpugnacao.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'><button style='    position: relative;float: none;  margin: 10px 10px;' class='close' onclick='getIdDeclinado(" + name.id + ")'>Declinar</button></div></div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}
async function getImpugnacaoAp() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/impugnacaoap',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow'> <img src='image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='height: 177px; padding:30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a href='telaEditalImpugnacao.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label> </div> <div style='height: 100px; padding: 30px;'> <label>Valor: <span>" + valor + "</span></label><br> <label>Data Inicio: <span>" + name.registro + "</span></label><br> <label>Data Fim: <span>" + name.entrega + "</span></label><br> <label>Objeto: <span class='text-span'>" + name.objeto + "...</span></label> </div> <span class='close' onclick='getIdDeclinado(" + name.id + ")'>Declinar</span> </div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getAprovado() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/aprovado',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalAprovado.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'>";


        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getAprovado2() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/aprovado2',
    // url: 'http://localhost:3000/aprovado2',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalAprovadoFase2.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'>";


        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getAprovado3() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/aprovado3',
    // url: 'http://localhost:3000/aprovado3',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalAprovadoFase3.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'>";


        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getAguardandoHomologacao() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/aguardandoHomologacao',
    // url: 'http://localhost:3000/aguardandoHomologacao',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalHomologacao.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'>";


        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getHomologados() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/homologados',
    // url: 'http://localhost:3000/homologados',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalHomologacao.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'>";


        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function getEsclarecimento() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/esclarecimento',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalEsclarecimento.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='padding: 30px 20px 0px 30px; width: 40%'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div><label style='padding: 5px 30px;'>Objeto: <span>" + name.objeto + "</span></label> <div style='text-align:-webkit-center;'><button style='    position: relative;float: none;  margin: 10px 10px;' class='close' onclick='getIdDeclinado(" + name.id + ")'>Declinar</button></div></div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}
async function getEsclarecimentoAp() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/esclarecimentoap',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var valor = name.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display:block;height:auto; border-radius: 10px;'> <div style='display:flex'><img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style=' padding:30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalEsclarecimento.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> <label>Objeto: <span>" + name.objeto + "</span></label> </div> <div style=' padding: 30px;'> <label>Valor: <span>" + valor + "</span></label><br> <label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label></div></div> <div style='text-align:-webkit-center;'><button style='    position: relative;float: none;  margin: 10px 10px;' class='close' onclick='getIdDeclinado(" + name.id + ")'>Declinar</button></div></div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}



async function getPendenteadm() {

  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/pendenteadm',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        const div = document.createElement("div");
        div.innerHTML = "<div class='box-content shadow' style='display: block;border-radius: 10px;'> <div style='display:flex'> <img src='../nts/image/edital_aberto.png' alt='' style='height: 100px;width: 130px;margin: 35px 0px 25px 20px;'> <div style='padding: 30px 20px 0px 30px; width: 60%;'> <label id='id_editalaberto'>Nº Edital:<a target='_BLANK' href='../nts/telaEditalRespondido.php?id=" + name.id + "'> <span>" + name.edital_id + "</span> </a></label><br> <label>Orgão: <span>" + name.orgao + "</span> </label><br> <label>Local: <span>" + name.local_obra + "</span></label><br> <label>Cad. Orgão: <span>NÃO</span></label><br> </div> <div style='width: 40%; padding: 30px 30px 0px 30px;'> <label>Nota: <span>" + name.valor + "</span></label><label style='margin-left:40px'>Status: <span style='color: " + name.color + "'>" + name.status + "</span></label><br><label>Prazo de execução: <span>" + name.prazo + "</span></label><br> <label>Data Entrega: <span>" + name.entrega + "</span></label><br><label>Processo: <span>" + name.num_processo + "</span></label> </div> </div>  <label style='padding: 0px 30px;'>Objeto: <span>" + name.objeto + "</span></label><div style='text-align: -webkit-center;'> <button style=' position: relative;float: none; margin: 10px 10px;' class='close' onclick='getIdDeclinado(" + name.id + ")'> Declinar </button></div> </div>";

        document.getElementById("seleciona-edital").appendChild(div);
      });
    })
    .catch(function (response) {
      console.log(response);

    });
}
//

async function getEtapa() {
  await axios({
    method: 'GET',
    url: 'https://nts-backend.herokuapp.com/etapa',
    data: {},
    headers: {}
  })
    .then(function (response) {
      response.data.forEach(function (name) {
        var btn = document.createElement("option");
        btn.innerHTML = name.motivo;
        btn.value = name.id;
        document.getElementById("selectMotivo").appendChild(btn);

      });

    })
    .catch(function (response) {
      //handle error

    });
}


function getIdDeclinado(edital_id) {
  $('#declinarId').html(edital_id);
  $('#declinio').modal('show');
}

// function getIdAgendar(edital_id){
//   $('#AgendarId').html(edital_id);
//   $('#agendamento').modal('show');  
// }

async function cadDeclinado() {
  const edital_id = $('#declinarId').html();
  const motivo = $('#selectMotivo').val();
  const observacao = $('#observacaoDeclinio').val();
  await axios({
    method: 'POST',
    url: 'https://nts-backend.herokuapp.com/declinado',
    data: {
      fk_id_edital: edital_id,
      etapa_declinada: 'NULL',
      motivo_id: motivo,
      observacao: observacao
    },
    headers: {}
  })
    .then(function (response) {
      $('#responseCad').html(response.data);
      $('#responseModal').modal('show');
      window.location.href = "index.php#../nts/telaedital.php";
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function cadAgendado(edital_id) {
  await axios({
    method: 'POST',
    url: 'https://nts-backend.herokuapp.com/agendado',
    data: {
      fk_id_edital: edital_id,
    },
    headers: {}
  })
    .then(function (response) {
      $('#responseCad').html(response.data);
      $('#responseModalAgendado').modal('show');
      window.location.href = "index.php#../nts/telaedital.php";
    })
    .catch(function (response) {
      console.log(response);

    });
}

async function cadAprovadoObs() {
  const edital_id = $('#declinarId').html();
  const observacao = $('#observacaoDeclinio').val();
  await axios({
    method: 'POST',
    url: 'https://nts-backend.herokuapp.com/aprovadoobs',
    data: {
      edital_id: edital_id,
      observacao: observacao



    },
    headers: {}
  })
    .then(function (response) {
      $('#responseCad').html(response.data);
      $('#responseModal').modal('show');

    })
    .catch(function (response) {
      console.log(response);

    });
}
