<?php
session_start();
if (!isset($_SESSION['ZWxldHJpY2Ft_adm'])) {
    header("Location: ../nts_admin/login.php");
}

require_once('conn/conexao.php');
$id = $_GET['id'];

//SELECT DE INFORMAÇÕES DO EDITAL
$sql = "SELECT 
                id,
                edital_id,
                num_processo,
                entrega,
                orgao,
                local_obra,
                objeto,
                valor,
                prazo,
                servico,
                especialidade,
                setor,
                registro 
            FROM edital WHERE id = $id";
$res = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res)) {
    $id_id = $row['id'];
    $edital_id = $row['edital_id'];
    $num_processo = $row['num_processo'];
    $prazo = $row['prazo'];
    if ($prazo == 0) {
        $prazo = "Sem informação";
    } else {
        $prazo .= " Meses";
    }
    $entrega = $row['entrega'];
    $orgao = $row['orgao'];
    $local_obra = $row['local_obra'];
    $objeto = $row['objeto'];
    $valor = $row['valor'];
    $servico = $row['servico'];
    $especialidade = $row['especialidade'];
    $setor = $row['setor'];
    $registro = $row['registro'];
}

$sqlDoc = "select * from edital_documento where edital_id = $id";
$resDoc = mysqli_query($conn, $sqlDoc);

while ($row = mysqli_fetch_array($resDoc)) {
    $ficheiro = $row['file'];
}

$sqlJuridico = "SELECT * FROM edital_juridico WHERE edital_id = $id_id AND status = 0";
$resJuridico = mysqli_query($conn, $sqlJuridico);
$countJuridico = mysqli_num_rows($resJuridico);

$sqlRecurso = "SELECT * FROM edital_recurso WHERE edital_id = $id_id AND status = 0";
$resRecurso = mysqli_query($conn, $sqlRecurso);
$countRecurso = mysqli_num_rows($resRecurso);

if ($countJuridico > 0) {
    $status_edital = "Aguardando Jurídico";
    $color_status = "#2775EA";
} else if ($countRecurso > 0) {
    $status_edital = "Aguardando Recurso";
    $color_status = "#FFA800";
} else {
    $status_edital = "Aguardando";
    $color_status = "#138700";
}

//FUNÇÕES
function getNota($setor, $id)
{
    global $conn;
    $sql = "SELECT 
            sum(er.valor) 
          FROM edital_respondido as er
          INNER JOIN usuario as u on
          er.user_id = u.id
          INNER JOIN setor as s on
          u.fk_setor = s.id
          WHERE 
            s.setor = '$setor' AND
            er.edital_id = $id";
    $res = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_array($res)) {
        $nota = $row[0];
    }
    if (!isset($nota)) {
        $nota = 20;
    }
    return $nota;
}

function getObs($setor, $id)
{
    global $conn;
    $sql = "SELECT 
            eo.observacao 
          FROM edital_respondido as er
          INNER JOIN usuario as u on
          er.user_id = u.id
          INNER JOIN setor as s on
          u.fk_setor = s.id
          INNER JOIN edital_observacao as eo on
          eo.user_id = u.id
          WHERE 
            s.setor = '$setor' AND
            er.edital_id = $id AND
            eo.edital_id = $id";
    $res = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_array($res)) {
        $obs = $row[0];
    }
    if (!isset($obs)) {
        $obs = "Sem observação de setor.";
    }
    return $obs;
}

//SQLS PARA MODAL DE NOTAS DE ACORDO COM SETOR
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Financeiro' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorFinanceiro = mysqli_query($conn, $sql);
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Estratégico' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorEstrategico = mysqli_query($conn, $sql);
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Técnico' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorTecnico = mysqli_query($conn, $sql);
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Jurídico' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorJuridico = mysqli_query($conn, $sql);
$sql = "SELECT
          eo.observacao,
          u.id as user,
          'Administrativo' as setor
        FROM edital_respondido as er
        INNER JOIN usuario as u on
        er.user_id = u.id
        INNER JOIN edital_observacao as eo on
        eo.user_id = u.id
        WHERE 
          er.edital_id = $id AND
          eo.edital_id = $id
        GROUP BY setor
        ";
$resSetorAdministrativo = mysqli_query($conn, $sql);

$sql = "SELECT * FROM categoria_motivo_declinado";
$res_categoria_motivo_declinio = mysqli_query($conn, $sql);

$sql = "SELECT * FROM edital_homologado WHERE id_edital = $id_id";
$res_homologado = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res_homologado)) {
    $status_homologado = $row['status'] ? $row['status'] : "X";
}
$count_homologado = mysqli_num_rows($res_homologado);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=11">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="EvolutionSoft Tecnologias LTDA">

    <link href="image/nts1.png" rel="icon">
    <link href="image/nts1.png" rel="apple-touch-icon">

    <title>Tela Edital Aprovados - Fase 3</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

    <style>
        @font-face {
            font-family: gotham;
            src: url(font/GothamMedium.ttf);
        }

        @font-face {
            font-family: gotham-bold;
            src: url(font/Gotham-Bold.otf);
        }

        span {
            font-family: gotham-bold;
            color: #000;
        }

        * {
            font-family: gotham;
        }

        body {
            background: #E5E5E5;
        }

        .box-content {
            margin: 30px;
            height: auto;
            padding-bottom: 20px;
            display: flex;
            font-size: 13px;
            color: #000;
            border: 1px solid #000;
            box-shadow: 2px rgba(0, 0, 0, 0.9);
        }

        .close {
            cursor: pointer;
            position: absolute;
            right: 0%;
            height: 25px;
            padding: 0px 5px;
            margin: 5px 35px 50px 0px;
        }

        .close:hover {
            background: #bbb;
            border-radius: 50px;
        }

        li {
            margin-bottom: 5px;
        }

        .file_customizada::-webkit-file-upload-button {
            visibility: hidden;
        }

        .custom-file-input::before {
            content: 'Select some files';
            display: inline-block;
            background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
            border: 1px solid #999;
            border-radius: 3px;
            padding: 5px 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
            font-weight: 700;
            font-size: 10pt;
        }

        .file_customizada:hover::before {
            border-color: black;
        }

        .file_customizada:active::before {
            background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
        }

        .div-valor-status {
            position: relative;
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .hide {
            display: none;
        }

        .show {
            display: block;
        }
    </style>
</head>
<!--<div id="preloader">
</div>-->

<body id="page-top">
    <div id="wrapper" style="display: block;">
        <!--<div id="seleciona-edital"></div>-->
        <a>
            <div class="box-content shadow" style="padding-bottom:0px;padding: 20px;">
                <div class="form-row">
                    <div class="col">
                        <div class="form-row">
                            <div class="col-2" style="display: flex;align-items: center;justify-content: center;">
                                <img src="image/edital_aberto.png" alt="" style="height: 100px;width: 200px;    align-self: center;">
                            </div>
                            <div class="col">
                                <div style="margin-right: 20px;margin-left: 20px;">
                                    <label>Nº Edital: <span><?php echo $edital_id; ?></span></label><br>
                                    <label>Orgão: <span><?php echo $orgao; ?></span> </label><br>
                                    <label>Local: <span><?php echo $local_obra; ?></span></label><br>
                                    <label>Cad. Orgão: <span>NÃO</span></label><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <label>Valor: <span><?php echo "R$ " . number_format($valor, 2, ',', '.'); ?></span></label><br>
                        <label>Prazo de execução: <span><?= $prazo ?></span></label><br>
                        <label>Data Entrega: <span><?php echo date('d/m/Y', strtotime($entrega)); ?></span></label><br>
                        <label>Processo: <span><?= $num_processo ?></span></label><br>
                    </div>

                    <label style="padding: 0px 15px;">Objeto: <span><?php echo $objeto; ?></span></label>
                </div>
            </div>
        </a>
    </div>

    <div class="container-fluid">
        <div class="row" style="height: 50%;">
            <div class="col-xl-8 col-lg-8">
                <div class="box-content-votacao" style="margin-right: 10px;background: #e5e5e5;box-shadow: none;">
                    <!-- C:\Users\otavio\Desktop\CREA SP - Controle de Acesso (Novo)-Fase 1.pdf -->
                    <!-- echo ficheiro no src -->
                    <!-- COLLAPSED-->
                    <div id="accordion-votacao" class="acc-widthh" style="height: 100% ;width: 96%;padding-right: 15px;">
                        <div id="conteudo-pautas" style="height:100%;">
                            <embed src="data:application/pdf;base64,<?php echo $ficheiro; ?>" type="application/pdf" frameborder="0" style="margin: 0;padding: 0;height: 100%;width: 96%;padding-right: 15px;">
                            </embed>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xl-4 col-lg-4">
                <form id="form-arquivo-edital" action="php/insere_arquivos.php" method="POST" enctype="multipart/form-data">
                    <div style=" margin-bottom:10px;">
                        <div id="buttonArquivos" class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#recurso2">
                            CONSULTAR RECURSO
                        </div>
                        <div id="buttonArquivos" class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#juridico2">
                            CONSULTAR JURIDICO
                        </div>
                        <div id="buttonArquivos" class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#baixarArq">
                            BAIXAR ARQUIVOS
                        </div>
                        <div id="buttonVerificarParecer" data-toggle="modal" data-target="#resultadoRes" class="botoes-consulta-edital-aprovado btn btn-secondary">
                            VERIFICAR PARECER
                        </div>
                        <div id="buttonEsclarecimentos" class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#esclarecimentos">
                            ESCLARECIMENTOS
                        </div>
                        <div id="buttonImpugnacao" class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#impugnacoes">
                            IMPUGNAÇÃO
                        </div>
                        <!-- <div id="buttonDocImpresso" class=".botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#imprimir"  >
                            DOCUMENTOS IMPRESSO
                        </div> -->
                        <a href="fpdi_working/index.php?id=<?= $id_id ?>" class="botoes-consulta-edital-aprovado btn btn-secondary">
                            DOCUMENTOS IMPRESSO
                        </a>
                        <div id="buttonCadastrarAta" class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#ataCad">
                            CADASTRAR ATA
                        </div>
                        <div id="buttonConsultarAta" class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#consultarAtas">
                            CONSULTAR ATA
                        </div>
                        <?php if ($count_homologado > 0) {
                            if ($status_homologado == 0) { ?>
                                <div class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#homologarCadastro">
                                    INSERIR HOMOLOGAÇÃO
                                </div>
                            <?php } else { ?>
                                <div class="botoes-consulta-edital-aprovado btn btn-secondary">
                                    EDITAL HOMOLOGADO
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="botoes-consulta-edital-aprovado btn btn-secondary" data-toggle="modal" data-target="#homologarCad">
                                HOMOLOGAR EDITAL
                            </div>
                        <?php } ?>
                    </div>
                </form>
            </div>


            <div class="row" style=" height: 38%;">
                <div class="col-xl-12 col-lg-12">
                    <div class="box-content-votacao" style="box-shadow: none;">
                        <div style="display: flex;background:#e5e5e5;">
                            <button <?php if ($countRecurso != 0) {
                                        echo "disabled";
                                    } ?> id="button-form" style="margin-right:10px; background:#C4C4C4;border: none; color: #2775EA; text-align: center;" data-toggle="modal" data-target="#juridico" type="button" class="btn btn-secondary">
                                JURÍDICO
                            </button>

                            <button <?php if ($countJuridico != 0) {
                                        echo "disabled";
                                    } ?> id="button-form" style="margin-right:10px;background:#C4C4C4;border: none; color: #FFA800; text-align: center;" data-toggle="modal" data-target="#recurso" type="button" class="btn btn-secondary">
                                APLICAR RECURSO
                            </button>
                            <button <?php if ($countJuridico != 0 || $countRecurso != 0) {
                                        echo "disabled";
                                    } ?> id="button-form" style="margin-right: 10px;background:#C4C4C4;border: none; color: #FF0000; text-align: center;" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#declinio">
                                DECLINAR
                            </button>
                            <button <?php if ($countJuridico != 0 || $countRecurso != 0) {
                                        echo "disabled";
                                    } ?> id="button-form" style="background:#C4C4C4;border: none; color: #07B204; text-align: center;" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#aprovadoObs">
                                APROVAR
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!----------- DECLINADO------------->
        <div class="modal fade" id="declinio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Deseja declinar o edital: <span id="declinarId"></span>?</h5>

                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Categoria do Motivo declinio:</label>
                            <select onchange="setaMotivoDeclinio(this)" class="form-control" id="selectCatMotivo">
                                <option value="">Selecione uma categoria</option>
                                <?php while ($row = mysqli_fetch_array($res_categoria_motivo_declinio)) { ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Motivo declinio:</label>
                            <select class="form-control" id="selectMotivo" readonly>
                            </select>
                            <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
                            <textarea class="form-control" style="cursor: auto;" id="observacaoDeclinio" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
                        <button onclick="cadDeclinado()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
                            Ok
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!----------- DECLINADO------------->
        <!-------------------------resposta------------------------------->
        <div class="modal fade" id="resultadoRes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body">
                        <?php while ($row = mysqli_fetch_array($resSetorFinanceiro)) { ?>
                            <div class="setorResultado">
                                <div class="lineSetorNota">
                                    <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                                    <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                                    <div class="obsResultado">
                                        <label style="color:#000; font-size:12px" for="">Observação:</label>
                                        <div class="textObservacaoRes" for="">
                                            <label for="">
                                                <?= getObs($row['setor'], $id_id) ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php while ($row = mysqli_fetch_array($resSetorEstrategico)) { ?>
                            <div class="setorResultado">
                                <div class="lineSetorNota">
                                    <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                                    <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                                    <div class="obsResultado">
                                        <label style="color:#000; font-size:12px" for="">Observação:</label>
                                        <div class="textObservacaoRes" for="">
                                            <label for="">
                                                <?= getObs($row['setor'], $id_id) ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php while ($row = mysqli_fetch_array($resSetorTecnico)) { ?>
                            <div class="setorResultado">
                                <div class="lineSetorNota">
                                    <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                                    <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                                    <div class="obsResultado">
                                        <label style="color:#000; font-size:12px" for="">Observação:</label>
                                        <div class="textObservacaoRes" for="">
                                            <label for="">
                                                <?= getObs($row['setor'], $id_id) ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php while ($row = mysqli_fetch_array($resSetorJuridico)) { ?>
                            <div class="setorResultado">
                                <div class="lineSetorNota">
                                    <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                                    <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                                    <div class="obsResultado">
                                        <label style="color:#000; font-size:12px" for="">Observação:</label>
                                        <div class="textObservacaoRes" for="">
                                            <label for="">
                                                <?= getObs($row['setor'], $id_id) ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php while ($row = mysqli_fetch_array($resSetorAdministrativo)) { ?>
                            <div class="setorResultado">
                                <div class="lineSetorNota">
                                    <label for=""><span>SETOR: </span><?= $row['setor'] ?></label>
                                    <label style="float: right;" for=""><span>NOTA: </span><?php echo getNota($row['setor'], $id_id) ?></label>
                                    <div class="obsResultado">
                                        <label style="color:#000; font-size:12px" for="">Observação:</label>
                                        <div class="textObservacaoRes" for="">
                                            <label for="">
                                                <?= getObs($row['setor'], $id_id) ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>

                    </div>
                </div>
            </div>
        </div>
        <!-------------------------resposta------------------------------->
        <!----------- JURIDICO------------->
        <div class="modal fade" id="juridico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Registrar recurso Juridico: <span id="declinarId"></span><?php echo $id_id ?>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <form action="php/juridico.php" method="POST" id="form-cadastra-juridico" enctype="multipart/form-data">
                            <input type="hidden" name="id_edital_juridico" value="<?= $id_id ?>">
                            <div class="form-group">
                                <?php
                                $sql = "SELECT * FROM edital_juridico WHERE edital_id = $id_id AND status = 0";
                                $res_juridico = mysqli_query($conn, $sql);
                                if (mysqli_num_rows($res_juridico) == 0) {
                                ?>
                                    <div class="custom-control custom-radio">
                                        <input onclick="abrir_concorrente(this,'juridico')" type="radio" class="custom-control-input" id="customRadio5" name="motivo_juridico" value="Próprio" checked>
                                        <label class="custom-control-label" for="customRadio5">Próprio</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input onclick="abrir_concorrente(this,'juridico')" type="radio" class="custom-control-input" id="customRadio6" name="motivo_juridico" value="Terceiros">
                                        <label class="custom-control-label" for="customRadio6">Terceiros</label>
                                    </div>
                                    <div class="hide" style="margin-top: 20px;place-content: space-evenly;" id="div_con_juridico">
                                        <input style="cursor: auto;margin-bottom:5px;" class="form-control" type="text" name="concorrente_juridico" id="concorrente_juridico" placeholder="CNPJ do concorrente solicitante">
                                        <input style="cursor: auto;" class="form-control" type="text" name="razao_social_juridico" id="razao_social_juridico" readonly>
                                    </div>
                                    <div style="margin-top: 20px;display: flex;place-content: space-evenly;">
                                        <input id="file_pedido_juridico" type="file" name="file_pedido_juridico" style="display:none;" required />
                                        <div id="falseinput_pedido_juridico" class="btn btn-primary" style="width:150px; height:30px;font-size:10px; display:flex;align-items:center;justify-content:center;">
                                            <label id="name_pedido_juridico" style="color:white;cursor:pointer;">Anexar Pedido PDF</label>
                                        </div>
                                    </div>
                                <?php } else {
                                    $sql = "SELECT id FROM edital_juridico WHERE edital_id = $id_id AND status = 0";
                                    $res_update_juridico = mysqli_query($conn, $sql);
                                    while ($row = mysqli_fetch_array($res_update_juridico)) {
                                        $id_update_juridico = $row[0];
                                    }
                                ?>
                                    <input type="hidden" name="id_update_juridico" value="<?= $id_update_juridico ?>">
                                    <div style="margin-top: 20px;display: flex;place-content: space-evenly;">
                                        <input id="file_resp_juridico" type="file" name="file_resp_juridico" style="display:none;" required />
                                        <div id="falseinput_resp_juridico" class="btn btn-primary" style="width:150px; height:30px;font-size:10px; display:flex;align-items:center;justify-content:center;">
                                            <label id="name_resp_juridico" style="color:white;cursor:pointer;">Anexar Resposta PDF</label>
                                        </div>
                                    </div>
                                <?php }
                                if (mysqli_num_rows($res_juridico) == 0) {
                                ?>
                                    <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
                                    <textarea class="form-control" style="cursor: auto;" id="observacaoAprovado" name="obs_juridico" rows="3"></textarea>
                                <?php } ?>
                            </div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Voltar</button>
                        <button onclick="cadastra_juridico()" type="button" class="btn btn-primary" style="text-align: center; text-transform: none;" onclick="">
                            Registrar
                        </button>

                    </div>
                </div>
            </div>
        </div>

        <!----------- JURIDICO------------->
        <!----------- JURIDICO  2------------->
        <div class="modal fade" id="juridico2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="margin-top: 50px; max-width: 650px !important;" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #0939c1; color: #fff;">
                        <h5 class="modal-title" id="exampleModalLabel">Recursos Juridicos Aplicados ao Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <?php
                        $sql = "SELECT * FROM edital_juridico WHERE edital_id = $id_id";
                        $res_consulta_juridico = mysqli_query($conn, $sql);

                        while ($row = mysqli_fetch_array($res_consulta_juridico)) {
                            $id_doc = $row['id'];
                            $sql = "SELECT resposta_pdf FROM edital_juridico WHERE id = $id_doc AND resposta_pdf IS NOT NULL";
                            $res_doc = mysqli_query($conn, $sql);
                            $count_res_doc = mysqli_num_rows($res_doc);
                        ?>

                            <div class="form-group" style="background: #fff;border-radius: 25px;padding: 20px;">
                                <div style="display:flex;">
                                    <div style="align-self: center;">
                                        <img style="width: 115px;" src="image/edital_aberto2.png" alt="">
                                    </div>

                                    <div style="display:block; margin-right:4%;">
                                        <p style="color:#2775EA">Jurídico <?= date('d/m/Y', strtotime($row['data_cad'])) ?> <span style="color:#5E5E5E"><?= $row['motivo'] ?></span></p>
                                        <p>Observação: <?= $row['observacao']; ?></p>
                                    </div>
                                    <div>
                                        <?php if ($row['status'] == 0) { ?>
                                            <div style="display:flex">
                                                <button onclick="aprovacao_juridico(1,<?= $row['id'] ?>,<?= $id_id ?>)" href="php/aprova_juridico.php?id=<?= $row['id'] ?>&id_edital=<?= $id_id ?>" class="btn btn-primary" style="margin-right: 5px;padding: 10px;text-align: -webkit-center;">
                                                    <i class="fas fa-check"></i>
                                                </button>
                                                <button onclick="aprovacao_juridico(2,<?= $row['id'] ?>,<?= $id_id ?>)" class="btn btn-primary" style="padding: 10px 15px;text-align: -webkit-center;">
                                                    <i class="fas fa-times"></i>
                                                </button>
                                            </div>
                                        <?php } else {
                                            if ($row['status'] == 1) {
                                                $status = "Aprovado";
                                            } else if ($row['status'] == 2) {
                                                $status = "Reprovado";
                                            }
                                        ?>
                                            <div style="display:flex">
                                                <div style="margin-right: 5px;padding: 10px;text-align: -webkit-center;">
                                                    <label style="margin:0"><?= $status ?></label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <a href="php/baixar_arquivo_consultas.php?tabela=juridico&campo=pedido_pdf&id=<?= $row['id'] ?>" class="btn btn-primary" style="padding-left: 6px;font-size: 16px;margin-top: 5px;">
                                            <i class="fas fa-cloud-download-alt"></i>
                                            Pedido
                                        </a>
                                        <?php if ($count_res_doc == 0) { ?>
                                            <a href="php/baixar_arquivo_consultas.php?tabela=juridico&campo=resposta_pdf&id=<?= $row['id'] ?>" class="btn btn-primary" style="padding-left: 6px;font-size: 16px;margin-top: 5px;">
                                                <i class="fas fa-cloud-download-alt"></i>
                                                Resposta
                                            </a>
                                        <?php } else { ?>
                                            <a class="btn btn-primary" style="padding-left: 6px;font-size: 16px;margin-top: 5px;">
                                                <i class="fas fa-cloud-download-alt"></i>
                                                Resposta
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <center><label class="text-muted">Não há mais para mostrar</label></center>
                    </div>
                </div>
            </div>
        </div>
        <!----------- JURIDICO 2------------->
        <!-------------------------recurso----------------------->
        <div class="modal fade" id="recurso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Aplicar Recurso: <span id="declinarId"></span><?php echo $id_id ?>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <form action="php/recurso.php" method="POST" id="form-cadastra-recurso" enctype="multipart/form-data">
                            <input type="hidden" name="id_edital_recurso" value="<?= $id_id ?>">
                            <div class="form-group">
                                <?php
                                $sql = "SELECT * FROM edital_recurso WHERE edital_id = $id_id AND status = 0";
                                $res_recurso = mysqli_query($conn, $sql);
                                if (mysqli_num_rows($res_recurso) == 0) {
                                ?>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" onclick="abrir_concorrente(this,'recurso')" class="custom-control-input" id="customRadio7" name="motivo_recurso" value="Próprio" checked>
                                        <label class="custom-control-label" for="customRadio7">Próprio</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" onclick="abrir_concorrente(this,'recurso')" class="custom-control-input" id="customRadio8" name="motivo_recurso" value="Terceiros">
                                        <label class="custom-control-label" for="customRadio8">Terceiros</label>
                                    </div>
                                    <div class="hide" style="margin-top: 20px;place-content: space-evenly;" id="div_con_recurso">
                                        <input style="cursor: auto;margin-bottom:5px;" class="form-control" type="text" name="concorrente_recurso" id="concorrente_recurso" placeholder="CNPJ do concorrente solicitante">
                                        <input style="cursor: auto;" class="form-control" type="text" name="razao_social_recurso" id="razao_social_recurso" readonly>
                                    </div>
                                    <div style="margin-top: 20px;display: flex;place-content: space-evenly;">
                                        <input id="file_pedido_recurso" type="file" name="file_pedido_recurso" style="display:none;" required />
                                        <div id="falseinput_pedido_recurso" class="btn btn-primary" style="width:150px; height:30px;font-size:10px; display:flex;align-items:center;justify-content:center;">
                                            <label id="name_pedido_recurso" style="color:white;cursor:pointer;">Anexar Pedido PDF</label>
                                        </div>
                                    </div>
                                <?php } else {
                                    $sql = "SELECT id FROM edital_recurso WHERE edital_id = $id_id AND status = 0";
                                    $res_update_recurso = mysqli_query($conn, $sql);
                                    while ($row = mysqli_fetch_array($res_update_recurso)) {
                                        $id_update_recurso = $row[0];
                                    }
                                ?>
                                    <input type="hidden" name="id_update_recurso" value="<?= $id_update_recurso ?>">
                                    <div style="margin-top: 20px;display: flex;place-content: space-evenly;">
                                        <input id="file_resp_recurso" type="file" name="file_resp_recurso" style="display:none;" required />
                                        <div id="falseinput_resp_recurso" class="btn btn-primary" style="width:150px; height:30px;font-size:10px; display:flex;align-items:center;justify-content:center;">
                                            <label id="name_resp_recurso" style="color:white;cursor:pointer;">Anexar Resposta PDF</label>
                                        </div>
                                    </div>
                                <?php }
                                if (mysqli_num_rows($res_recurso) == 0) {
                                ?>
                                    <label style="margin-top: 20px" for="exampleFormControlSelect1">Observação:</label>
                                    <textarea class="form-control" style="cursor: auto;" id="observacaoAprovado" name="obs_recurso" rows="3"></textarea>
                                <?php } ?>
                            </div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Voltar</button>
                        <button onclick="cadastra_recurso()" type="button" class="btn btn-primary" style="text-align: center; text-transform: none;">
                            Registrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-------------------------recurso----------------------->
        <!----------- recurso2------------->
        <div class="modal fade" id="recurso2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 730px !important;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">teste Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <?php
                        $sql = "SELECT * FROM edital_recurso WHERE edital_id = $id_id";
                        $res_consulta_recurso = mysqli_query($conn, $sql);

                        while ($row = mysqli_fetch_array($res_consulta_recurso)) {
                            $id_doc = $row['id'];
                            $sql = "SELECT resposta_pdf FROM edital_recurso WHERE id = $id_doc AND resposta_pdf IS NOT NULL";
                            $res_doc = mysqli_query($conn, $sql);
                            $count_res_doc = mysqli_num_rows($res_doc);
                        ?>
                            <div class="form-group" style="background: #fff;border-radius: 25px;padding: 20px;">
                                <div style="display:flex;">
                                    <div style="align-self: center;">
                                        <img style="width: 115px;" src="image/edital_aberto3.png" alt="">
                                    </div>

                                    <div style="display:block; margin-right:4%">
                                        <p style="color:#FFA800">Recurso <?= date('d/m/Y', strtotime($row['data_cad'])) ?> <span style="color:#5E5E5E"><?= $row['motivo'] ?></span></p>
                                        <p>Observação: <?= $row['observacao']; ?></p>
                                    </div>
                                    <div>
                                        <?php if ($row['status'] == 0) { ?>
                                            <div style="display:flex">
                                                <button onclick="aprovacao_recurso(1,<?= $row['id'] ?>,<?= $id_id ?>)" class="btn btn-primary" style="background:#FFA800;margin-right: 5px;padding: 10px;text-align: -webkit-center;color:#fff">
                                                    <i class="fas fa-check"></i>
                                                </button>
                                                <button onclick="aprovacao_recurso(2,<?= $row['id'] ?>,<?= $id_id ?>)" class="btn btn-primary" style="background:#FFA800;padding: 10px 15px;text-align: -webkit-center;color:#fff">
                                                    <i class="fas fa-times"></i>
                                                </button>
                                            </div>
                                        <?php } else {
                                            if ($row['status'] == 1) {
                                                $status = "Aprovado";
                                            } else if ($row['status'] == 2) {
                                                $status = "Reprovado";
                                            }
                                        ?>
                                            <div style="display:flex">
                                                <div style="margin-right: 5px;padding: 10px;text-align: -webkit-center;">
                                                    <label style="margin:0"><?= $status ?></label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <a href="php/baixar_arquivo_consultas.php?tabela=recurso&campo=pedido_pdf&id=<?= $row['id'] ?>" class="btn" style="background:#FFA800;padding-left: 6px;font-size: 16px;margin-top: 5px;color:#fff">
                                            <i class="fas fa-cloud-download-alt"></i>
                                            Pedido
                                        </a>
                                        <?php if ($count_res_doc > 0) { ?>
                                            <a href="php/baixar_arquivo_consultas.php?tabela=recurso&campo=resposta_pdf&id=<?= $row['id'] ?>" class="btn" style="background:#FFA800;padding-left: 6px;font-size: 16px;margin-top: 5px;color:#fff">
                                                <i class="fas fa-cloud-download-alt"></i>
                                                Resposta
                                            </a>
                                        <?php } else { ?>
                                            <a class="btn" style="background:#FFA800;padding-left: 6px;font-size: 16px;margin-top: 5px;">
                                                <i class="fas fa-cloud-download-alt"></i>
                                                Resposta
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <center><label class="text-muted">Não há mais para mostrar</label></center>
                    </div>
                </div>
            </div>
        </div>
        <!----------- recurso 2------------->
        <!----------- esclarecimentos------------->
        <div class="modal fade" id="esclarecimentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 730px !important;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <div id="accordion-votacao" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                            <?php
                            $sql = "SELECT * FROM edital_esclarecimento WHERE edital_id = $id_id";
                            $res_esclarecimentos = mysqli_query($conn, $sql);

                            while ($row = mysqli_fetch_array($res_esclarecimentos)) {

                            ?>
                                <div class="form-group" style="background: #fff;border-radius: 25px;padding: 20px; margin-bottom: 5px">
                                    <div style="display:flex;">
                                        <div style="align-self: center;">
                                            <img style="width: 115px;" src="image/edital_aberto2.png" alt="">
                                        </div>

                                        <div style="display:block;margin-right:4%;">
                                            <p style="color:#2775EA">Esclarecimento <?= date('d/m/Y', strtotime($row['data_cad'])) ?> <span style="color:#5E5E5E"><?= $row['motivo_1'] ?></span></p>
                                            <p><?= $row['observacao']; ?></p>
                                        </div>
                                        <div>
                                            <a href="php/baixar_arquivo_esc_imp.php?tabela=esclarecimento&campo=pedido_pdf&id=<?= $row['id'] ?>" class="btn btn-primary" style="padding-left: 6px;font-size: 16px;margin-top: 5px;">
                                                <i class="fas fa-cloud-download-alt"></i>
                                                Pedido
                                            </a>
                                            <a href="php/baixar_arquivo_esc_imp.php?tabela=esclarecimento&campo=resposta_pdf&id=<?= $row['id'] ?>" class="btn btn-primary" style="padding-left: 6px;font-size: 16px;margin-top: 5px;">
                                                <i class="fas fa-cloud-download-alt"></i>
                                                Resposta
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <center><label class="text-muted">Não há mais para mostrar</label></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!----------- esclarecimentos------------->
        <!----------- impugnações------------->
        <div class="modal fade" id="impugnacoes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 730px !important;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <div id="accordion-votacao" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                            <?php
                            $sql = "SELECT * FROM edital_impugnacao WHERE edital_id = $id_id";
                            $res_impugnacoes = mysqli_query($conn, $sql);

                            while ($row = mysqli_fetch_array($res_impugnacoes)) {

                            ?>
                                <div class="form-group" style="background: #fff;border-radius: 25px;padding: 20px; margin-bottom: 5px">
                                    <div style="display:flex;">
                                        <div style="align-self: center;">
                                            <img style="width: 115px;" src="image/edital_aberto2.png" alt="">
                                        </div>

                                        <div style="display:block; margin-right:4%;">
                                            <p style="color:#2775EA">Impugnação <?= date('d/m/Y', strtotime($row['data_cad'])) ?> <span style="color:#5E5E5E"><?= $row['motivo_2'] ?></span></p>
                                            <p>Observação: <?= $row['observacao'] ?></p>
                                        </div>
                                        <div>
                                            <a href="php/baixar_arquivo_esc_imp.php?tabela=impugnacao&campo=pedido_pdf&id=<?= $row['id'] ?>" class="btn btn-primary" style="padding-left: 6px;font-size: 16px;margin-top: 5px;">
                                                <i class="fas fa-cloud-download-alt"></i>
                                                Pedido
                                            </a>
                                            <a href="php/baixar_arquivo_esc_imp.php?tabela=impugnacao&campo=resposta_pdf&id=<?= $row['id'] ?>" class="btn btn-primary" style="padding-left: 6px;font-size: 16px;margin-top: 5px;">
                                                <i class="fas fa-cloud-download-alt"></i>
                                                Resposta
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <center><label class="text-muted">Não há mais para mostrar</label></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!----------- impugnações ------------->
        <!----------- baixar------------->
        <div class="modal fade" id="baixarArq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 730px !important;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <div id="accordion-votacao" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                            <?php
                            $sql = "SELECT * FROM edital_arquivo WHERE edital_id = $id_id";
                            $res_edital_arquivos = mysqli_query($conn, $sql);

                            while ($row = mysqli_fetch_array($res_edital_arquivos)) {
                            ?>
                                <div class="form-group" style="background: #fff;border-radius: 25px;padding: 20px; margin-bottom: 5px;position:relative">
                                    <div style="display:flex;">
                                        <div style="align-self: center;">
                                            <img style="width: 40px;" src="image/edital_aberto2.png" alt="">
                                        </div>

                                        <div style="display:block;align-self: center;margin-left: 10px;">
                                            <p style="margin: 0px;"><?= $row['nome_arquivo'] ?></p>
                                        </div>
                                        <a href="php/download_arquivos.php?id_arquivo=<?= $row['id'] ?>" class="btn btn-primary" style="padding-left: 6px;font-size: 25px;margin-top: 5px;position: absolute;width: 45px;right: 19px;top: 14px;">
                                            <i class="fas fa-cloud-download-alt"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <center><label class="text-muted">Não há mais para mostrar</label></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!----------- baixar ------------->
        <!----------- imprimir------------->
        <!-- <div class="modal fade" id="imprimir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 730px !important;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Edital Nº: <span id="declinarId"></span>?</h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <div id="accordion-votacao" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                            <div id="conteudo-pautas">
                                <embed src="data:application/pdf;base64,<?php //echo ficheiro; 
                                                                        ?>" type="application/pdf" frameborder="0" style="margin: 0;padding: 0;height: 43vh;width: 96%;padding-right: 15px;">
                                </embed>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer" style="justify-content: center;"> -->
        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
        <!-- <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Voltar</button>
                        <button onclick="location.reload()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
                            Imprimir -->
        <!-- </button>

                    </div>
                </div>
            </div>
        </div> -->
        <!----------- imprimir ------------->
        <!-----------------------------------APROVADO------------------------------------------------------------->
        <div class="modal fade" id="aprovadoObs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">NTS ENGENHARIA</h5>

                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="php/aprovarFaseFinal.php" method="POST" id="form-aprovar">
                            <input type="hidden" name="id_edital_aprovado" value="<?= $id_id ?>">
                        </form>
                        <div class="form-group" style="text-align-last: center;">
                            <label style="margin-top: 20px; color: #000" for="exampleFormControlSelect1">Tem certeza que deseja aprovar o edital <?= $id_id ?>?</label>
                        </div>
                    </div>
                    <div class="modal-footer" style="justify-content: center!important;">
                        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                        <button onclick="aprovar()" type="button" data-dismiss="modal" class="btn" style="text-align: center; text-transform: none;background: #07B204;color: #fff;">
                            Aprovar
                        </button>

                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;color: #fff;background: #FF0000;" data-dismiss="modal">Cancelar</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="homologarCad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">NTS ENGENHARIA</h5>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="php/homologar.php" method="POST" id="form-homologar">
                            <input type="hidden" name="id_edital_homologar" value="<?= $id_id ?>">
                        </form>
                        <div class="form-group" style="text-align-last: center;">
                            <label style="margin-top: 20px; color: #000" for="exampleFormControlSelect1">Tem certeza que deseja enviar para homologação o edital <?= $id_id ?>?</label>
                        </div>
                    </div>
                    <div class="modal-footer" style="justify-content: center!important;">
                        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                        <button onclick="homologar()" type="button" data-dismiss="modal" class="btn" style="text-align: center; text-transform: none;background: #07B204;color: #fff;">
                            Homologar
                        </button>

                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;color: #fff;background: #FF0000;" data-dismiss="modal">Cancelar</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="homologarCadastro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">NTS ENGENHARIA</h5>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="php/cadastra_homologacao.php" method="POST" id="form-homologar-cadastro" enctype="multipart/form-data">
                            <input type="hidden" name="id_edital_homologar" value="<?= $id_id ?>">
                            <div class="row">
                                <div class="col">
                                    <input type="file" style="display: none" id="documento_homologacao" name="documento_homologacao" accept=".pdf">
                                    <div id="falseinput_homologacao" class="btn btn-primary" style="width:50%; height:30px;font-size:10px; display:flex;align-items:center;justify-content:center;">
                                        <label id="name_homologacao" style="color:white;cursor:pointer;">Anexar PDF</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <textarea style="cursor:auto;" name="obs_homologacao" id="obs_homologacao" rows="3" class="form-control" placeholder="Observação"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="justify-content: center!important;">
                        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Voltar</button>
                        <button onclick="cadastra_homologacao()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-transform: none; text-align: center;">
                            Registrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!----------- cadastrar ata------------->
        <div class="modal fade" id="ataCad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <form action="php/cadastra_ata.php" id="form-cadastra-ata" method="POST">
                            <input type="hidden" name="id_edital_ata" value="<?= $id_id ?>">
                            <input type="hidden" name="tela_ata" id="tela_ata" value="Fase3">

                            <div class="form-row">
                                <div class="col"></div>
                                <div class="col-5">
                                    <label for="modalidade_ata">Selecione a modalidade da ATA:</label>
                                </div>
                                <div class="col-5">
                                    <select name="modalidade_ata" id="modalidade_ata" class="form-control" onchange="define_campos_ata(this)">
                                        <option value="0">Habilitação</option>
                                        <option value="1">Proposta</option>
                                        <option value="2">Proposta técnica</option>
                                    </select>
                                </div>
                                <div class="col"></div>
                            </div><br>
                            <div class="form-row" style="margin-bottom: 1rem">
                                <div class="col-5" style="align-self: center;">
                                    <label for="data_ata" style="font-size:14px;"><b>Data Sessão: </b></label>
                                    <input type="date" class="fields-ata" name="data_ata" id="data_ata" style="font-size: 12px;">
                                </div>
                                <div class="col" style="align-self: center;">
                                    <label for="inicio_ata" style="font-size:14px;"><b>Horário Início: </b></label>
                                    <input type="time" class="fields-ata" name="inicio_ata" id="inicio_ata" style="font-size: 12px;">
                                </div>
                                <div class="col" style="align-self: center;">
                                    <label for="fim_ata" style="font-size:14px;"><b>Horário Fim: </b></label>
                                    <input type="time" class="fields-ata" name="fim_ata" id="fim_ata" style="font-size: 12px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label>Concorrente: <span>NTS ENGENHARIA</span></label>
                                        <input type="hidden" name="concorrente_1" value="0">
                                        <input class="fields-ata" style="color:#2137FF;vertical-align: top;width: 100%;margin-bottom: 5px;" value="NTS ENGENHARIA" readonly>
                                    </div>
                                    <div class="col" style="align-self: flex-end;">
                                        <input type="text" name="rep_concorrente_1" class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;" placeholder="Representante Legal">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-row">
                                            <div class="col-4">
                                                <input type="number" step="0.01" class="fields-ata" placeholder="R$" name="valor_concorrente_1" id="valor_concorrente_1" style="width: 100%;">
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <p style="margin: 0;font-size: 20px;align-self:center;">Pos:</p>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <select name="posicao_concorrente_1" id="posicao_concorrente_1" class="fields-ata" style="vertical-align: top;float: right;padding:11px;">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <p style="margin: 0;font-size: 20px;align-self:center;">Hab:</p>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <select name="hab_concorrente_1" id="hab_concorrente_1" class="fields-ata" style="vertical-align: top;float: right;padding:11px;">
                                                    <option value="1">S</option>
                                                    <option value="0">N</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="aleg_concorrente_1" class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;" placeholder="Alegações e motivos">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label>Concorrente: <span id="concorrente_nome_2">Não encontrado</span></label>
                                        <input type="number" name="concorrente_2" id="concorrente_2" class="fields-ata" style="color: #5D5D5D;;vertical-align: top;width: 100%;margin-bottom: 5px;">
                                    </div>
                                    <div class="col" style="align-self: flex-end;">
                                        <input type="text" name="rep_concorrente_2" class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;" placeholder="Representante Legal">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-row">
                                            <div class="col-4">
                                                <input type="number" step="0.01" class="fields-ata" placeholder="R$" name="valor_concorrente_2" id="valor_concorrente_2" style="width: 100%;">
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <p style="margin: 0;font-size: 20px;align-self:center;">Pos:</p>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <select name="posicao_concorrente_2" id="posicao_concorrente_2" class="fields-ata" style="vertical-align: top;float: right;padding:11px;">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <p style="margin: 0;font-size: 20px;align-self:center;">Hab:</p>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <select name="hab_concorrente_2" id="hab_concorrente_2" class="fields-ata" style="vertical-align: top;float: right;padding:11px;">
                                                    <option value="1">S</option>
                                                    <option value="0">N</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="aleg_concorrente_2" class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;" placeholder="Alegações e motivos">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label>Concorrente: <span id="concorrente_nome_3">Não encontrado</span></label>
                                        <input type="number" name="concorrente_3" id="concorrente_3" class="fields-ata" style="color: #5D5D5D;;vertical-align: top;width: 100%;margin-bottom: 5px;">
                                    </div>
                                    <div class="col" style="align-self: flex-end;">
                                        <input type="text" name="rep_concorrente_3" class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;" placeholder="Representante Legal">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-row">
                                            <div class="col-4">
                                                <input type="number" step="0.01" class="fields-ata" placeholder="R$" name="valor_concorrente_3" id="valor_concorrente_3" style="width: 100%;">
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <p style="margin: 0;font-size: 20px;align-self:center;">Pos:</p>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <select name="posicao_concorrente_3" id="posicao_concorrente_3" class="fields-ata" style="vertical-align: top;float: right;padding:11px;">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <p style="margin: 0;font-size: 20px;align-self:center;">Hab:</p>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <select name="hab_concorrente_3" id="hab_concorrente_3" class="fields-ata" style="vertical-align: top;float: right;padding:11px;">
                                                    <option value="1">S</option>
                                                    <option value="0">N</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="aleg_concorrente_3" class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;" placeholder="Alegações e motivos">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label>Concorrente: <span id="concorrente_nome_4">Não encontrado</span></label>
                                        <input type="number" name="concorrente_4" id="concorrente_4" class="fields-ata" style="color: #5D5D5D;;vertical-align: top;width: 100%;margin-bottom: 5px;">
                                    </div>
                                    <div class="col" style="align-self: flex-end;">
                                        <input type="text" name="rep_concorrente_4" class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;" placeholder="Representante Legal">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-row">
                                            <div class="col-4">
                                                <input type="number" step="0.01" class="fields-ata" placeholder="R$" name="valor_concorrente_4" id="valor_concorrente_4" style="width: 100%;">
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <p style="margin: 0;font-size: 20px;align-self:center;">Pos:</p>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <select name="posicao_concorrente_4" id="posicao_concorrente_4" class="fields-ata" style="vertical-align: top;float: right;padding:11px;">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <p style="margin: 0;font-size: 20px;align-self:center;">Hab:</p>
                                            </div>
                                            <div class="col-2" style="align-self: center;">
                                                <select name="hab_concorrente_4" id="hab_concorrente_4" class="fields-ata" style="vertical-align: top;float: right;padding:11px;">
                                                    <option value="1">S</option>
                                                    <option value="0">N</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <input type="text" name="aleg_concorrente_4" class="fields-ata" style="vertical-align: top;width: 100%;margin-bottom: 5px;" placeholder="Alegações e motivos">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <textarea class="form-control" placeholder="Observação" name="obs_ata" id="obs_ata" rows="3" maxlength="300"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="justify-content: center;background:#E6E6E6">
                        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Voltar</button>
                        <button onclick="cadastra_ata()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
                            Registrar
                        </button>

                    </div>
                </div>
            </div>
        </div>
        <!----------- cadastrar ata ------------->
        <!-- consultar lista de atas -->
        <div class="modal fade" id="consultarAtas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 730px !important;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <div id="accordion-votacao" class="acc-widthh" style="height: 45vh;    padding: 10px;">
                            <?php
                            $sql = "SELECT * FROM edital_ata WHERE edital_id = $id_id";
                            $res_atas = mysqli_query($conn, $sql);

                            $cont_ata = 1;
                            while ($row = mysqli_fetch_array($res_atas)) {
                            ?>
                                <div onclick="abrir_ata(<?= $row['id'] ?>)" class="form-group" style="background: #fff;border-radius: 25px;padding: 20px; margin-bottom: 5px;position:relative;cursor:pointer;">
                                    <div style="display:flex;align-items: center;">
                                        <div style="align-self: center;">
                                            <img style="width: 40px;" src="image/edital_aberto2.png" alt="">
                                        </div>

                                        <div style="display:block;align-self: center;margin-left: 10px;">
                                            <p style="margin: 0px;">Ata <?= $cont_ata ?></p>
                                        </div>
                                        <label style="padding-left: 6px;position: absolute;right: 19px;">
                                            <?= date('d/m/Y', strtotime($row['data'])) ?>
                                        </label>
                                    </div>
                                </div>
                            <?php
                                $cont_ata++;
                            } ?>
                            <center>
                                <label>Não há mais atas</label>
                            </center>
                        </div>
                    </div>
                    <div class="modal-footer" style="justify-content: center;background:#E6E6E6">
                        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Voltar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- consultar lista de atas -->
        <!----------- consultar ata selecionada------------->
        <div class="modal fade" id="ataSelecionada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;color:#000">Edital Nº: <?= $id_id ?></h5>
                    </div>
                    <div class="modal-body" style="background: #E2E2E2;">
                        <div id="conteudo_ata"></div>
                    </div>
                    <div class="modal-footer" style="justify-content: center;background:#E6E6E6">
                        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Voltar</button>
                    </div>
                </div>
            </div>
        </div>
        <!----------- declinado sucesso ------------->

        <div class="modal fade" id="responseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style=" font-weight: 500;">Declinado com sucesso!
                        </h5>

                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div id="responseCad" style=" color: #000;">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                        <button type="button" class="btn btn-secondary" style="text-transform: none; text-align: center;" data-dismiss="modal">Fechar</button>
                        <button onclick="location.reload()" type="button" data-dismiss="modal" class="btn btn-primary" style="text-align: center; text-transform: none;">
                            Ok
                        </button>

                    </div>
                </div>
            </div>
        </div>

        <!------------------------------------------------------------------------------------------------------------------->


        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="js/fuctions.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>



</body>
<script>
    var inputs = [];
    var input;

    input = document.getElementById("concorrente_2");
    inputs.push(input);
    input = document.getElementById("concorrente_3");
    inputs.push(input);
    input = document.getElementById("concorrente_4");
    inputs.push(input);

    inputs.forEach(function(elem) {
        elem.addEventListener("keyup", function(event) {
            if (elem.value.length == 14 && event.keyCode != 17) {
                var indice_input = elem.id.split("_")[1];
                var span = $('#concorrente_nome_' + indice_input);
                $.get('php/verifica_cad_concorrente.php?cnpj=' + elem.value, async function(data) {
                    if (data == 0) {
                        await axios({
                                method: 'GET',
                                url: 'https://api.cnpja.com.br/companies/' + elem.value,
                                data: {},
                                headers: {
                                    "authorization": "0552132e-4140-4fa7-82f7-ab436ce169b3-463054c6-31fc-4624-8944-12b4b5abe1ee"
                                }
                            })
                            .then(function(response) {
                                var formData = new FormData();
                                // console.log(response);

                                var number = response.data.address.city;
                                if (number == "SN") {
                                    number = 0;
                                }

                                var ativo = response.data.registration.status;
                                if (ativo == "ATIVA") {
                                    ativo = 1;
                                } else {
                                    ativo = 0;
                                }

                                formData.append("cnpj_concorrente", response.data.tax_id);
                                response.data.name = response.data.name.replace(/'/g, "´");
                                formData.append("razao_social_concorrente", response.data.name);
                                // response.data.nome_fantasia = response.data.razao_social.replace(/'/g, "´");
                                formData.append("nome_fantasia_concorrente", response.data.name);
                                formData.append("natureza_juridica_concorrente", response.data.legal_nature.description);
                                formData.append("porte_empresa_concorrente", response.data.size);
                                formData.append("cnae_concorrente", response.data.primary_activity.description);
                                formData.append("logradouro_concorrente", response.data.address.street);
                                formData.append("numero_logradouro_concorrente", number);
                                formData.append("complemento_logradouro_concorrente", response.data.address.details);
                                formData.append("bairro_concorrente", response.data.address.neighborhood);
                                formData.append("municipio_concorrente", response.data.address.city);
                                formData.append("estado_concorrente", response.data.address.state);
                                formData.append("cep_concorrente", response.data.address.zip);
                                formData.append("ativo_concorrente", ativo);
                                // formData.append("recadastrado_concorrente", response.data.recadastrado);
                                // formData.append("habilitado_licitar_concorrente", response.data.habilitado_licitar);

                                $.ajax({
                                    url: "../nts_admin/php/cadastra_concorrente.php",
                                    type: 'POST',
                                    data: formData,
                                    success: function(data) {
                                        alert("Cadastrado com Sucesso!")
                                    },
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    xhr: function() { // Custom XMLHttpRequest
                                        var myXhr = $.ajaxSettings.xhr();
                                        if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                                            myXhr.upload.addEventListener('progress', function() {

                                            }, false);
                                        }
                                        return myXhr;
                                    }
                                });
                            })
                            .catch(function(response) {
                                alert("Não encontrado.");
                                span.html("Não encontrado.");
                            });
                    } else {
                        $.get('php/get_concorrente.php?cnpj=' + elem.value, function(data2) {
                            span.html(data2);
                        });
                    }
                });
            }
        });
    });

    $('#concorrente_juridico').on("change keyup paste", function() {
        let input = $(this).val();
        if (input.length == 14) {
            $.get('php/get_concorrente.php?cnpj=' + input, function(data) {
                $('#razao_social_juridico').val(data);
            });
        }
    });

    $('#concorrente_recurso').on("change keyup paste", function() {
        let input = $(this).val();
        if (input.length == 14) {
            $.get('php/get_concorrente.php?cnpj=' + input, function(data) {
                $('#razao_social_recurso').val(data);
            });
        }
    });

    $(document).ready(function() {
        $('#falseinput_pedido_juridico').click(function() {
            $("#file_pedido_juridico").click();
        });
        $('#falseinput_resp_juridico').click(function() {
            $("#file_resp_juridico").click();
        });
        $('#falseinput_pedido_recurso').click(function() {
            $("#file_pedido_recurso").click();
        });
        $('#falseinput_resp_recurso').click(function() {
            $("#file_resp_recurso").click();
        });
        $('#falseinput_homologacao').click(function() {
            $("#documento_homologacao").click();
        });

        // DEFAULT HABILITAÇÃO DA ATA
        for (i = 1; i <= 4; i++) {
            document.getElementById('valor_concorrente_' + i).setAttribute('disabled', 'disabled');
            document.getElementById('posicao_concorrente_' + i).setAttribute('disabled', 'disabled');
            document.getElementById('hab_concorrente_' + i).setAttribute('disabled', 'disabled');

            document.getElementById('hab_concorrente_' + i).removeAttribute('disabled');
        }
    });

    $('#file_pedido_juridico').change(function() {
        $('#name_pedido_juridico').html($('#file_pedido_juridico')[0].files[0].name);
    });
    $('#file_resp_juridico').change(function() {
        $('#name_resp_juridico').html($('#file_resp_juridico')[0].files[0].name);
    });
    $('#file_pedido_recurso').change(function() {
        $('#name_pedido_recurso').html($('#file_pedido_recurso')[0].files[0].name);
    });
    $('#file_resp_recurso').change(function() {
        $('#name_resp_recurso').html($('#file_resp_recurso')[0].files[0].name);
    });
    $('#documento_homologacao').change(function() {
        $('#name_homologacao').html($('#documento_homologacao')[0].files[0].name);
    });

    function abrir_concorrente(obj, cad) {
        console.log("entrou");
        if (obj.value == "Terceiros") {
            document.getElementById('div_con_' + cad).classList.add("show");
            document.getElementById('div_con_' + cad).classList.remove("hide");
            // $('div_con_'+cad).removeClass('hide').addClass('show');
        } else {
            document.getElementById('div_con_' + cad).classList.add("hide");
            document.getElementById('div_con_' + cad).classList.remove("show");
        }
    }

    function homologar() {
        $('#form-homologar').submit();
        setTimeout(function() {
            location.reload();
        }, 200);
    }

    function cadastra_homologacao() {
        $('#form-homologar-cadastro').submit();
        setTimeout(function() {
            location.reload();
        }, 200);
    }

    function declinar() {
        $('#form-declinar').submit();
    }

    function cadastra_juridico() {
        $('#form-cadastra-juridico').submit();
    }

    function aprovacao_juridico(status, id, edital) {
        $.get('php/aprovacao_juridico.php?status=' + status + '&id=' + id + '&id_edital=' + edital);
        location.reload();
    }

    function cadastra_recurso() {
        $('#form-cadastra-recurso').submit();
    }

    function aprovacao_recurso(status, id, edital) {
        $.get('php/aprovacao_recurso.php?status=' + status + '&id=' + id + '&id_edital=' + edital);
        location.reload();
    }

    function define_campos_ata(obj) {
        console.log(obj.value);
        if (parseInt(obj.value) == 0) {
            for (i = 1; i <= 4; i++) {
                console.log("a");
                // readonly em valor e posição
                document.getElementById('valor_concorrente_' + i).setAttribute('disabled', 'disabled');
                document.getElementById('posicao_concorrente_' + i).setAttribute('disabled', 'disabled');
                document.getElementById('hab_concorrente_' + i).setAttribute('disabled', 'disabled');

                document.getElementById('hab_concorrente_' + i).removeAttribute('disabled');
            }
        } else if (parseInt(obj.value) == 1) {
            // disabled habilitado
            for (i = 1; i <= 4; i++) {
                console.log("b");
                document.getElementById('valor_concorrente_' + i).setAttribute('disabled', 'disabled');
                document.getElementById('posicao_concorrente_' + i).setAttribute('disabled', 'disabled');
                document.getElementById('hab_concorrente_' + i).setAttribute('disabled', 'disabled');

                document.getElementById('valor_concorrente_' + i).removeAttribute('disabled');
                document.getElementById('posicao_concorrente_' + i).removeAttribute('disabled');

            }
        } else if (parseInt(obj.value) == 2) {
            // todos habilitados
            for (i = 1; i <= 4; i++) {
                console.log("c");
                document.getElementById('valor_concorrente_' + i).setAttribute('disabled', 'disabled');
                document.getElementById('posicao_concorrente_' + i).setAttribute('disabled', 'disabled');
                document.getElementById('hab_concorrente_' + i).setAttribute('disabled', 'disabled');

                document.getElementById('valor_concorrente_' + i).removeAttribute('disabled');
                document.getElementById('posicao_concorrente_' + i).removeAttribute('disabled');
                document.getElementById('hab_concorrente_' + i).removeAttribute('disabled');
            }
        }
    }

    function cadastra_ata() {
        $('#form-cadastra-ata').submit();
    }

    function abrir_ata(id) {
        $.get('php/get_ata_selecionada.php?id=' + id, function(data) {
            $('#conteudo_ata').html(data);
            $('#ataSelecionada').modal("show");
        });
    }

    function baixa_arquivo_consultas(tabela, campo, id) {
        $.get('php/baixar_arquivo_consultas.php?tabela=' + tabela + '&campo=' + campo + '&id=' + id);
    }

    function aprovar() {
        $('#form-aprovar').submit();
    }

    function setaMotivoDeclinio(obj) {
        $.get('php/get_motivo_por_categoria.php?id_cat=' + obj.value, function(data) {
            $('#selectMotivo').html(data);
            $('#selectMotivo').removeAttr("readonly");
        });
    }
</script>

</html>